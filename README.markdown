# Safety Lock

## Motivation

Our ability to fabricate more and more productive general-purpose processors is diminishing.
In light of this, teams in research and industry are increasingly reaching for programmable accelerators.
For example, large companies have already begun deploying FPGA's in many high-throughput contexts.

The proliferation of programmable hardware leads to two important concerns:

  1. Consumers want *guarantees of safety properties* of the designs they plan to use.
  2. Designers want the ability to *protect their intellectual property (IP)* in the context of a design marketplace.

A fair amount of research has focused on each of these concerns in isolation:

  + There has been recent work looking to prove the safety properties of hardware.

  + A recent area of research has been focused on so called Logic Locking techniques.
    These techniques add key inputs to a design which must be correctly set before the circuit will function correctly.
    Therefore, the designer can use their ability to distribute the key as a means to prevent IP leaks.
    Some work in this area includes the following:

    1. [Roy, Koushanfar, and Markov, 2008](http://www.univ-st-etienne.fr/salware/Bibliography_Salware/IC%20Protection/article/Roy2008.pdf)
    2. [Subramanyan, Ray, and Malik, 2015](http://www.eecs.umich.edu/courses/eecs578/eecs578.f15/papers/sub15.pdf)
    3. [Xie and Srivastava, 2016](http://link.springer.com/chapter/10.1007/978-3-662-53140-2_7)
    4. [Yasin, Mazumdar, Rejendran, and Sinanoglu, 2016](http://ieeexplore.ieee.org/document/7495588/)

However, comparatively little emphasis has been placed on addressing both concerns simultaneously:
Can we prove safety properties of hardware in the presence of encryption?
This question is important because the two concerns are *not orthogonal*:

  + Strategies for guaranteeing the safety properties of designs involve generating formal proofs.
    However, these proofs *many not hold when the original design is modified*.

  + Logic Locking techniques involve *modifying the original design* and adding additional key inputs.

An approach which addresses both concerns will need to provide proofs that hold for a design modified by logic locking.
The goals of Safety Lock are chosen to explore strategies for providing safety properties for locked designs:

  + Investigate and articulate any trade-offs between the two concerns.
  + Modify existing techniques in both research areas to operate in conjunction with one another.
  + Provide a prototype implementation which applies the technqiues to benchmark designs.
  + Investigate the theoretical and experimental characteristics of the techniques/prototype.

## Directory Structure

  + Benchmark/Design
    A set of Verilog core designs including both trivial designs and complex, open-source FGPA cores from OpenCores and AxBench.
  + Benchmark/LockedNetlist
    A set of netlist designs along with matching, encrypted designs.
  + SafetyLock
    Software implementation of prototype.
    Currently includes an implementation of a Random Logic Locking algorithm [1].

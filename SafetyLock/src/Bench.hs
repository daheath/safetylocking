{-# LANGUAGE LambdaCase #-}

module Bench where

import qualified Gate as G
import Netlist
import Format (argApply)

import Data.Fix (cata)

fromNetlist :: Netlist -> String
fromNetlist nl =
  unlines (map input   $ inputs  nl) ++
  unlines (map output  $ outputs nl) ++
  unlines (map connect $ nl)

  where input  i = argApply "INPUT"  [G.showWire i]
        output o = argApply "OUTPUT" [G.showWire o]
        connect (Connection g o) = G.showWire o ++ " = " ++ gate g

        gate :: G.NetGate -> String
        gate = G.netwise $ cata $ \case
          G.Conn w     -> G.showWire w
          G.Not  x     -> argApply "not" [x]
          G.Bin  g x y -> argApply (G.showBinaryGate g) [x, y]

fromCircuit :: Circuit c => c -> String
fromCircuit = fromNetlist . netlist

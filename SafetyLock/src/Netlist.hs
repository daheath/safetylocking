{-# LANGUAGE FlexibleInstances, LambdaCase #-}

module Netlist where

import qualified Gate as G
import Identifier

import Prelude hiding (and, not)
import Control.Monad.Trans.State (StateT, modify, execStateT)
import Control.Monad.Trans.Class (lift)
import Data.Fix (cataM)

import Data.Set (Set)
import qualified Data.Set as S

type Netlist = [Connection]

data Connection = Connection G.NetGate G.Wire
  deriving (Show, Eq)

type Partial a = StateT Netlist IdState a

wireSet :: Netlist -> Set G.Wire
wireSet = foldMap cwires
  where cwires (Connection g w) = S.insert w (G.netwise G.wires $ g)

wires, inputs, outputs, inners, vars :: Netlist -> [G.Wire]
wires   = S.toList . wireSet
inputs  = filter G.isInput  . wires
outputs = filter G.isOutput . wires
inners  = filter G.isInner  . wires
vars nl = inputs nl ++ outputs nl

input, output, wire :: Partial G.Wire
input  = G.Wire G.Input  <$> lift buildId
output = G.Wire G.Output <$> lift buildId
wire   = G.Wire G.Inner  <$> lift buildId

connect :: G.NetGate -> G.Wire -> Partial G.Wire
connect g w = modify (Connection g w :) >> pure w

runPartial :: Partial a -> Netlist
runPartial c = reverse $ runId (execStateT c [])

class Circuit a where
  circuit :: a -> Partial [G.Wire]

instance Circuit G.Gate where
  circuit x = (\x -> [x]) <$> inner x
    where
      inner :: G.Gate -> Partial G.Wire
      inner = cataM $ \case
        G.Conn s -> pure s
        gate     -> connect gate =<< wire

instance (Circuit c) => Circuit (G.Gate -> c) where
  circuit f = G.conn <$> input >>= circuit . f

instance Circuit c => Circuit [c] where
  circuit = fmap concat . mapM circuit

netlist :: Circuit c => c -> Netlist
netlist c = runPartial $ do
  outs <- circuit c
  modify (map (setWire
               (\w@(G.Wire _ id) ->
                   if w `elem` outs then G.Wire G.Output id else w)))

  where setWire :: (G.Wire -> G.Wire) -> Connection -> Connection
        setWire f (Connection g o) = Connection (fmap f g) (f o)

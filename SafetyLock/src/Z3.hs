{-# LANGUAGE FlexibleInstances, LambdaCase #-}

module Z3 where

import Gate
import Netlist (Netlist)
import qualified Netlist as N
import Format (indent, parens)

import Data.Fix (cata)

gate :: Gate -> String
gate = cata $ \case
  Conn w     -> showWire w
  Not  x     -> parens ["not" , x]
  Bin  g x y -> parens [showBinaryGate g, x, y]

relation :: N.Netlist -> String
relation nl = parens ("Circuit" : map showWire (N.vars nl))

netlist :: N.Netlist -> String
netlist nl = "(rule (=>\n" ++ indent (lines (body nl) ++ [relation nl]) ++ "))\n"

body :: N.Netlist -> String
body nl = "(and\n" ++ indent connections ++ ")"
  where
    connections = map connect nl
    connect (N.Connection g o) = parens ["=", showWire o, netwise gate g]

class Query q where
  query :: [Gate] -> q -> Maybe Gate

instance Query Gate where
  query [] g = Just g
  query _ _  = Nothing

instance Query q => Query (Gate -> q) where
  query (x:xs) f = query xs (f x)
  query _ _      = Nothing

script :: (N.Circuit c, Query q) => c -> q -> Maybe String
script c q = do
  q <- query (map conn (N.vars nl)) q
  pure $ unlines [ "(set-option :fixedpoint.engine \"duality\")"
                 , decl
                 , "(declare-rel q ())"
                 , vars
                 , netlist nl
                 , rule q
                 , "(query q)"
                 ]
  where
    rule q = "(rule (=> (not\n" ++
             "  (=>\n" ++
             "    " ++ relation nl ++ "\n" ++
             "    " ++ gate q ++ "))\n" ++
             "  q))\n"
    nl = N.netlist c
    decl = parens ["declare-rel"
                  , "Circuit"
                  , parens $ map (const "Bool") (N.vars nl)]
    vars = unlines (map var (N.vars nl) ++ map cons (N.inners nl))
    var  v = parens ["declare-var"  , showWire v, "Bool"]
    cons v = parens ["declare-const", showWire v, "Bool"]

{-# LANGUAGE DeriveFunctor, DeriveTraversable, LambdaCase #-}

module Gate where

import Prelude hiding (not, and, or)

import Data.Fix (Fix (Fix), cata)

import Data.Set (Set)
import qualified Data.Set as S

import Identifier

data Wire' = Input | Output | Inner
  deriving (Eq, Ord, Show)

data Wire = Wire Wire' Identifier
  deriving (Eq, Ord, Show)

isInput  (Wire wt _) = wt == Input
isOutput (Wire wt _) = wt == Output
isInner  (Wire wt _) = wt == Inner

showWire :: Wire -> String
showWire (Wire ty (Identifier id)) =
  let prefix = case ty of
        Input  -> 'i'
        Output -> 'o'
        Inner  -> 'w' in
  prefix : show id

data BinaryGate = And | Nand | Or | Nor | Xor | Xnor
  deriving (Show, Eq)

showBinaryGate :: BinaryGate -> String
showBinaryGate g = case g of
  And  -> "and"
  Nand -> "nand"
  Or   -> "or"
  Nor  -> "nor"
  Xor  -> "xor"
  Xnor -> "xnor"

data Gate' a
  = Conn Wire
  | Not a
  | Bin BinaryGate a a
  deriving (Show, Eq, Functor, Foldable, Traversable)

not    = Fix . Not
conn   = Fix . Conn
and  x = Fix . (Bin And ) x
nand x = Fix . (Bin Nand) x
or   x = Fix . (Bin Or  ) x
nor  x = Fix . (Bin Nor ) x
xor  x = Fix . (Bin Xor ) x
xnor x = Fix . (Bin Xnor) x

type Gate = Fix Gate'

type NetGate = Gate' Wire

promote :: NetGate -> Gate
promote g = case g of
  Conn w    -> conn w
  Not x     -> not  (conn x)
  Bin g x y -> Fix $ Bin g (conn x) (conn y)

netwise :: (Gate -> a) -> NetGate -> a
netwise f = f . promote

wires :: Gate -> Set Wire
wires = cata $ \case
  Conn w    -> S.singleton w
  Not  x    -> x
  Bin _ x y -> S.union x y

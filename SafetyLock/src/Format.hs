module Format where

import Data.List (intercalate)

parens :: [String] -> String
parens ss = "(" ++ unwords ss ++ ")"

indent :: [String] -> String
indent = intercalate "\n" . map ("  "++)

arglist :: [String] -> String
arglist ss = "(" ++ intercalate ", " ss ++ ")"

argApply :: String -> [String] -> String
argApply f as = f ++ arglist as

block :: String -> String -> String -> String
block bef mid aft =
  bef ++ "\n" ++ indent (lines mid) ++ "\n" ++ aft

{-# LANGUAGE LambdaCase #-}

module Simulate where

import qualified Gate as G
import qualified Netlist as N

import Control.Monad.Trans.State (StateT, evalStateT, get, modify)
import Control.Monad.Trans.Class (lift)
import Data.Fix (cataM)

import qualified Data.Map as M
import Data.Map (Map)

type Eval a = StateT (Map G.Wire Bool) Maybe a

nand, nor, xor, xnor :: Bool -> Bool -> Bool
nand x y = not (x && y)
nor  x y = not (x || y)
xor  x y = (x || y) && not (x && y)
xnor x y = not (xor x y)

fetch :: G.Wire -> Eval Bool
fetch w = get >>= lift . M.lookup w

gate :: G.NetGate -> Eval Bool
gate = G.netwise $ cataM $ \case
  G.Conn s    -> fetch s
  G.Not  x    -> pure $ not x
  G.Bin g x y -> pure $ (binary g) x y
  where
    binary :: G.BinaryGate -> Bool -> Bool -> Bool
    binary g = case g of
      G.And  -> (&&)
      G.Nand -> nand
      G.Or   -> (||)
      G.Nor  -> nor
      G.Xor  -> xor
      G.Xnor -> xnor

connect :: N.Connection -> Eval ()
connect (N.Connection g o) = gate g >>= \b -> modify (M.insert o b)

simulate :: N.Circuit c => [Bool] -> c -> Maybe [Bool]
simulate bs c =
  let nl   = N.netlist c
      inps = N.inputs nl
      init = M.fromList $ zip inps bs in
  if length inps /= length bs
  then Nothing
  else evalStateT (do
        mapM connect nl
        m <- get
        mapM (\s -> lift $ M.lookup s m) (N.outputs nl))
        init

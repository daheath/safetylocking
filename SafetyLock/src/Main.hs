module Main where

import qualified Gate as G
import Netlist

import qualified Simulate as S
import qualified Bench as B
import qualified Encrypt as E
import qualified Z3 as Z

import Data.Random

test x y = [ G.and (G.not x) y
           , G.and x (G.not y)
           ]

q :: G.Gate -> G.Gate -> G.Gate -> G.Gate -> G.Gate
q _ _ x y = G.or (G.not x) (G.not y)

-- main :: IO ()
-- main = case Z.script test q of
--   Nothing -> putStrLn "Oops!"
--   Just sc -> putStr sc

main :: IO ()
main = do
  let orig = netlist test
  enc <- runRVar (E.random 2 orig) StdRandom
  let (r, key) = enc orig
  putStrLn $ E.showKey key
  putStrLn $ B.fromNetlist r

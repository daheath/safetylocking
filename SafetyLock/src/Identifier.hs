{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Identifier where

import Control.Monad.State (State, evalState, state)

newtype Identifier = Identifier { getIdentifier :: Integer }
  deriving (Eq, Ord, Show)

newtype IdState a = IdState { runIdState :: State Integer a }
  deriving (Functor, Applicative, Monad)

runIdFrom :: Integer -> IdState a -> a
runIdFrom i m = (evalState . runIdState) m i

runId :: IdState a -> a
runId = runIdFrom 0

idState = IdState . state

buildId :: IdState Identifier
buildId = Identifier <$> idState (\id -> (id, id+1))

module Encrypt where

import qualified Gate as G
import Netlist
import Identifier

import Control.Monad (replicateM)

import Data.Random
import qualified Data.Random.Extras as RE (sample, choice)
import Data.List (zipWith4)
import Data.Map (Map)
import qualified Data.Map as M
import GHC.Exts (sortWith)

type KeySize = Int
type Key = [Bool]

showKey :: Key -> String
showKey = map showBool
  where showBool b = if b then '1' else '0'

-- An encryption is a function which encrypts a netlist with a key, returning
-- both the new netlist and and key.
type Encryption = Netlist -> (Netlist, Key)

-- The typical method for encrypting a netlist is to substitute some wires for
-- others and insert some new netlist connections.
encrypt :: Map G.Wire G.Wire -> [Connection] -> Key -> Netlist -> (Netlist, Key)
encrypt substs insrts key nl =
  (sortConns $ remapLhs substs nl ++ insrts, key)
  where
    remapLhs = map . remapConn
    remapConn m (Connection g o) = Connection g (M.findWithDefault o o m)
    sortConns = sortWith (\(Connection _ (G.Wire _ id)) -> id)

-- The random algorithm for netlist encryption is relatively simple:
--   Select a random key. Then, for each bit in the key add a new gate.
--   If the bit is low, the new gate should be an XOR, otherwise XNOR.
--   Randomly select internal wires of the circuit. Set each as the output
--   of a given encryption gate. Replace the assignments of the internal
--   wire with a new wire. Input this new wire and the corresponding key
--   input to that gate.
--
--   TODO netlist simplification
random :: KeySize -> Netlist -> RVar Encryption
random k nl = do
  smp <- RE.sample k (inners nl)
  key <- replicateM k (RE.choice keyBit)
  let inserts = map (\b -> if b then G.Bin G.Xnor else G.Bin G.Xor) key
  let m = M.fromList $ zip smp keyWires
  let ncs = newConns smp inserts
  return $ encrypt m ncs key

  where
    keyBit  = [True, False]
    maxId   = maximum $ map (\(G.Wire _ (Identifier id)) -> id) $ wires nl
    (keyInputs, keyWires) =
      runIdFrom (maxId + 1)
        (do inputs <- replicateM k ((G.Wire G.Input) <$> buildId)
            wires  <- replicateM k ((G.Wire G.Inner) <$> buildId)
            return (inputs, wires))

    newConns :: [G.Wire] -> [G.Wire -> G.Wire -> G.NetGate] -> [Connection]
    newConns smp gates = zipWith4 newConn smp gates keyInputs keyWires

    newConn :: G.Wire -> (G.Wire -> G.Wire -> G.NetGate) -> G.Wire -> G.Wire -> Connection
    newConn smp gate keyInput keyWire = Connection (gate keyInput keyWire) smp

/* Password module */

//This is a wrapper for the property check
module top # (
	parameter PASSWORD = 1,
	parameter SENSITIVE_DATA = 3
)
(
	input wire i_clk,
	input wire i_rst,
	input wire i_pwd,
	output wire o_property
);

	reg [1:0] r_senstive;
	wire [1:0] received_data;

	assign o_property = ~(((i_pwd == PASSWORD) && (received_data == r_sensitive))
						|| ((i_pwd != PASSWORD) && (received_data != r_sensitive)));	


	always @ (posedge clk) 
	begin: r_sensitive
		if(i_rst) begin
			r_sensitive <= 0;
		end
		else begin
			r_sensitive <= SENSITIVE_DATA;
		end
	end


	password# (
		.PASSWORD(PASSWORD)
	) passwd
//	password passwd
	(
		.i_password(i_pwd),
		.i_sensitive(r_sensitive),
		.o_output(received_data)
	);

endmodule


module password 
# (
	parameter PASSWORD = 1
)
(
	input i_password,
	input wire[1:0] i_sensitive,
	output wire[1:0] o_output
);
	
	assign o_output = (i_password == PASSWORD)? i_sensitive : 1;
//	assign o_output = (i_password != PASSWORD)? i_sensitive : 0;
//	assign o_output = (i_password != 1)? i_sensitive : 0;
//	assign o_output = (i_password == 1)? i_sensitive : 0;

endmodule


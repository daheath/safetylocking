proc get_fanin {p f} {
	file delete -force $f
	foreach_in_collection itr [all_fanin -to $p -flat -only_cells -trace_arcs all] {
		query_object $itr >> $f 
	}
}

# Change the topmodule name to your design top module
set topModule "UBBKA_31_0_31_0"


set NoMessageList "VER-314"
set NoMessageList "$NoMessageList LINT-32"
set NoMessageList "$NoMessageList LINT-33"
set NoMessageList "$NoMessageList LINT-2"

suppress_message $NoMessageList

## Set the search path for libraries and files
set search_path [subst { . ./rtl ../../lib}]

## Set the libraries
set target_library_nvt "../../lib/NangateOpenCellLibrary_typical_ccs.db"
set target_library_hvt "../../lib/NangateOpenCellLibrary_slow_ccs.db"
set target_library_lvt "../../lib/NangateOpenCellLibrary_fast_ccs.db"
set target_library "$target_library_nvt $target_library_hvt $target_library_lvt"
set link_library "$target_library $synthetic_library"

define_design_lib WORK -path ./work

##  Read RTL verilog netlist
read_verilog -rtl UBBKA_31_0_31_0.v

# Analyzing the design
analyze -library WORK -format verilog {UBBKA_31_0_31_0.v}

set_dont_use {NangateOpenCellLibrary/FA*}
set_dont_use {NangateOpenCellLibrary/AOI*}
set_dont_use {NangateOpenCellLibrary/HA*}
set_dont_use {NangateOpenCellLibrary/OAI*}
set_dont_use {NangateOpenCellLibrary/MUX*}
set_dont_use {NangateOpenCellLibrary/CLK*}
set_dont_use {NangateOpenCellLibrary/SDFF*}
set_dont_use {NangateOpenCellLibrary/TLAT*}
set_dont_use {NangateOpenCellLibrary/DL*}

current_design UBBKA_31_0_31_0
## Set design rules
set_fix_multiple_port_nets -all -buffer_constants


## Timing constraing
set_max_delay -from [all_inputs] -to [all_outputs] 0.58


compile -map_effort high
check_design

## Change names before write
redirect change_names { change_names -hier -rule verilog -verbose }

#Creating SDF Files
write_sdf syn/${topModule}.mapped.fast.sdf

##  Write gate level verilog file for the design.
write -hi -format verilog -output syn/dc.${topModule}.gates.fanin.v

#all_fanin
get_fanin S[0] approximate_gates/all_fanin_s0.txt
get_fanin S[1] approximate_gates/all_fanin_s1.txt
get_fanin S[2] approximate_gates/all_fanin_s2.txt
get_fanin S[3] approximate_gates/all_fanin_s3.txt
get_fanin S[4] approximate_gates/all_fanin_s4.txt
get_fanin S[5] approximate_gates/all_fanin_s5.txt
get_fanin S[6] approximate_gates/all_fanin_s6.txt
get_fanin S[7] approximate_gates/all_fanin_s7.txt
get_fanin S[8] approximate_gates/all_fanin_s8.txt
get_fanin S[9] approximate_gates/all_fanin_s9.txt
get_fanin S[10] approximate_gates/all_fanin_s10.txt
get_fanin S[11] approximate_gates/all_fanin_s11.txt
get_fanin S[12] approximate_gates/all_fanin_s12.txt
get_fanin S[13] approximate_gates/all_fanin_s13.txt
get_fanin S[14] approximate_gates/all_fanin_s14.txt
get_fanin S[15] approximate_gates/all_fanin_s15.txt
get_fanin S[16] approximate_gates/all_fanin_s16.txt
get_fanin S[17] approximate_gates/all_fanin_s17.txt
get_fanin S[18] approximate_gates/all_fanin_s18.txt
get_fanin S[19] approximate_gates/all_fanin_s19.txt
get_fanin S[20] approximate_gates/all_fanin_s20.txt
get_fanin S[21] approximate_gates/all_fanin_s21.txt
get_fanin S[22] approximate_gates/all_fanin_s22.txt
get_fanin S[23] approximate_gates/all_fanin_s23.txt
get_fanin S[24] approximate_gates/all_fanin_s24.txt
get_fanin S[25] approximate_gates/all_fanin_s25.txt
get_fanin S[26] approximate_gates/all_fanin_s26.txt
get_fanin S[27] approximate_gates/all_fanin_s27.txt
get_fanin S[28] approximate_gates/all_fanin_s28.txt
get_fanin S[29] approximate_gates/all_fanin_s29.txt
get_fanin S[30] approximate_gates/all_fanin_s30.txt
get_fanin S[31] approximate_gates/all_fanin_s31.txt
get_fanin S[32] approximate_gates/all_fanin_s32.txt

#all_fanin -to S[0] -flat -only_cells -trace_arcs all > approximate_gates/all_fanin_s0.txt

## Generates the reports
#report_area              > log/${topModule}.fast.area
#report_cell              > log/${topModule}.fast.cell
#report_hierarchy         > log/${topModule}.fast.hier
#report_net               > log/${topModule}.fast.net
#report_power             > log/${topModule}.fast.pow
#report_timing -nworst 50 > log/${topModule}.fast.tim

# Final checks
check_timing
check_design

quit

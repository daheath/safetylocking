# Change the topmodule name to your design top module
set topModule "UBBKA_31_0_31_0"


set NoMessageList "VER-314"
set NoMessageList "$NoMessageList LINT-32"
set NoMessageList "$NoMessageList LINT-33"
set NoMessageList "$NoMessageList LINT-2"

suppress_message $NoMessageList

## Set the search path for libraries and files
set search_path [subst { . ./rtl ../../lib}]

## Set the libraries
set target_library_nvt "../../lib/NangateOpenCellLibrary_typical_ccs.db"
set target_library_hvt "../../lib/NangateOpenCellLibrary_slow_ccs.db"
set target_library_lvt "../../lib/NangateOpenCellLibrary_fast_ccs.db"
set target_library "$target_library_nvt $target_library_hvt $target_library_lvt"
set link_library "$target_library $synthetic_library"

define_design_lib WORK -path ./work

##  Read RTL verilog netlist
read_verilog -rtl UBBKA_31_0_31_0.v

# Analyzing the design
analyze -library WORK -format verilog {UBBKA_31_0_31_0.v}

set_dont_use {NangateOpenCellLibrary/FA*}
set_dont_use {NangateOpenCellLibrary/AOI*}
set_dont_use {NangateOpenCellLibrary/HA*}
set_dont_use {NangateOpenCellLibrary/OAI*}
set_dont_use {NangateOpenCellLibrary/MUX*}
set_dont_use {NangateOpenCellLibrary/CLK*}
set_dont_use {NangateOpenCellLibrary/SDFF*}
set_dont_use {NangateOpenCellLibrary/TLAT*}
set_dont_use {NangateOpenCellLibrary/DL*}


current_design UBBKA_31_0_31_0

## Set design rules
set_fix_multiple_port_nets -all -buffer_constants



compile -map_effort high
check_design

## Change names before write
redirect change_names { change_names -hier -rule verilog -verbose }

#Creating SDF Files
write_sdf syn/${topModule}.mapped.unconstrained.sdf

## Generates the reports
report_area              > log/${topModule}.unconstrained.area
report_cell              > log/${topModule}.unconstrained.cell
report_hierarchy         > log/${topModule}.unconstrained.hier
report_net               > log/${topModule}.unconstrained.net
report_power             > log/${topModule}.unconstrained.pow
report_timing -nworst 50 > log/${topModule}.unconstrained.tim

##  Write gate level verilog file for the design.
write -hi -format verilog -output syn/dc.${topModule}.gates.unconstrained.v


# Final checks
check_timing
check_design

quit

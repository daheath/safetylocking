
module GPGenerator_0 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module CarryOperator_0 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module GPGenerator_1 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_2 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_3 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_4 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_5 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_6 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_7 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_8 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_9 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_10 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_11 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_12 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_13 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_14 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_15 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_16 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_17 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_18 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_19 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_20 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_21 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_22 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_23 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_24 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_25 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_26 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_27 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_28 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_29 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_30 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module GPGenerator_31 ( Go, Po, A, B );
  input A, B;
  output Go, Po;


  XOR2_X1 U1 ( .A(B), .B(A), .Z(Po) );
  AND2_X1 U2 ( .A1(B), .A2(A), .ZN(Go) );
endmodule


module CarryOperator_1 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_2 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_3 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_4 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_5 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_6 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_7 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_8 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_9 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_10 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_11 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_12 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_13 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_14 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_15 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_16 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_17 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_18 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_19 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_20 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_21 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_22 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_23 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_24 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_25 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_26 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_27 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_28 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_29 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_30 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_31 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_32 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_33 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_34 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_35 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_36 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_37 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_38 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_39 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_40 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_41 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_42 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_43 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_44 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_45 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_46 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_47 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_48 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_49 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_50 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_51 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_52 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_53 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_54 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_55 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module CarryOperator_56 ( Go, Po, Gi1, Pi1, Gi2, Pi2 );
  input Gi1, Pi1, Gi2, Pi2;
  output Go, Po;
  wire   n1;

  AND2_X1 U1 ( .A1(Pi2), .A2(Pi1), .ZN(Po) );
  OR2_X1 U2 ( .A1(n1), .A2(Gi1), .ZN(Go) );
  AND2_X1 U3 ( .A1(Gi2), .A2(Pi1), .ZN(n1) );
endmodule


module UBPriBKA_31_0 ( S, X, Y, Cin );
  output [32:0] S;
  input [31:0] X;
  input [31:0] Y;
  input Cin;
  wire   P0_31_, P0_29, P0_27, P0_25, P0_23, P0_21, P0_19, P0_17, P0_15, P0_13,
         P0_11, P0_9, P0_7, P0_5, P0_3, P0_1, G0_31_, G0_29, G0_27, G0_25,
         G0_23, G0_21, G0_19, G0_17, G0_15, G0_13, G0_11, G0_9, G0_7, G0_5,
         G0_3, G0_1, P2_31_, P2_27, P2_23, P2_19, P2_15, P2_11, P2_7, P2_3,
         G2_31_, G2_27, G2_23, G2_19, G2_15, G2_11, G2_7, G2_3, P3_31_, P3_23,
         P3_15, P3_7, G3_31_, G3_23, G3_15, G3_7, P4_31_, P4_15, G4_31_, G4_15,
         P5_31_, G5_31_, P7_23_, G7_23_, P8_27_, P8_19, P8_11, G8_27_, G8_19,
         G8_11, P9_29_, P9_25, P9_21, P9_17, P9_13, P9_9, P9_5, G9_29_, G9_25,
         G9_21, G9_17, G9_13, G9_9, G9_5, P10_30_, P10_28, P10_26, P10_24,
         P10_22, P10_20, P10_18, P10_16, P10_14, P10_12, P10_10, P10_8, P10_6,
         P10_4, P10_2, G10_30_, G10_28, G10_26, G10_24, G10_22, G10_20, G10_18,
         G10_16, G10_14, G10_12, G10_10, G10_8, G10_6, G10_4, G10_2, n1, n2,
         n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17,
         n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31,
         n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45,
         n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59,
         n60, n61, n62, n63;
  wire   [31:0] P1;
  wire   [31:0] G1;

  GPGenerator_0 U0 ( .Go(G1[0]), .Po(P1[0]), .A(X[0]), .B(Y[0]) );
  GPGenerator_31 U1 ( .Go(G0_1), .Po(P0_1), .A(X[1]), .B(Y[1]) );
  GPGenerator_30 U2 ( .Go(G1[2]), .Po(P1[2]), .A(X[2]), .B(Y[2]) );
  GPGenerator_29 U3 ( .Go(G0_3), .Po(P0_3), .A(X[3]), .B(Y[3]) );
  GPGenerator_28 U4 ( .Go(G1[4]), .Po(P1[4]), .A(X[4]), .B(Y[4]) );
  GPGenerator_27 U5 ( .Go(G0_5), .Po(P0_5), .A(X[5]), .B(Y[5]) );
  GPGenerator_26 U6 ( .Go(G1[6]), .Po(P1[6]), .A(X[6]), .B(Y[6]) );
  GPGenerator_25 U7 ( .Go(G0_7), .Po(P0_7), .A(X[7]), .B(Y[7]) );
  GPGenerator_24 U8 ( .Go(G1[8]), .Po(P1[8]), .A(X[8]), .B(Y[8]) );
  GPGenerator_23 U9 ( .Go(G0_9), .Po(P0_9), .A(X[9]), .B(Y[9]) );
  GPGenerator_22 U10 ( .Go(G1[10]), .Po(P1[10]), .A(X[10]), .B(Y[10]) );
  GPGenerator_21 U11 ( .Go(G0_11), .Po(P0_11), .A(X[11]), .B(Y[11]) );
  GPGenerator_20 U12 ( .Go(G1[12]), .Po(P1[12]), .A(X[12]), .B(Y[12]) );
  GPGenerator_19 U13 ( .Go(G0_13), .Po(P0_13), .A(X[13]), .B(Y[13]) );
  GPGenerator_18 U14 ( .Go(G1[14]), .Po(P1[14]), .A(X[14]), .B(Y[14]) );
  GPGenerator_17 U15 ( .Go(G0_15), .Po(P0_15), .A(X[15]), .B(Y[15]) );
  GPGenerator_16 U16 ( .Go(G1[16]), .Po(P1[16]), .A(X[16]), .B(Y[16]) );
  GPGenerator_15 U17 ( .Go(G0_17), .Po(P0_17), .A(X[17]), .B(Y[17]) );
  GPGenerator_14 U18 ( .Go(G1[18]), .Po(P1[18]), .A(X[18]), .B(Y[18]) );
  GPGenerator_13 U19 ( .Go(G0_19), .Po(P0_19), .A(X[19]), .B(Y[19]) );
  GPGenerator_12 U20 ( .Go(G1[20]), .Po(P1[20]), .A(X[20]), .B(Y[20]) );
  GPGenerator_11 U21 ( .Go(G0_21), .Po(P0_21), .A(X[21]), .B(Y[21]) );
  GPGenerator_10 U22 ( .Go(G1[22]), .Po(P1[22]), .A(X[22]), .B(Y[22]) );
  GPGenerator_9 U23 ( .Go(G0_23), .Po(P0_23), .A(X[23]), .B(Y[23]) );
  GPGenerator_8 U24 ( .Go(G1[24]), .Po(P1[24]), .A(X[24]), .B(Y[24]) );
  GPGenerator_7 U25 ( .Go(G0_25), .Po(P0_25), .A(X[25]), .B(Y[25]) );
  GPGenerator_6 U26 ( .Go(G1[26]), .Po(P1[26]), .A(X[26]), .B(Y[26]) );
  GPGenerator_5 U27 ( .Go(G0_27), .Po(P0_27), .A(X[27]), .B(Y[27]) );
  GPGenerator_4 U28 ( .Go(G1[28]), .Po(P1[28]), .A(X[28]), .B(Y[28]) );
  GPGenerator_3 U29 ( .Go(G0_29), .Po(P0_29), .A(X[29]), .B(Y[29]) );
  GPGenerator_2 U30 ( .Go(G1[30]), .Po(P1[30]), .A(X[30]), .B(Y[30]) );
  GPGenerator_1 U31 ( .Go(G0_31_), .Po(P0_31_), .A(X[31]), .B(Y[31]) );
  CarryOperator_0 U32 ( .Go(G1[1]), .Po(P1[1]), .Gi1(G0_1), .Pi1(P0_1), .Gi2(
        G1[0]), .Pi2(P1[0]) );
  CarryOperator_56 U33 ( .Go(G1[3]), .Po(P1[3]), .Gi1(G0_3), .Pi1(P0_3), .Gi2(
        G1[2]), .Pi2(P1[2]) );
  CarryOperator_55 U34 ( .Go(G1[5]), .Po(P1[5]), .Gi1(G0_5), .Pi1(P0_5), .Gi2(
        G1[4]), .Pi2(P1[4]) );
  CarryOperator_54 U35 ( .Go(G1[7]), .Po(P1[7]), .Gi1(G0_7), .Pi1(P0_7), .Gi2(
        G1[6]), .Pi2(P1[6]) );
  CarryOperator_53 U36 ( .Go(G1[9]), .Po(P1[9]), .Gi1(G0_9), .Pi1(P0_9), .Gi2(
        G1[8]), .Pi2(P1[8]) );
  CarryOperator_52 U37 ( .Go(G1[11]), .Po(P1[11]), .Gi1(G0_11), .Pi1(P0_11), 
        .Gi2(G1[10]), .Pi2(P1[10]) );
  CarryOperator_51 U38 ( .Go(G1[13]), .Po(P1[13]), .Gi1(G0_13), .Pi1(P0_13), 
        .Gi2(G1[12]), .Pi2(P1[12]) );
  CarryOperator_50 U39 ( .Go(G1[15]), .Po(P1[15]), .Gi1(G0_15), .Pi1(P0_15), 
        .Gi2(G1[14]), .Pi2(P1[14]) );
  CarryOperator_49 U40 ( .Go(G1[17]), .Po(P1[17]), .Gi1(G0_17), .Pi1(P0_17), 
        .Gi2(G1[16]), .Pi2(P1[16]) );
  CarryOperator_48 U41 ( .Go(G1[19]), .Po(P1[19]), .Gi1(G0_19), .Pi1(P0_19), 
        .Gi2(G1[18]), .Pi2(P1[18]) );
  CarryOperator_47 U42 ( .Go(G1[21]), .Po(P1[21]), .Gi1(G0_21), .Pi1(P0_21), 
        .Gi2(G1[20]), .Pi2(P1[20]) );
  CarryOperator_46 U43 ( .Go(G1[23]), .Po(P1[23]), .Gi1(G0_23), .Pi1(P0_23), 
        .Gi2(G1[22]), .Pi2(P1[22]) );
  CarryOperator_45 U44 ( .Go(G1[25]), .Po(P1[25]), .Gi1(G0_25), .Pi1(P0_25), 
        .Gi2(G1[24]), .Pi2(P1[24]) );
  CarryOperator_44 U45 ( .Go(G1[27]), .Po(P1[27]), .Gi1(G0_27), .Pi1(P0_27), 
        .Gi2(G1[26]), .Pi2(P1[26]) );
  CarryOperator_43 U46 ( .Go(G1[29]), .Po(P1[29]), .Gi1(G0_29), .Pi1(P0_29), 
        .Gi2(G1[28]), .Pi2(P1[28]) );
  CarryOperator_42 U47 ( .Go(G1[31]), .Po(P1[31]), .Gi1(G0_31_), .Pi1(P0_31_), 
        .Gi2(G1[30]), .Pi2(P1[30]) );
  CarryOperator_41 U48 ( .Go(G2_3), .Po(P2_3), .Gi1(G1[3]), .Pi1(P1[3]), .Gi2(
        G1[1]), .Pi2(P1[1]) );
  CarryOperator_40 U49 ( .Go(G2_7), .Po(P2_7), .Gi1(G1[7]), .Pi1(P1[7]), .Gi2(
        G1[5]), .Pi2(P1[5]) );
  CarryOperator_39 U50 ( .Go(G2_11), .Po(P2_11), .Gi1(G1[11]), .Pi1(P1[11]), 
        .Gi2(G1[9]), .Pi2(P1[9]) );
  CarryOperator_38 U51 ( .Go(G2_15), .Po(P2_15), .Gi1(G1[15]), .Pi1(P1[15]), 
        .Gi2(G1[13]), .Pi2(P1[13]) );
  CarryOperator_37 U52 ( .Go(G2_19), .Po(P2_19), .Gi1(G1[19]), .Pi1(P1[19]), 
        .Gi2(G1[17]), .Pi2(P1[17]) );
  CarryOperator_36 U53 ( .Go(G2_23), .Po(P2_23), .Gi1(G1[23]), .Pi1(P1[23]), 
        .Gi2(G1[21]), .Pi2(P1[21]) );
  CarryOperator_35 U54 ( .Go(G2_27), .Po(P2_27), .Gi1(G1[27]), .Pi1(P1[27]), 
        .Gi2(G1[25]), .Pi2(P1[25]) );
  CarryOperator_34 U55 ( .Go(G2_31_), .Po(P2_31_), .Gi1(G1[31]), .Pi1(P1[31]), 
        .Gi2(G1[29]), .Pi2(P1[29]) );
  CarryOperator_33 U56 ( .Go(G3_7), .Po(P3_7), .Gi1(G2_7), .Pi1(P2_7), .Gi2(
        G2_3), .Pi2(P2_3) );
  CarryOperator_32 U57 ( .Go(G3_15), .Po(P3_15), .Gi1(G2_15), .Pi1(P2_15), 
        .Gi2(G2_11), .Pi2(P2_11) );
  CarryOperator_31 U58 ( .Go(G3_23), .Po(P3_23), .Gi1(G2_23), .Pi1(P2_23), 
        .Gi2(G2_19), .Pi2(P2_19) );
  CarryOperator_30 U59 ( .Go(G3_31_), .Po(P3_31_), .Gi1(G2_31_), .Pi1(P2_31_), 
        .Gi2(G2_27), .Pi2(P2_27) );
  CarryOperator_29 U60 ( .Go(G4_15), .Po(P4_15), .Gi1(G3_15), .Pi1(P3_15), 
        .Gi2(G3_7), .Pi2(P3_7) );
  CarryOperator_28 U61 ( .Go(G4_31_), .Po(P4_31_), .Gi1(G3_31_), .Pi1(P3_31_), 
        .Gi2(G3_23), .Pi2(P3_23) );
  CarryOperator_27 U62 ( .Go(G5_31_), .Po(P5_31_), .Gi1(G4_31_), .Pi1(P4_31_), 
        .Gi2(G4_15), .Pi2(P4_15) );
  CarryOperator_26 U63 ( .Go(G7_23_), .Po(P7_23_), .Gi1(G3_23), .Pi1(P3_23), 
        .Gi2(G4_15), .Pi2(P4_15) );
  CarryOperator_25 U64 ( .Go(G8_11), .Po(P8_11), .Gi1(G2_11), .Pi1(P2_11), 
        .Gi2(G3_7), .Pi2(P3_7) );
  CarryOperator_24 U65 ( .Go(G8_19), .Po(P8_19), .Gi1(G2_19), .Pi1(P2_19), 
        .Gi2(G4_15), .Pi2(P4_15) );
  CarryOperator_23 U66 ( .Go(G8_27_), .Po(P8_27_), .Gi1(G2_27), .Pi1(P2_27), 
        .Gi2(G7_23_), .Pi2(P7_23_) );
  CarryOperator_22 U67 ( .Go(G9_5), .Po(P9_5), .Gi1(G1[5]), .Pi1(P1[5]), .Gi2(
        G2_3), .Pi2(P2_3) );
  CarryOperator_21 U68 ( .Go(G9_9), .Po(P9_9), .Gi1(G1[9]), .Pi1(P1[9]), .Gi2(
        G3_7), .Pi2(P3_7) );
  CarryOperator_20 U69 ( .Go(G9_13), .Po(P9_13), .Gi1(G1[13]), .Pi1(P1[13]), 
        .Gi2(G8_11), .Pi2(P8_11) );
  CarryOperator_19 U70 ( .Go(G9_17), .Po(P9_17), .Gi1(G1[17]), .Pi1(P1[17]), 
        .Gi2(G4_15), .Pi2(P4_15) );
  CarryOperator_18 U71 ( .Go(G9_21), .Po(P9_21), .Gi1(G1[21]), .Pi1(P1[21]), 
        .Gi2(G8_19), .Pi2(P8_19) );
  CarryOperator_17 U72 ( .Go(G9_25), .Po(P9_25), .Gi1(G1[25]), .Pi1(P1[25]), 
        .Gi2(G7_23_), .Pi2(P7_23_) );
  CarryOperator_16 U73 ( .Go(G9_29_), .Po(P9_29_), .Gi1(G1[29]), .Pi1(P1[29]), 
        .Gi2(G8_27_), .Pi2(P8_27_) );
  CarryOperator_15 U74 ( .Go(G10_2), .Po(P10_2), .Gi1(G1[2]), .Pi1(P1[2]), 
        .Gi2(G1[1]), .Pi2(P1[1]) );
  CarryOperator_14 U75 ( .Go(G10_4), .Po(P10_4), .Gi1(G1[4]), .Pi1(P1[4]), 
        .Gi2(G2_3), .Pi2(P2_3) );
  CarryOperator_13 U76 ( .Go(G10_6), .Po(P10_6), .Gi1(G1[6]), .Pi1(P1[6]), 
        .Gi2(G9_5), .Pi2(P9_5) );
  CarryOperator_12 U77 ( .Go(G10_8), .Po(P10_8), .Gi1(G1[8]), .Pi1(P1[8]), 
        .Gi2(G3_7), .Pi2(P3_7) );
  CarryOperator_11 U78 ( .Go(G10_10), .Po(P10_10), .Gi1(G1[10]), .Pi1(P1[10]), 
        .Gi2(G9_9), .Pi2(P9_9) );
  CarryOperator_10 U79 ( .Go(G10_12), .Po(P10_12), .Gi1(G1[12]), .Pi1(P1[12]), 
        .Gi2(G8_11), .Pi2(P8_11) );
  CarryOperator_9 U80 ( .Go(G10_14), .Po(P10_14), .Gi1(G1[14]), .Pi1(P1[14]), 
        .Gi2(G9_13), .Pi2(P9_13) );
  CarryOperator_8 U81 ( .Go(G10_16), .Po(P10_16), .Gi1(G1[16]), .Pi1(P1[16]), 
        .Gi2(G4_15), .Pi2(P4_15) );
  CarryOperator_7 U82 ( .Go(G10_18), .Po(P10_18), .Gi1(G1[18]), .Pi1(P1[18]), 
        .Gi2(G9_17), .Pi2(P9_17) );
  CarryOperator_6 U83 ( .Go(G10_20), .Po(P10_20), .Gi1(G1[20]), .Pi1(P1[20]), 
        .Gi2(G8_19), .Pi2(P8_19) );
  CarryOperator_5 U84 ( .Go(G10_22), .Po(P10_22), .Gi1(G1[22]), .Pi1(P1[22]), 
        .Gi2(G9_21), .Pi2(P9_21) );
  CarryOperator_4 U85 ( .Go(G10_24), .Po(P10_24), .Gi1(G1[24]), .Pi1(P1[24]), 
        .Gi2(G7_23_), .Pi2(P7_23_) );
  CarryOperator_3 U86 ( .Go(G10_26), .Po(P10_26), .Gi1(G1[26]), .Pi1(P1[26]), 
        .Gi2(G9_25), .Pi2(P9_25) );
  CarryOperator_2 U87 ( .Go(G10_28), .Po(P10_28), .Gi1(G1[28]), .Pi1(P1[28]), 
        .Gi2(G8_27_), .Pi2(P8_27_) );
  CarryOperator_1 U88 ( .Go(G10_30_), .Po(P10_30_), .Gi1(G1[30]), .Pi1(P1[30]), 
        .Gi2(G9_29_), .Pi2(P9_29_) );
  XNOR2_X1 U89 ( .A(P0_9), .B(n1), .ZN(S[9]) );
  NOR2_X1 U90 ( .A1(n2), .A2(G10_8), .ZN(n1) );
  AND2_X1 U91 ( .A1(P10_8), .A2(Cin), .ZN(n2) );
  XNOR2_X1 U92 ( .A(P1[8]), .B(n3), .ZN(S[8]) );
  NOR2_X1 U93 ( .A1(n4), .A2(G3_7), .ZN(n3) );
  AND2_X1 U94 ( .A1(P3_7), .A2(Cin), .ZN(n4) );
  XNOR2_X1 U95 ( .A(P0_7), .B(n5), .ZN(S[7]) );
  NOR2_X1 U96 ( .A1(n6), .A2(G10_6), .ZN(n5) );
  AND2_X1 U97 ( .A1(P10_6), .A2(Cin), .ZN(n6) );
  XNOR2_X1 U98 ( .A(P1[6]), .B(n7), .ZN(S[6]) );
  NOR2_X1 U99 ( .A1(n8), .A2(G9_5), .ZN(n7) );
  AND2_X1 U100 ( .A1(P9_5), .A2(Cin), .ZN(n8) );
  XNOR2_X1 U101 ( .A(P0_5), .B(n9), .ZN(S[5]) );
  NOR2_X1 U102 ( .A1(n10), .A2(G10_4), .ZN(n9) );
  AND2_X1 U103 ( .A1(P10_4), .A2(Cin), .ZN(n10) );
  XNOR2_X1 U104 ( .A(P1[4]), .B(n11), .ZN(S[4]) );
  NOR2_X1 U105 ( .A1(n12), .A2(G2_3), .ZN(n11) );
  AND2_X1 U106 ( .A1(P2_3), .A2(Cin), .ZN(n12) );
  XNOR2_X1 U107 ( .A(P0_3), .B(n13), .ZN(S[3]) );
  NOR2_X1 U108 ( .A1(n14), .A2(G10_2), .ZN(n13) );
  AND2_X1 U109 ( .A1(P10_2), .A2(Cin), .ZN(n14) );
  OR2_X1 U110 ( .A1(n15), .A2(G5_31_), .ZN(S[32]) );
  AND2_X1 U111 ( .A1(P5_31_), .A2(Cin), .ZN(n15) );
  XNOR2_X1 U112 ( .A(P0_31_), .B(n16), .ZN(S[31]) );
  NOR2_X1 U113 ( .A1(n17), .A2(G10_30_), .ZN(n16) );
  AND2_X1 U114 ( .A1(P10_30_), .A2(Cin), .ZN(n17) );
  XNOR2_X1 U115 ( .A(P1[30]), .B(n18), .ZN(S[30]) );
  NOR2_X1 U116 ( .A1(n19), .A2(G9_29_), .ZN(n18) );
  AND2_X1 U117 ( .A1(P9_29_), .A2(Cin), .ZN(n19) );
  XNOR2_X1 U118 ( .A(P1[2]), .B(n20), .ZN(S[2]) );
  NOR2_X1 U119 ( .A1(n21), .A2(G1[1]), .ZN(n20) );
  AND2_X1 U120 ( .A1(P1[1]), .A2(Cin), .ZN(n21) );
  XNOR2_X1 U121 ( .A(P0_29), .B(n22), .ZN(S[29]) );
  NOR2_X1 U122 ( .A1(n23), .A2(G10_28), .ZN(n22) );
  AND2_X1 U123 ( .A1(P10_28), .A2(Cin), .ZN(n23) );
  XNOR2_X1 U124 ( .A(P1[28]), .B(n24), .ZN(S[28]) );
  NOR2_X1 U125 ( .A1(n25), .A2(G8_27_), .ZN(n24) );
  AND2_X1 U126 ( .A1(P8_27_), .A2(Cin), .ZN(n25) );
  XNOR2_X1 U127 ( .A(P0_27), .B(n26), .ZN(S[27]) );
  NOR2_X1 U128 ( .A1(n27), .A2(G10_26), .ZN(n26) );
  AND2_X1 U129 ( .A1(P10_26), .A2(Cin), .ZN(n27) );
  XNOR2_X1 U130 ( .A(P1[26]), .B(n28), .ZN(S[26]) );
  NOR2_X1 U131 ( .A1(n29), .A2(G9_25), .ZN(n28) );
  AND2_X1 U132 ( .A1(P9_25), .A2(Cin), .ZN(n29) );
  XNOR2_X1 U133 ( .A(P0_25), .B(n30), .ZN(S[25]) );
  NOR2_X1 U134 ( .A1(n31), .A2(G10_24), .ZN(n30) );
  AND2_X1 U135 ( .A1(P10_24), .A2(Cin), .ZN(n31) );
  XNOR2_X1 U136 ( .A(P1[24]), .B(n32), .ZN(S[24]) );
  NOR2_X1 U137 ( .A1(n33), .A2(G7_23_), .ZN(n32) );
  AND2_X1 U138 ( .A1(P7_23_), .A2(Cin), .ZN(n33) );
  XNOR2_X1 U139 ( .A(P0_23), .B(n34), .ZN(S[23]) );
  NOR2_X1 U140 ( .A1(n35), .A2(G10_22), .ZN(n34) );
  AND2_X1 U141 ( .A1(P10_22), .A2(Cin), .ZN(n35) );
  XNOR2_X1 U142 ( .A(P1[22]), .B(n36), .ZN(S[22]) );
  NOR2_X1 U143 ( .A1(n37), .A2(G9_21), .ZN(n36) );
  AND2_X1 U144 ( .A1(P9_21), .A2(Cin), .ZN(n37) );
  XNOR2_X1 U145 ( .A(P0_21), .B(n38), .ZN(S[21]) );
  NOR2_X1 U146 ( .A1(n39), .A2(G10_20), .ZN(n38) );
  AND2_X1 U147 ( .A1(P10_20), .A2(Cin), .ZN(n39) );
  XNOR2_X1 U148 ( .A(P1[20]), .B(n40), .ZN(S[20]) );
  NOR2_X1 U149 ( .A1(n41), .A2(G8_19), .ZN(n40) );
  AND2_X1 U150 ( .A1(P8_19), .A2(Cin), .ZN(n41) );
  XNOR2_X1 U151 ( .A(P0_1), .B(n42), .ZN(S[1]) );
  NOR2_X1 U152 ( .A1(n43), .A2(G1[0]), .ZN(n42) );
  AND2_X1 U153 ( .A1(Cin), .A2(P1[0]), .ZN(n43) );
  XNOR2_X1 U154 ( .A(P0_19), .B(n44), .ZN(S[19]) );
  NOR2_X1 U155 ( .A1(n45), .A2(G10_18), .ZN(n44) );
  AND2_X1 U156 ( .A1(P10_18), .A2(Cin), .ZN(n45) );
  XNOR2_X1 U157 ( .A(P1[18]), .B(n46), .ZN(S[18]) );
  NOR2_X1 U158 ( .A1(n47), .A2(G9_17), .ZN(n46) );
  AND2_X1 U159 ( .A1(P9_17), .A2(Cin), .ZN(n47) );
  XNOR2_X1 U160 ( .A(P0_17), .B(n48), .ZN(S[17]) );
  NOR2_X1 U161 ( .A1(n49), .A2(G10_16), .ZN(n48) );
  AND2_X1 U162 ( .A1(P10_16), .A2(Cin), .ZN(n49) );
  XNOR2_X1 U163 ( .A(P1[16]), .B(n50), .ZN(S[16]) );
  NOR2_X1 U164 ( .A1(n51), .A2(G4_15), .ZN(n50) );
  AND2_X1 U165 ( .A1(P4_15), .A2(Cin), .ZN(n51) );
  XNOR2_X1 U166 ( .A(P0_15), .B(n52), .ZN(S[15]) );
  NOR2_X1 U167 ( .A1(n53), .A2(G10_14), .ZN(n52) );
  AND2_X1 U168 ( .A1(P10_14), .A2(Cin), .ZN(n53) );
  XNOR2_X1 U169 ( .A(P1[14]), .B(n54), .ZN(S[14]) );
  NOR2_X1 U170 ( .A1(n55), .A2(G9_13), .ZN(n54) );
  AND2_X1 U171 ( .A1(P9_13), .A2(Cin), .ZN(n55) );
  XNOR2_X1 U172 ( .A(P0_13), .B(n56), .ZN(S[13]) );
  NOR2_X1 U173 ( .A1(n57), .A2(G10_12), .ZN(n56) );
  AND2_X1 U174 ( .A1(P10_12), .A2(Cin), .ZN(n57) );
  XNOR2_X1 U175 ( .A(P1[12]), .B(n58), .ZN(S[12]) );
  NOR2_X1 U176 ( .A1(n59), .A2(G8_11), .ZN(n58) );
  AND2_X1 U177 ( .A1(P8_11), .A2(Cin), .ZN(n59) );
  XNOR2_X1 U178 ( .A(P0_11), .B(n60), .ZN(S[11]) );
  NOR2_X1 U179 ( .A1(n61), .A2(G10_10), .ZN(n60) );
  AND2_X1 U180 ( .A1(P10_10), .A2(Cin), .ZN(n61) );
  XNOR2_X1 U181 ( .A(P1[10]), .B(n62), .ZN(S[10]) );
  NOR2_X1 U182 ( .A1(n63), .A2(G9_9), .ZN(n62) );
  AND2_X1 U183 ( .A1(P9_9), .A2(Cin), .ZN(n63) );
  XOR2_X1 U184 ( .A(P1[0]), .B(Cin), .Z(S[0]) );
endmodule


module UBPureBKA_31_0 ( S, X, Y );
  output [32:0] S;
  input [31:0] X;
  input [31:0] Y;


  UBPriBKA_31_0 U0 ( .S(S), .X(X), .Y(Y), .Cin(1'b0) );
endmodule


module UBBKA_31_0_31_0 ( S, X, Y );
  output [32:0] S;
  input [31:0] X;
  input [31:0] Y;


  UBPureBKA_31_0 U0 ( .S(S), .X(X), .Y(Y) );
endmodule


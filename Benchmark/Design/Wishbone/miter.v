
/*
	Target module: Wishbone flash controller

	
	Policy:
		level {L,H};
		order {L<H};
		set flash_dat_i as H;
		set wb_dat_o as L;

		if(wb_we_i == 1)
			allow flash_dat_i -> wb_dat_o;


*/

module miter #(
	parameter	RESET_VALUE		=	0
)
(
	input	wire							i_clk,
	input	wire							i_rst,

	//Inputs for the wb_flash modules
	input	wire	[18	:	0]	wb_adr_i_1,
	input	wire	[18	:	0]	wb_adr_i_2,
	input	wire	[31	:	0]	wb_dat_i_1,
	input	wire	[31	:	0]	wb_dat_i_2,
	input	wire	[3	:	0]	wb_sel_i_1,
	input	wire	[3	:	0]	wb_sel_i_2,
	input	wire				wb_we_i_1,
	input	wire				wb_we_i_2,
	input	wire				wb_stb_i_1,
	input	wire				wb_stb_i_2,
	input	wire				wb_cyc_i_1,
	input	wire				wb_cyc_i_2,
	input	wire	[7	:	0]	flash_dat_i_1,
	input	wire	[7	:	0]	flash_dat_i_2,

	output	wire							o_property

);
	

	wire	[31	:	0]	wb_dat_o_1, wb_dat_o_2;
	wire				wb_ack_o_1,	wb_ack_o_2;
	wire	[18	:	0]	flash_adr_o_1,	flash_adr_o_2;
	wire	[7	:	0]	flash_dat_o_1,	flash_dat_o_2;
	wire				flash_oe, flash_ce, flash_we;


	reg p1;

	/* Property Logic */

	always @(*)
	begin: PROPERTY_LOGIC_1
		//1. Other inputs remain same
		if(	(wb_adr_i_1 == wb_adr_i_2)
			&&	(wb_dat_i_1	==	wb_dat_i_2)
			&&	(wb_sel_i_1	==	wb_sel_i_2)
			&&	(wb_we_i_1	==	wb_we_i_2)
			&&	(wb_stb_i_1	==	wb_stb_i_2)
			&&	(wb_cyc_i_1	==	wb_cyc_i_2)
		) begin
			
			if(flash_dat_i_1 != flash_dat_i_2) begin
				p1 = (wb_dat_o_1 == wb_dat_o_2);
			end
			else begin
				p1 = 1;
			end
		end
		else begin
			p1 = 1;
		end
	end

	assign	o_property = ~p1;


	/* Input storing logic */


	/* Module under verification */
	wb_flash f1 (
		.clk_i			(i_clk			),
		.nrst_i			(~i_rst			),
		.wb_adr_i		(wb_adr_i_1		),
		.wb_dat_o		(wb_dat_o_1		),
		.wb_dat_i		(wb_dat_i_1		),
		.wb_sel_i		(wb_sel_i_1		),
		.wb_we_i		(wb_we_i_1		),
		.wb_stb_i		(wb_stb_i_1		),
		.wb_cyc_i		(wb_cyc_i_1		),
		.wb_ack_o		(wb_ack_o_1		),
		.flash_adr_o	(flash_adr_o_1	),
		.flash_dat_o	(flash_dat_o_1	),
		.flash_dat_i	(flash_dat_i_1	),
		.flash_oe		(flash_oe_1		),
		.flash_ce		(flash_ce_1		),
		.flash_we		(flash_we_1		)
	);

	wb_flash f2 (
		.clk_i			(i_clk			),
		.nrst_i			(~i_rst			),
		.wb_adr_i		(wb_adr_i_2		),
		.wb_dat_o		(wb_dat_o_2		),
		.wb_dat_i		(wb_dat_i_2		),
		.wb_sel_i		(wb_sel_i_2		),
		.wb_we_i		(wb_we_i_2		),
		.wb_stb_i		(wb_stb_i_2		),
		.wb_cyc_i		(wb_cyc_i_2		),
		.wb_ack_o		(wb_ack_o_2		),
		.flash_adr_o	(flash_adr_o_2	),
		.flash_dat_o	(flash_dat_o_2	),
		.flash_dat_i	(flash_dat_i_2	),
		.flash_oe		(flash_oe_2		),
		.flash_ce		(flash_ce_2		),
		.flash_we		(flash_we_2		)
	);

endmodule

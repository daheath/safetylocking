
/*
	Target module: Stone_Kogge adder

	For readability, I renamed
		X -> i_sensitive
		Y -> i_normal
		S -> o_output

	Policy:

		level {L,H}
		order {L<H}

		set i_sensitive as H;
		set i_normal as L;
		set o_output as L;

*/

module miter #(
	parameter	DATA_WIDTH	=	32,
	parameter	RESET_VALUE	=	1
)
(
	input							i_clk,
	input							i_rst,
	input	[DATA_WIDTH-1	:	0]	i_sensitive,
	input	[DATA_WIDTH-1	:	0]	i_normal,
	output							o_property
);
	
	wire	[DATA_WIDTH		:	0]	o_output;

	reg		[DATA_WIDTH-1	:	0]	input_record_i_sensitive;
	reg		[DATA_WIDTH-1	:	0]	input_record_i_normal;
	reg		[DATA_WIDTH-1	:	0]	output_record_o_output;



	reg p1;

	/* Property Logic */
	always @(*)
	begin: PROPERTY_LOGIC_1
		if(input_record_i_normal == i_normal) begin				//Other values are fixed
			if(input_record_i_sensitive != i_sensitive) begin	//Sensitive value varies
				if(output_record_o_output == o_output) begin	//Output should not change
					p1 = 1;
				end
				else begin
					p1 = 0;
				end
			end
			else begin
				p1 = 1;
			end
		end
		else begin
			p1 = 1;
		end
	end

	assign	o_property = ~p1;

	/* Input/output storing logic */

	always@(posedge i_clk) 
	begin:	INPUT_RECORD_I_SENSITIVE
		if(i_rst) begin
			input_record_i_sensitive <= RESET_VALUE;
		end
		else begin
			input_record_i_sensitive <= i_sensitive;
		end
	end

	always@(posedge i_clk) 
	begin:	INPUT_RECORD_I_NORMAL
		if(i_rst) begin
			input_record_i_normal <= RESET_VALUE;
		end
		else begin
			input_record_i_normal <= i_normal;
		end
	end

	always@(posedge i_clk) 
	begin:	INPUT_RECORD_O_OUTPUT
		if(i_rst) begin
			output_record_o_output <= RESET_VALUE;
		end
		else begin
			output_record_o_output <= o_output;
		end
	end

	/* Module under verification */

	UBKSA_31_0_31_0 stone_kogge (
		.S(o_output),
		.X(i_sensitive),
		.Y(i_normal)
	);

endmodule

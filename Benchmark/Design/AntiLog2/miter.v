
/*
	Target module: Reed_Solomon_Decoder
	
	Policy:
		Level {L,H}
		order {L<H};
		set DIN as H;
		set DOUT as L;

		allow DIN[-2] -> DOUT
*/

module miter #(
	parameter	INPUT_WIDTH		=	9,
	parameter	OUTPUT_WIDTH	=	24,
	parameter	RESET_VALUE		=	0
)
(
	input	wire							i_clk,
	input	wire							i_rst,

	/* DIN */
	input	wire	[INPUT_WIDTH-1	:	0]	i_DIN_0,
	input	wire	[INPUT_WIDTH-1	:	0]	i_DIN_1,

	output	wire							o_property
);
	localparam	MAX_TIME_INDEX = 2;
	localparam	TARGET_TIME_INDEX = 2;


	wire [OUTPUT_WIDTH-1:	0]	o_DOUT_0;
	wire [OUTPUT_WIDTH-1:	0]	o_DOUT_1;

	reg	[INPUT_WIDTH-1	:	0]	ir_DIN_0[1:MAX_TIME_INDEX];
	reg	[INPUT_WIDTH-1	:	0]	ir_DIN_1[1:MAX_TIME_INDEX];

	reg p1=1;

	/* Property Logics */
	always @(*)
	begin:PROPERTY_LOGIC
		if(ir_DIN_0[TARGET_TIME_INDEX] == ir_DIN_1[TARGET_TIME_INDEX]) begin
			p1 = (o_DOUT_0 == o_DOUT_1);
		end
		else begin
			p1 = 1;
		end
	end

	assign o_property = ~p1;



	/* Input Recording Registers */
	genvar i;
	generate
	for(i=1; i<MAX_TIME_INDEX+1; i=i+1) begin

		always@(posedge i_clk)
		begin: INPUT_STORING
			if(i_rst) begin
				ir_DIN_0[i] <= RESET_VALUE;
				ir_DIN_1[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_DIN_0[i] <= i_DIN_0;
					ir_DIN_1[i] <= i_DIN_1;
				end
				else begin
					ir_DIN_0[i] <= ir_DIN_0[i-1];
					ir_DIN_1[i] <= ir_DIN_1[i-1];
				end
			end
		end
	end
	endgenerate


	AntiLog2 m0 (
		.clk		(i_clk			),
		.DIN		(i_DIN_0		),
		.DOUT		(o_DOUT_0		)
	);

	AntiLog2 m1 (
		.clk		(i_clk			),
		.DIN		(i_DIN_1		),
		.DOUT		(o_DOUT_1		)
	);


endmodule


/*
	Target module: UART
	
    input sys_clk,
    input sys_rst,

    input [13:0] csr_a,
    input csr_we,
    input [31:0] csr_di,
    output reg [31:0] csr_do,

    output rx_irq,
    output tx_irq,

    input uart_rx,
    output uart_tx	
	
	Policy:

		   default level H
		   set csr_do as L
		   order {L<H}

	       prohibit csr_di[2] => csr_do
	       
	Attacking the missing case
*/

module miter #(
    parameter   RESET_VALUE     =   0,
	parameter	CSR_A_WIDTH		=	14,
	parameter	CSR_DI_WIDTH	=	32,
    parameter   CSR_DO_WIDTH    =   32	
)
(
	input	wire							sys_clk,
    input   wire                            sys_rst,
	
    input   wire    [CSR_A_WIDTH-1  :   0]  csr_a_1,
    input   wire    [CSR_A_WIDTH-1  :   0]  csr_a_2,
    
    input   wire                            csr_we_1,
    input   wire                            csr_we_2,
    
    input   wire    [CSR_DI_WIDTH-1 :   0]  csr_di_1,
    input   wire    [CSR_DI_WIDTH-1 :   0]  csr_di_2,

    input   wire                            uart_rx_1,
    input   wire                            uart_rx_2,

	output	wire							o_property
);
	localparam	MAX_TIME_INDEX = 2;
	
    /* Output Ports */
    wire [31  :   0] csr_do_1;
    wire [31  :   0] csr_do_2;

    wire rx_irq_1;
    wire rx_irq_2;
    
    wire tx_irq_1;
    wire tx_irq_2;
    
    wire uart_tx_1;
    wire uart_tx_2;
      
    /* Input Value Registers */
    reg     [CSR_A_WIDTH-1  :   0]  ir_csr_a_1[1  :   MAX_TIME_INDEX];
    reg     [CSR_A_WIDTH-1  :   0]  ir_csr_a_2[1  :   MAX_TIME_INDEX];
    
    reg                             ir_csr_we_1[1  :   MAX_TIME_INDEX];
    reg                             ir_csr_we_2[1  :   MAX_TIME_INDEX];
    
    reg     [CSR_DI_WIDTH-1 :   0]  ir_csr_di_1[1  :   MAX_TIME_INDEX];
    reg     [CSR_DI_WIDTH-1 :   0]  ir_csr_di_2[1  :   MAX_TIME_INDEX];

    reg                             ir_uart_rx_1[1  :   MAX_TIME_INDEX];
    reg                             ir_uart_rx_2[1  :   MAX_TIME_INDEX];
      
	/* Property Logics */
    reg p1=1;

	always @(*)
	begin: PROPERTY_LOGIC
        if(ir_csr_a_1[2] == ir_csr_a_2[2]
            && ir_csr_a_1[1] == ir_csr_a_2[1]
            &&  ir_csr_we_1[2] == ir_csr_we_2[2]
            &&  ir_csr_we_1[1] == ir_csr_we_2[1]            
            &&  ir_csr_di_1[2] != ir_csr_di_2[2]
            &&  ir_csr_di_1[1] == ir_csr_di_2[1]            
            &&  ir_uart_rx_1[2] == ir_uart_rx_2[2]
            &&  ir_uart_rx_1[1] == ir_uart_rx_2[1]            
          )
        begin
            p1 = (csr_do_1 == csr_do_2);
        end
		else begin
			p1 = 1;
		end
	end

	assign o_property = ~p1;

    /* Input Recording Registers */
     
    always @(posedge sys_clk)
    begin: CSR_A
        if(sys_rst) begin
            ir_csr_a_1 [1] <= RESET_VALUE;
            ir_csr_a_1 [2] <= RESET_VALUE;
          
            ir_csr_a_2 [1] <= RESET_VALUE;
            ir_csr_a_2 [2] <= RESET_VALUE;
        end
        else begin
           ir_csr_a_1[1] <= csr_a_1;
           ir_csr_a_1[2] <= ir_csr_a_1[1];

           ir_csr_a_2[1] <= csr_a_2;
           ir_csr_a_2[2] <= ir_csr_a_2[1];
        end
    end
    
    always @(posedge sys_clk)
    begin: CSR_WE
        if(sys_rst) begin
            ir_csr_we_1 [1] <= RESET_VALUE;
            ir_csr_we_1 [2] <= RESET_VALUE;
          
            ir_csr_we_2 [1] <= RESET_VALUE;
            ir_csr_we_2 [2] <= RESET_VALUE;
        end
        else begin
           ir_csr_we_1[1] <= csr_we_1;
           ir_csr_we_1[2] <= ir_csr_we_1[1];

           ir_csr_we_2[1] <= csr_we_2;
           ir_csr_we_2[2] <= ir_csr_we_2[1];
        end
    end

    always @(posedge sys_clk)
    begin: CSR_DI
        if(sys_rst) begin
            ir_csr_di_1 [1] <= RESET_VALUE;
            ir_csr_di_1 [2] <= RESET_VALUE;
          
            ir_csr_we_2 [1] <= RESET_VALUE;
            ir_csr_we_2 [2] <= RESET_VALUE;
        end
        else begin
           ir_csr_di_1[1] <= csr_di_1;
           ir_csr_di_1[2] <= ir_csr_di_1[1];

           ir_csr_di_2[1] <= csr_di_2;
           ir_csr_di_2[2] <= ir_csr_di_2[1];
        end
    end
    
    always @(posedge sys_clk)
    begin: UART_RX
        if(sys_rst) begin
            ir_uart_rx_1 [1] <= RESET_VALUE;
            ir_uart_rx_1 [2] <= RESET_VALUE;
          
            ir_uart_rx_2 [1] <= RESET_VALUE;
            ir_uart_rx_2 [2] <= RESET_VALUE;
        end
        else begin
           ir_uart_rx_1[1] <= uart_rx_1;
           ir_uart_rx_1[2] <= ir_uart_rx_1[1];

           ir_uart_rx_2[1] <= uart_rx_2;
           ir_uart_rx_2[2] <= ir_uart_rx_2[1];
        end
    end
    
    uart uart_dut_1 (
        .sys_clk    (sys_clk    ),
        .sys_rst    (sys_rst    ),
        .csr_a      (csr_a_1    ),
        .csr_we     (csr_we_1   ),
        .csr_di     (csr_di_1   ),
        .csr_do     (csr_do_1   ),
        .rx_irq     (rx_irq_1   ),
        .tx_irq     (tx_irq_1   ),
        .uart_rx    (uart_rx_1  ),
        .uart_tx    (uart_tx_1  )
    );

    uart uart_dut_2 (
        .sys_clk    (sys_clk    ),
        .sys_rst    (sys_rst    ),
        .csr_a      (csr_a_1    ),
        .csr_we     (csr_we_2   ),
        .csr_di     (csr_di_2   ),
        .csr_do     (csr_do_2   ),
        .rx_irq     (rx_irq_2   ),
        .tx_irq     (tx_irq_2   ),
        .uart_rx    (uart_rx_2  ),
        .uart_tx    (uart_tx_2  )
    );
    
endmodule


/*
    Target module: ETHERNET
       
    Policy:
       level {L<H}
       default level H
           
       if(pkt_rx_data_1[1] != 0)
         prohibit pkt_rx_data_1[1] -> prk_rx_data_1[0]

           
    Attacking the retention data
*/

module miter #(
    parameter   RESET_VALUE     =   0,
    parameter   CSR_A_WIDTH     =   14,
    parameter   CSR_DI_WIDTH    =   32,
    parameter   CSR_DO_WIDTH    =   32  
)
(

    /* System inputs */    
    input                   clk_156m25,             // To rx_dq0 of rx_dequeue.v, ...
    input                   reset_156m25_n,         // To rx_dq0 of rx_dequeue.v, ...
    

    /* Fixed inputs */

	input [63:0]  rxdfifo_rdata,
	input [7:0]   rxdfifo_rstatus,
	input         rxdfifo_rempty,
	input         rxdfifo_ralmost_empty,

	input         pkt_rx_ren,

    output  wire            o_property
);
    localparam  MAX_TIME_INDEX = 6;
    localparam  TARGET_TIME_INDEX = 2;

    /* Output Ports */

	wire        rxdfifo_ren;

	wire [63:0] pkt_rx_data;
	wire        pkt_rx_val;
	wire        pkt_rx_sop;
	wire        pkt_rx_eop;
	wire        pkt_rx_err;
	wire [2:0]  pkt_rx_mod;
	wire        pkt_rx_avail;

	wire        status_rxdfifo_udflow_tog;




    /* Input Value Registers */
    reg  [63:0]           reg_pkt_rx_data;
      

	reg isReset = 0;

    always@(posedge clk_156m25)
    begin: IS_RESET	
    	if(~reset_156m25_n) begin
	    	isReset <= 1;
		end
	end

    /* Output Storing */
    always@(posedge clk_156m25)
    begin: OUTPUT_REGISTERS
      if(~reset_156m25_n) begin
        reg_pkt_rx_data <= 0;
      end
      else begin
        reg_pkt_rx_data <= pkt_rx_data;
      end
    end

    /* Property logic */
    reg property = 0;
    
    always@(*)
    begin: PROPERTY
      if(isReset && (pkt_rx_data == reg_pkt_rx_data)) begin
        property = (reg_pkt_rx_data == 0);
      end
      else begin
        property = 1;
      end
    end
    
    assign o_property = ~property;

rx_dequeue rx_deque(
	.clk_156m25					(clk_156m25		),
	.reset_156m25_n				(reset_156m25_n	),


	/* Inputs */
	.rxdfifo_rdata				(rxdfifo_rdata			),
	.rxdfifo_rstatus			(rxdfifo_rstatus		),
	.rxdfifo_rempty				(rxdfifo_rempty			),
	.rxdfifo_ralmost_empty		(rxdfifo_ralmost_empty	),
	.pkt_rx_ren					(pkt_rx_ren				),

	/* Outputs */

	.rxdfifo_ren				(rxdfifo_ren	),

	.pkt_rx_data				(pkt_rx_data	),
	.pkt_rx_val					(pkt_rx_val		),
	.pkt_rx_sop					(pkt_rx_sop		),
	.pkt_rx_eop					(pkt_rx_eop		),
	.pkt_rx_err					(pkt_rx_err		),
	.pkt_rx_mod					(pkt_rx_mod		),
	.pkt_rx_avail				(pkt_rx_avail	),

	.status_rxdfifo_udflow_tog	(status_rxdfifo_udflow_tog	)
);

   
    
endmodule

module Multiplier_31_0_3000_tb;
       reg [31:0] IN1;
       reg [31:0] IN2;
       wire [63:0] P;
       reg [63:0] C;
       reg clk, rst;


	initial begin
		$dumpfile("TB.vcd");
		$dumpvars(0,Multiplier_31_0_3000_tb);
	end

      initial begin
            clk = 0;
			rst = 1;

			#5
			rst = 0;
							
			IN1 = 0;
			IN2 = 0;
			#20
            
            repeat(10)
	       		begin
		          IN1 = $random; IN2 =$random; 
        	    assign C =$signed(IN1) * $signed(IN2);
			        #20;
              $display("IN1=%d,IN2=%d,P=%d,C=%d", IN1, IN2, P,C);
            end

            #1000 $finish;
      end

       always begin
        #5 clk = ~clk;
       end
       Multiplier_31_0_3000 M0(P, IN1, IN2, clk, rst);
endmodule
        

/*

	Policy

	level {L, H}
	order {L < H}
	set P as L;
	set IN1 as H;
	set IN2 as H;


	allow IN1[-1] -> P;
	allow IN2[-1] -> P;

*/
module miter (
	input	[31:0]	IN1_m1,
	input	[31:0]	IN1_m2,

	input	[31:0]	IN2_m1,
	input	[31:0]	IN2_m2,

	input	i_clk,
	input	i_rst,

	output	o_property
);

	localparam	RESET_VALUE = 0;
	localparam	MAX_INDEX = 2;

	wire	[63:0]	P_m1, P_m2;

	reg		[31:0]	ir_IN1_m1[1:MAX_INDEX];
	reg		[31:0]	ir_IN1_m2[1:MAX_INDEX];

	reg		[31:0]	ir_IN2_m1[1:MAX_INDEX];
	reg		[31:0]	ir_IN2_m2[1:MAX_INDEX];


	reg	p1=1;

	always @ (*)
	begin: PROPERTY_LOGIC1
		if(ir_IN2_m1[1] == ir_IN2_m2[1]) begin	//IN2 is fixed
			if(ir_IN1_m1[1] == ir_IN1_m2[1]) begin
				p1 = (P_m1 == P_m2);				//If the inputs were the same, the results should be same
			end
			else begin
				p1 = 1;
			end
		end
		else begin
			p1 = 1;
		end
	end
	
	assign o_property = ~p1;

	always@(posedge i_clk) 
	begin: M1_INPUT_STORE
		if(i_rst) begin
			ir_IN1_m1[1] <= RESET_VALUE;
			ir_IN1_m1[2] <= RESET_VALUE;

			ir_IN2_m1[1] <= RESET_VALUE;
			ir_IN2_m1[2] <= RESET_VALUE;
		end
		else begin
			ir_IN1_m1[1] <= IN1_m1;
			ir_IN1_m1[2] <= ir_IN1_m1[1];

			ir_IN2_m1[1] <= IN2_m1;
			ir_IN2_m1[2] <= ir_IN2_m1[1];
		end
	end

	always@(posedge i_clk) 
	begin: M2_INPUT_STORE
		if(i_rst) begin
			ir_IN1_m2[1] <= RESET_VALUE;
			ir_IN1_m2[2] <= RESET_VALUE;

			ir_IN2_m2[1] <= RESET_VALUE;
			ir_IN2_m2[2] <= RESET_VALUE;
		end
		else begin
			ir_IN1_m2[1] <= IN1_m2;
			ir_IN1_m2[2] <= ir_IN1_m2[1];

			ir_IN2_m2[1] <= IN2_m2;
			ir_IN2_m2[2] <= ir_IN2_m2[1];
		end
	end

	
	ex e1	(
		.P			(P_m1	),
		.IN1		(IN1_m1	),
		.IN2		(IN2_m1	),
		.clk		(i_clk	),
		.rst		(i_rst	)
	);

	ex e2	(
		.P			(P_m2	),
		.IN1		(IN1_m2	),
		.IN2		(IN2_m2	),
		.clk		(i_clk	),
		.rst		(i_rst	)
	);

	/*
	Multiplier_31_0_3000 m1 (
		.P			(P_m1	),
		.IN1		(IN1_m1	),
		.IN2		(IN2_m1	),
		.clk		(i_clk	),
		.rst		(i_rst	)
	);
	
	Multiplier_31_0_3000 m2 (
		.P			(P_m2	),
		.IN1		(IN1_m2	),
		.IN2		(IN2_m2	),
		.clk		(i_clk	),
		.rst		(i_rst	)
	);
	*/

endmodule

module ex (
	output	[63:0]	P,
	input	[31:0]	IN1,
	input	[31:0]	IN2,
	input			clk,
	input			rst
);

	reg [63:0]	tmp;

	assign P = tmp;

	always@(posedge clk)
	begin: TMP
		if(rst) begin
			tmp <= 0;
		end
		else begin
			tmp <= IN1 + IN2;
			//{32'b00000000000000000000000000000000,  IN1 * IN2};
		end

	end

endmodule

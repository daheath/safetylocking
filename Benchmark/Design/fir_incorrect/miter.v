
/*
	Target module: Fir

	This module takes five consecutive inputs(x) and calculate dataout.
	This module is pipelined.
	Thus, the value of output channel(dataout) is affected by x[0] ~ x[-4].

	x[-5] should not affect the value of dataout.
	If this is ensured, than we can assert that the input window is correct.
	(As it is pipelined, we don't need to verify upon x[-6], x[-7]...
	
	Policy:

		//Checks the input window is correct
		prohibit x[-5] -> dataout

*/

module miter #(
	parameter	INPUT_WIDTH		=	8,
	parameter	OUTPUT_WIDTH	=	10,
	parameter	RESET_VALUE		=	0
)
(
	input	wire							i_clk,
	input	wire							i_rst,
	input	wire	[INPUT_WIDTH-1	:	0]	i_x0,	
	input	wire	[INPUT_WIDTH-1	:	0]	i_x1,	
	output	wire							o_property
);
	
	wire	[OUTPUT_WIDTH-1	:	0]	dataout0;
	wire	[OUTPUT_WIDTH-1	:	0]	dataout1;

	//Index: 0~1 machine number (duplicated test module)
	//		 1~5 To store the values over time
	//Register index starts from one in order to make the index exactly same as the time index
	reg		[INPUT_WIDTH-1	:	0]	ir_x0		[1:5];
	reg		[INPUT_WIDTH-1	:	0]	ir_x1		[1:5];

	reg p1;

	/* Property Logic */
	//	prohibit x[-5] -> dataout

	always @(*)
	begin: PROPERTY_LOGIC_1
		if( 
			//If the input values during the time -4 ~ 0 remain same
			(ir_x0[4] == ir_x1[4])
			&&  (ir_x0[3] == ir_x1[3])
			&& (ir_x0[2] == ir_x1[2])
			&& (ir_x0[1] == ir_x1[1])
			&& (i_x0 == i_x1)
		) begin
			//And if the x[5] is different from current input
			if(ir_x0[5] != ir_x1[5]) begin
				p1 = (dataout0 == dataout1);	//The output should be the same (No information flow)
			end
			else begin
				p1 = 1;
			end
		end
		else begin
			p1 = 1;
		end
	end
	
	assign	o_property = ~p1;
//	assign	o_property = 1;


	/* Input storing logic */
	// Implemented using shift registers


	always@(posedge i_clk)
	begin: INPUT_RECORD_X
		if(i_rst) begin
			ir_x0[1] <= RESET_VALUE;
			ir_x0[2] <= RESET_VALUE;
			ir_x0[3] <= RESET_VALUE;
			ir_x0[4] <= RESET_VALUE;
			ir_x0[5] <= RESET_VALUE;

			ir_x1[1] <= RESET_VALUE;
			ir_x1[2] <= RESET_VALUE;
			ir_x1[3] <= RESET_VALUE;
			ir_x1[4] <= RESET_VALUE;
			ir_x1[5] <= RESET_VALUE;
		end
		else begin
			ir_x0[1] <= i_x0;
			ir_x0[2] <= ir_x0[1];
			ir_x0[3] <= ir_x0[2];
			ir_x0[4] <= ir_x0[3];
			ir_x0[5] <= ir_x0[4];

			ir_x1[1] <= i_x1;
			ir_x1[2] <= ir_x1[1];
			ir_x1[3] <= ir_x1[2];
			ir_x1[4] <= ir_x1[3];
			ir_x1[5] <= ir_x1[4];
		end
	end

/*
	genvar t,m;
	generate
		for(m=0;m<2;m=m+1)
		begin: ITERATE_MACHINES
			//Input record for x
			for(t=1;t<5;t=t+1)
			begin: INPUT_RECORD_X
				always@(posedge i_clk) 
				begin:	INPUT_RECORD_X_SUB
					if(i_rst) begin
						if( m == 0 ) begin
							ir_x0[t] <= RESET_VALUE;
						end
						else begin
							ir_x1[t] <= RESET_VALUE;
						end
					end
					else begin
						if(t == 1) begin
							if( m == 0 ) begin
								ir_x0[t] <= i_x0;
							end
							else begin
								ir_x1[t] <= i_x1;
							end
						end
						else begin
							if( m == 0) begin
								ir_x0[t] <= ir_x0[t-1];
							end
							else begin
								ir_x1[t] <= ir_x1[t-1];
							end
						end
					end
				end
			end
		end

	endgenerate
*/
	/* Module under verification */
	filterfir m0 (
		.clk		(i_clk),
		.rst		(i_rst),
		.x			(i_x0),
		.dataout	(dataout0)
	);

	filterfir m1 (
		.clk		(i_clk),
		.rst		(i_rst),
		.x			(i_x1),
		.dataout	(dataout1)
	);


endmodule

//Shift amt = 5
module shift01(A, O);
	input [7:0] A;
	output [7:0] O;

	assign O = A >> 3'b101;
endmodule

//Shift amt = 4
module shift02(A, O);
	input [7:0] A;
	output [7:0] O;

	assign O = A >> 3'b100;
endmodule

//Shift amt = 3
module shift03(A, O);
	input [7:0] A;
	output [7:0] O;

	assign O = A >> 3'b011;
endmodule

//Shift amt = 2
module shift04(A, O);
	input [7:0] A;
	output [7:0] O;

	assign O = A >> 3'b010;
endmodule

//Shift amt = 1
module shift05(A, O);
	input [7:0] A;
	output [7:0] O;

	assign O = A >> 3'b001;
endmodule



// main module FIR
module filterfir(clk,rst,x,dataout);
	input	[7:0]	x;
	input			clk,rst;
	output	[9:0]	dataout;

	wire	[7:0]	d1,d2,d3;
	wire	[7:0]	m1,m2,m3,m4,m5;
	wire	[7:0]	d11,d12,d13,d14;
	wire	[7:0]	d15;		//Added

//	reg		[7:0]	trojan[0:4];

	//parameter h0=3'b101;
	//parameter h1=3'b100;
	//parameter h2=3'b011;
	//parameter h3=3'b010;
	//parameter h4=3'b001;
	//assign m1=x>>h0;
	shift01 SH01(x,m1);

	dff u2(clk,rst,x,d11);
	//assign m2=d11>>h1;
	shift02 SH02(d11, m2);

	assign d1=m1+m2;
	dff u4(clk,rst,d11,d12);
	//assign m3=d12>>h2;
	shift03 SH03(d12, m3);


	assign d2=d1+m3;
	dff u6(clk,rst,d12,d13);
	//assign m4=d13>>h3;
	shift04 SH04(d13, m4);

	assign d3=d2+m4;
	dff u8(clk,rst,d13,d14);
	//assign m5=d14>>h4;
	shift05 SH05(d14, m5);

	dff u10(clk,rst,d14,d15);	//Added

//	assign dataout=d3+m5;		// m1+m2+m3+m4+m5
	assign dataout=d15+m5;			// Malicous behaviour
endmodule

module dff(clk,rst,d,q);// sub module d flipflop
	input clk,rst;
	input [7:0]d;
	output [7:0]q;
	reg [7:0]q;

	always@(posedge clk)
	begin
		if(rst==1)
		begin
			q=0;
		end
		else
		begin
			q=d;
		end
	end
endmodule


/*
	Target module: UART
	
    input sys_clk,
    input sys_rst,

    input [13:0] csr_a,
    input csr_we,
    input [31:0] csr_di,
    output reg [31:0] csr_do,

    output rx_irq,
    output tx_irq,

    input uart_rx,
    output uart_tx	
	
	Policy:

		   default level H
		   set csr_do as L
		   order {L<H}

		   if(csr_adr[13:10] == csr_addr && csr_we[2] != 1) begin
			 prohibit csr_di[2] => csr_do
		   end
	       
	Attacking the missing case
*/

module miter #(
    parameter   RESET_VALUE     =   0,
	parameter	CSR_A_WIDTH		=	14,
	parameter	CSR_DI_WIDTH	=	32,
    parameter   CSR_DO_WIDTH    =   32	
)
(
	input	wire							sys_clk,
    input   wire                            sys_rst,
	
    input   wire    [CSR_A_WIDTH-1  :   0]  csr_a_1,
    input   wire    [CSR_A_WIDTH-1  :   0]  csr_a_2,
    
    input   wire                            csr_we_1,
    input   wire                            csr_we_2,
    
    input   wire    [CSR_DI_WIDTH-1 :   0]  csr_di_1,
    input   wire    [CSR_DI_WIDTH-1 :   0]  csr_di_2,

    input   wire                            uart_rx_1,
    input   wire                            uart_rx_2,

	output	wire							o_property
);
	localparam	MAX_TIME_INDEX = 6;
	localparam  TARGET_TIME_INDEX = 2;

    /* Output Ports */
    wire [31  :   0] csr_do_1;
    wire [31  :   0] csr_do_2;

    wire rx_irq_1;
    wire rx_irq_2;
    
    wire tx_irq_1;
    wire tx_irq_2;
    
    wire uart_tx_1;
    wire uart_tx_2;
     
	reg [4:0] cycles;
	reg isReset;

    /* Input Value Registers */
    reg     [CSR_A_WIDTH-1  :   0]  ir_csr_a_1[1  :   MAX_TIME_INDEX];
    reg     [CSR_A_WIDTH-1  :   0]  ir_csr_a_2[1  :   MAX_TIME_INDEX];
    
    reg                             ir_csr_we_1[1  :   MAX_TIME_INDEX];
    reg                             ir_csr_we_2[1  :   MAX_TIME_INDEX];
    
    reg     [CSR_DI_WIDTH-1 :   0]  ir_csr_di_1[1  :   MAX_TIME_INDEX];
    reg     [CSR_DI_WIDTH-1 :   0]  ir_csr_di_2[1  :   MAX_TIME_INDEX];

    reg                             ir_uart_rx_1[1  :   MAX_TIME_INDEX];
    reg                             ir_uart_rx_2[1  :   MAX_TIME_INDEX];
      
	/* Property Logics */

	reg [MAX_TIME_INDEX:1]pre_condition;
	reg [MAX_TIME_INDEX:1]p2_wide;


	always@(posedge sys_clk)
	begin: CYCLES
	  if(sys_rst) begin
	    cycles <= 0;
	  end
	  else begin
		if(cycles<MAX_TIME_INDEX) begin
		  cycles <= cycles + 1;
		end
	  end
	end

	assign isReset = (cycles == MAX_TIME_INDEX);

	genvar i;
	generate
	for(i=1; i<MAX_TIME_INDEX+1; i=i+1) 
	begin
		always @(*)
		begin:P1_WIDE
			if(i==1) begin
				pre_condition[i] = ~(ir_csr_a_1[i] == ir_csr_a_2[i]
								&&  ir_csr_a_1[i][13:10] != 0
            					&&  ir_csr_we_1[i] == ir_csr_we_2[i]
            					&&  ir_csr_di_1[i] == ir_csr_di_2[i]
				            	&&  ir_uart_rx_1[i] == ir_uart_rx_2[i]
			          	  	 );
			end
			else if(i!=TARGET_TIME_INDEX) begin
				pre_condition[i] = ~(ir_csr_a_1[i] == ir_csr_a_2[i]
            					&&  ir_csr_we_1[i] == ir_csr_we_2[i]
            					&&  ir_csr_di_1[i] == ir_csr_di_2[i]
				            	&&  ir_uart_rx_1[i] == ir_uart_rx_2[i]
			          	  	 );
			end
			else begin
				pre_condition[i] = ~(ir_csr_a_1[i] == ir_csr_a_2[i]
								&&  ir_csr_a_1[i][13:10] != 0
            					&&  ir_csr_we_1[i] == ir_csr_we_2[i]
            					&&  ir_csr_di_1[i] != ir_csr_di_2[i]
				            	&&  ir_uart_rx_1[i] == ir_uart_rx_2[i]
								&&  (ir_csr_we_1[i] == 0)
			          	  	 );
			end

		end
	end
	endgenerate

	wire property;
//	assign property = isReset? ((pre_condition == 0)? (csr_do_1 == csr_do_2) : 1):1;
	assign property = (pre_condition == 0)? (csr_do_1 == csr_do_2) : 1;
	assign o_property = ~property;

/*

    reg p1=1;

	always @(*)
	begin: PROPERTY_LOGIC
        if(
//				ir_csr_a_1[3] == ir_csr_a_2[3]
			&&	ir_csr_a_1[2] == ir_csr_a_2[2]
            &&  ir_csr_a_1[1] == ir_csr_a_2[1]
			&&  ir_csr_a_1[2][13:10] == 0
//            &&  ir_csr_we_1[3] == ir_csr_we_2[3]
            &&  ir_csr_we_1[2] == ir_csr_we_2[2]
            &&  ir_csr_we_1[1] == ir_csr_we_2[1]
			&&  ir_csr_we_1[2] == 0
//			&&  ir_csr_di_1[3] == ir_csr_di_2[3]
			&&  ir_csr_di_1[2] != ir_csr_di_2[2]
            &&  ir_csr_di_1[1] == ir_csr_di_2[1]            
//            &&  ir_uart_rx_1[3] == ir_uart_rx_2[3]
            &&  ir_uart_rx_1[2] == ir_uart_rx_2[2]
            &&  ir_uart_rx_1[1] == ir_uart_rx_2[1]            
          )
        begin
            p1 = (csr_do_1 == csr_do_2);
        end
		else begin
			p1 = 1;
		end
	end

	assign o_property = ~p1;
*/
    /* Input Recording Registers */
    genvar i;
	generate
	for(i=1; i<MAX_TIME_INDEX+1; i=i+1) begin

		always@(posedge i_clk)
		begin: IR_CSR_A
			if(sys_rst) begin
				ir_csr_a_1[i] <= RESET_VALUE;
				ir_csr_a_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_csr_a_1[i] <= csr_a_1;
					ir_csr_a_2[i] <= csr_a_2;
				end
				else begin
					ir_csr_a_1[i] <= ir_csr_a_1[i-1];
					ir_csr_a_2[i] <= ir_csr_a_2[i-1];
				end
			end
		end

		always@(posedge i_clk)
		begin: IR_CSR_WE
			if(sys_rst) begin
				ir_csr_we_1[i] <= RESET_VALUE;
				ir_csr_we_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_csr_we_1[i] <= csr_we_1;
					ir_csr_we_2[i] <= csr_we_2;
				end
				else begin
					ir_csr_we_1[i] <= ir_csr_we_1[i-1];
					ir_csr_we_2[i] <= ir_csr_we_2[i-1];
				end
			end
		end

		always@(posedge i_clk)
		begin: IR_CSR_DI
			if(sys_rst) begin
				ir_csr_di_1[i] <= RESET_VALUE;
				ir_csr_di_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_csr_di_1[i] <= csr_di_1;
					ir_csr_di_2[i] <= csr_di_2;
				end
				else begin
					ir_csr_di_1[i] <= ir_csr_di_1[i-1];
					ir_csr_di_2[i] <= ir_csr_di_2[i-1];
				end
			end
		end

		always@(posedge i_clk)
		begin: IR_UART_RX
			if(sys_rst) begin
				ir_uart_rx_1[i] <= RESET_VALUE;
				ir_uart_rx_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_uart_rx_1[i] <= uart_rx_1;
					ir_uart_rx_2[i] <= uart_rx_2;
				end
				else begin
					ir_uart_rx_1[i] <= ir_uart_rx_1[i-1];
					ir_uart_rx_2[i] <= ir_uart_rx_2[i-1];
				end
			end
		end
	end
	endgenerate
   
    
    uart uart_dut_1 (
        .sys_clk    (sys_clk    ),
        .sys_rst    (sys_rst    ),
        .csr_a      (csr_a_1    ),
        .csr_we     (csr_we_1   ),
        .csr_di     (csr_di_1   ),
        .csr_do     (csr_do_1   ),
        .rx_irq     (rx_irq_1   ),
        .tx_irq     (tx_irq_1   ),
        .uart_rx    (uart_rx_1  ),
        .uart_tx    (uart_tx_1  )
    );

    uart uart_dut_2 (
        .sys_clk    (sys_clk    ),
        .sys_rst    (sys_rst    ),
        .csr_a      (csr_a_2    ),
        .csr_we     (csr_we_2   ),
        .csr_di     (csr_di_2   ),
        .csr_do     (csr_do_2   ),
        .rx_irq     (rx_irq_2   ),
        .tx_irq     (tx_irq_2   ),
        .uart_rx    (uart_rx_2  ),
        .uart_tx    (uart_tx_2  )
    );
    
endmodule

/* Password module */

/* Policy
	if(i_pwd != PASSWORD)
		prohibit r_sensitive -> outcoming_data
*/

//This is a wrapper for the property check
module top # (
	parameter PASSWORD = 1231321,
	parameter SENSITIVE_DATA = 303242
)
(
	input	wire			i_clk,
	input	wire			i_rst,
	input	wire	[31:0]	i_pwd,
	output	wire			o_property
);

	reg		[31:0]	r_sensitive;
	wire	[31:0]	outcoming_data;

	//Property combinational logic
	assign o_property = ~(((i_pwd == PASSWORD) && (outcoming_data == r_sensitive))
						|| ((i_pwd != PASSWORD) && (outcoming_data != r_sensitive)));	

	always @ (posedge i_clk) 
	begin
		if(i_rst) begin
			r_sensitive <= 0;
		end
		else begin
			r_sensitive <= SENSITIVE_DATA;
		end
	end

	password# (
		.PASSWORD(PASSWORD)
	) passwd
	(
		.i_password(i_pwd),
		.i_sensitive(r_sensitive),
		.o_output(outcoming_data)
	);

endmodule

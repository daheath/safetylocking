
module password 
# (
	parameter PASSWORD = 1
)
(
	input wire	[31:0]	i_password,
	input wire	[31:0]	i_sensitive,
	output wire	[31:0]	o_output
);
	
	assign o_output = (i_password == PASSWORD)? i_sensitive : 1;	//Correct circuit
//	assign o_output = (i_password != PASSWORD)? i_sensitive : 1;	//Incorrect circuit

endmodule



/*
	Target module: Simple GPIO
	
	Policy:
	    if(req == 0)
	       prohibit req => grant

*/

module miter #(
	parameter	I_REQ_WIDTH		=	4,
	parameter	O_GRANT_WIDTH	=	4,
	parameter	RESET_VALUE		=	0
)
(
	input	wire							i_clk,
	input	wire							i_rst_an,
	input	wire [I_REQ_WIDTH-1  :   0]		i_req,

	output	wire							o_property
);
	localparam	MAX_TIME_INDEX = 2;
	
	/* Evaluation States */
	localparam  STATE_IDL = 2'b00; //IDLE
	localparam  STATE_RST = 2'b01; //RESET
	localparam  STATE_FST = 2'b10; //FIRST_CYCLE
	localparam  STATE_RUN = 2'b11; //RUNNING

    /* Output Ports */
    reg  [O_GRANT_WIDTH-1   :   0]  o_grant;

    /* Input Value Registers */
    reg [I_REQ_WIDTH-1      :   0]  ir_req;

    /* Evaluation State */
    reg [1:0] eval_started = STATE_IDL;

	/* Property Logics */
    reg p1=1;

	always @(*)
	begin: PROPERTY_LOGIC
        if(eval_started ==  STATE_RUN && ir_req == 0) 
        begin
            p1 = (o_grant == 0);
        end
		else begin
			p1 = 1;
		end
	end

	assign o_property = ~p1;
//  assign o_property = 1;

    always @(posedge i_clk)
    begin: EVAL_STRATED
        if(~i_rst_an) begin
            eval_started <= STATE_RST;        
        end
        else if(eval_started == STATE_RST) begin
            eval_started <= STATE_FST;
        end
        else if(eval_started == STATE_FST) begin
            eval_started <= STATE_RUNNING;
        end
    end

    /* Input Recording Registers */
    always @(posedge i_clk)
    begin: IR_REQ
        if(~i_rst_an) begin
            ir_req <= RESET_VALUE;
        end
        else begin
            ir_req <= i_req;
        end
    end

    round_robin_arbiter arb1 (
        .rst_an     (i_rst_an       ),
        .clk        (i_clk          ),
        .req        (i_req_1         ),
        .grant      (o_grant_1       )
    );
    
    
    round_robin_arbiter arb2 (
        .rst_an     (i_rst_an       ),
        .clk        (i_clk          ),
        .req        (i_req_2         ),
        .grant      (o_grant_2       )    
    );

endmodule

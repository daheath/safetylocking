
module top (
	output wire o_out
);

	reg r_a = 1'b1;
	reg r_b = 1'b0;

	or2 orgate (
		.i_a(r_a),
		.i_b(r_b),
		.o_c(o_out)
	);

endmodule

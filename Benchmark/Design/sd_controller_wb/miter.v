/*
	Policy

	level	{L,H};
	order	{L<H};
	set wb_dat_o as L;

	set wb_dat_i as H;
	set wb_adr_i as H;
	set	wb_sel_i as H;
	set wb_we_i as H;
	set wb_cyc_i as H;
	set wb_stb_i as H;
	set write_req_s as H;
	set cmd_set_s as H;
	set cmd_arg_s as H;

	allow	wb_dat_i[-1] -> wb_dat_o
	allow	wb_adr_i[-1] -> wb_dat_o
	allow	wb_sel_i[-1] -> wb_dat_o
	allow	wb_we_i[-1] -> wb_dat_o
	allow	wb_cyc_i[-1] -> wb_dat_o
	allow	wb_stb_i[-1] -> wb_dat_o
	allow	write_req_s[-1] -> wb_dat_o
	allow	cmd_set_s[-1] -> wb_dat_o
	allow	cmd_arg_s[-1] -> wb_dat_o

*/

module miter #(
	parameter	DATA_WIDTH		=	32,
	parameter	CMD_WIDTH		=	16,
	parameter	ADDR_WIDTH		=	8,
	parameter	SEL_WIDTH		=	4,
	parameter	CTI_WIDTH		=	3,
	parameter	BTE_WIDTH		=	2,
	parameter	SD_DATA_WIDTH	=	4,
	parameter	RESET_VALUE		=	0
)
(
	input	wire								i_clk,
	input	wire								i_rst,

	input	wire	[DATA_WIDTH-1		:	0]	wb_dat_i_m1,
	input	wire	[DATA_WIDTH-1		:	0]	wb_dat_i_m2,
	
	input	wire	[ADDR_WIDTH-1		:	0]	wb_adr_i_m1,
	input	wire	[ADDR_WIDTH-1		:	0]	wb_adr_i_m2,

	input	wire	[SEL_WIDTH-1		:	0]	wb_sel_i_m1,
	input	wire	[SEL_WIDTH-1		:	0]	wb_sel_i_m2,

	input	wire								wb_we_i_m1,
	input	wire								wb_we_i_m2,

	input	wire								wb_cyc_i_m1,
	input	wire								wb_cyc_i_m2,

	input	wire								wb_stb_i_m1,
	input	wire								wb_stb_i_m2,

	input	wire								write_req_s_m1,
	input	wire								write_req_s_m2,

	input	wire	[CMD_WIDTH-1		:	0]	cmd_set_s_m1,
	input	wire	[CMD_WIDTH-1		:	0]	cmd_set_s_m2,

	input	wire	[DATA_WIDTH-1		:	0]	cmd_arg_s_m1,
	input	wire	[DATA_WIDTH-1		:	0]	cmd_arg_s_m2,

	output	wire								o_property

);

	localparam	MAX_TIME_INDEX		=	4;
	localparam	TARGET_TIME_INDEX	=	1;

	/*	Outputs of the target module	*/
	wire	[DATA_WIDTH-1		:	0]	wb_dat_o_m1;
	wire	[DATA_WIDTH-1		:	0]	wb_dat_o_m2;

	wire								wb_ack_o_m1;
	wire								wb_ack_o_m2;

	wire								we_m_tx_bd_m1;
	wire								we_m_tx_bd_m2;

	wire								new_cmd_m1;
	wire								new_cmd_m2;

	wire								we_ack_m1;
	wire								we_ack_m2;

	wire								int_ack_m1;
	wire								int_ack_m2;

	wire								cmd_int_busy_m1;
	wire								cmd_int_busy_m2;

	wire								we_m_rx_bd_m1;
	wire								we_m_rx_bd_m2;

	wire								int_busy_m1;
	wire								int_busy_m2;

	/*	Input-recording registers	*/
	reg		[DATA_WIDTH-1		:	0]	ir_wb_dat_i_m1[1:MAX_TIME_INDEX];
	reg		[DATA_WIDTH-1		:	0]	ir_wb_dat_i_m2[1:MAX_TIME_INDEX];
	
	reg		[ADDR_WIDTH-1		:	0]	ir_wb_adr_i_m1[1:MAX_TIME_INDEX];
	reg		[ADDR_WIDTH-1		:	0]	ir_wb_adr_i_m2[1:MAX_TIME_INDEX];

	reg		[SEL_WIDTH-1		:	0]	ir_wb_sel_i_m1[1:MAX_TIME_INDEX];
	reg		[SEL_WIDTH-1		:	0]	ir_wb_sel_i_m2[1:MAX_TIME_INDEX];

	reg									ir_wb_we_i_m1[1:MAX_TIME_INDEX];
	reg									ir_wb_we_i_m2[1:MAX_TIME_INDEX];

	reg									ir_wb_cyc_i_m1[1:MAX_TIME_INDEX];
	reg									ir_wb_cyc_i_m2[1:MAX_TIME_INDEX];

	reg									ir_wb_stb_i_m1[1:MAX_TIME_INDEX];
	reg									ir_wb_stb_i_m2[1:MAX_TIME_INDEX];

	reg									ir_write_req_s_m1[1:MAX_TIME_INDEX];
	reg									ir_write_req_s_m2[1:MAX_TIME_INDEX];

	reg		[CMD_WIDTH-1		:	0]	ir_cmd_set_s_m1[1:MAX_TIME_INDEX];
	reg		[CMD_WIDTH-1		:	0]	ir_cmd_set_s_m2[1:MAX_TIME_INDEX];

	reg		[DATA_WIDTH-1		:	0]	ir_cmd_arg_s_m1[1:MAX_TIME_INDEX];
	reg		[DATA_WIDTH-1		:	0]	ir_cmd_arg_s_m2[1:MAX_TIME_INDEX];

	/*	Property Logics	*/
	reg p1;

	assign	o_property = ~p1;

	always@(*)
	begin: PROPERTY_LOGIC_1
		if((ir_wb_adr_i_m1[TARGET_TIME_INDEX] == ir_wb_adr_i_m2[TARGET_TIME_INDEX])
			&&	(ir_wb_sel_i_m1[TARGET_TIME_INDEX] == ir_wb_sel_i_m2[TARGET_TIME_INDEX])
			&&	(ir_wb_we_i_m1[TARGET_TIME_INDEX] == ir_wb_we_i_m2[TARGET_TIME_INDEX])
			&&	(ir_wb_cyc_i_m1[TARGET_TIME_INDEX] == ir_wb_cyc_i_m2[TARGET_TIME_INDEX])
			&&	(ir_wb_stb_i_m1[TARGET_TIME_INDEX] == ir_wb_stb_i_m2[TARGET_TIME_INDEX])
			&&	(ir_write_req_s_m1[TARGET_TIME_INDEX] == ir_write_req_s_m2[TARGET_TIME_INDEX])
			&&	(ir_cmd_set_s_m1[TARGET_TIME_INDEX] == ir_cmd_set_s_m2[TARGET_TIME_INDEX])
			&&	(ir_cmd_arg_s_m1[TARGET_TIME_INDEX]	== ir_cmd_arg_s_m2[TARGET_TIME_INDEX])
			&&	(ir_wb_dat_i_m1[TARGET_TIME_INDEX] == ir_wb_dat_i_m2[TARGET_TIME_INDEX])
		)	begin
			p1 = (wb_dat_o_m1 == wb_dat_o_m2)? 1:0;
		end
		else begin
			p1 = 1;
		end
	end

	/* Input storing logics	*/
	genvar i;
	generate
		for(i=1; i<MAX_TIME_INDEX;	i=i+1) begin
			always @ (posedge i_clk)
			begin: INPUT_STORING
				if(i_rst) begin
					ir_wb_dat_i_m1[i] <= RESET_VALUE;
					ir_wb_dat_i_m2[i] <= RESET_VALUE;
	
					ir_wb_adr_i_m1[i] <= RESET_VALUE;
					ir_wb_adr_i_m2[i] <=RESET_VALUE;

					ir_wb_sel_i_m1[i] <=RESET_VALUE;
					ir_wb_sel_i_m2[i] <=RESET_VALUE;

					ir_wb_we_i_m1[i] <=RESET_VALUE;
					ir_wb_we_i_m2[i] <=RESET_VALUE;

					ir_wb_cyc_i_m1[i] <=RESET_VALUE;
					ir_wb_cyc_i_m2[i] <=RESET_VALUE;

					ir_wb_stb_i_m1[i] <= RESET_VALUE;
					ir_wb_stb_i_m2[i] <= RESET_VALUE;

					ir_write_req_s_m1[i] <= RESET_VALUE;
					ir_write_req_s_m2[i] <= RESET_VALUE;

					ir_cmd_set_s_m1[i] <= RESET_VALUE;
					ir_cmd_set_s_m2[i] <= RESET_VALUE;

					ir_cmd_arg_s_m1[i] <= RESET_VALUE;
					ir_cmd_arg_s_m2[i] <= RESET_VALUE;

				end
				else begin
					if(i==1) begin
						ir_wb_dat_i_m1[i] <= wb_dat_i_m1;
						ir_wb_dat_i_m2[i] <= wb_dat_i_m2;
	
						ir_wb_adr_i_m1[i] <= wb_adr_i_m1;
						ir_wb_adr_i_m2[i] <= wb_adr_i_m2;

						ir_wb_sel_i_m1[i] <= wb_sel_i_m1;
						ir_wb_sel_i_m2[i] <= wb_sel_i_m2;

						ir_wb_we_i_m1[i] <= wb_we_i_m1;
						ir_wb_we_i_m2[i] <= wb_we_i_m2;

						ir_wb_cyc_i_m1[i] <= wb_cyc_i_m1;
						ir_wb_cyc_i_m2[i] <= wb_cyc_i_m2;

						ir_wb_stb_i_m1[i] <= wb_stb_i_m1;
						ir_wb_stb_i_m2[i] <= wb_stb_i_m2;

						ir_write_req_s_m1[i] <= write_req_s_m1;
						ir_write_req_s_m2[i] <= write_req_s_m2;

						ir_cmd_set_s_m1[i] <= cmd_set_s_m1;
						ir_cmd_set_s_m2[i] <= cmd_set_s_m2;

						ir_cmd_arg_s_m1[i] <= cmd_arg_s_m1;
						ir_cmd_arg_s_m2[i] <= cmd_arg_s_m2;
					end
					else begin
						ir_wb_dat_i_m1[i] <= ir_wb_dat_i_m1[i-1];
						ir_wb_dat_i_m2[i] <= ir_wb_dat_i_m2[i-1];
	
						ir_wb_adr_i_m1[i] <= ir_wb_adr_i_m1[i-1];
						ir_wb_adr_i_m2[i] <= ir_wb_adr_i_m2[i-1];

						ir_wb_sel_i_m1[i] <= ir_wb_sel_i_m1[i-1];
						ir_wb_sel_i_m2[i] <= ir_wb_sel_i_m2[i-1];

						ir_wb_we_i_m1[i] <= ir_wb_we_i_m1[i-1];
						ir_wb_we_i_m2[i] <= ir_wb_we_i_m2[i-1];

						ir_wb_cyc_i_m1[i] <= ir_wb_cyc_i_m1[i-1];
						ir_wb_cyc_i_m2[i] <= ir_wb_cyc_i_m2[i-1];

						ir_wb_stb_i_m1[i] <= ir_wb_stb_i_m1[i-1];
						ir_wb_stb_i_m2[i] <= ir_wb_stb_i_m2[i-1];

						ir_write_req_s_m1[i] <= ir_write_req_s_m1[i-1];
						ir_write_req_s_m2[i] <= ir_write_req_s_m2[i-1];

						ir_cmd_set_s_m1[i] <= ir_cmd_set_s_m1[i-1];
						ir_cmd_set_s_m2[i] <= ir_cmd_set_s_m2[i-1];

						ir_cmd_arg_s_m1[i] <= ir_cmd_arg_s_m1[i-1];
						ir_cmd_arg_s_m2[i] <= ir_cmd_arg_s_m2[i-1];
					end
				end
			end
		end
	endgenerate





sd_controller_wb	cnt1	(
	.wb_clk_i		(i_clk				),
	.wb_rst_i		(i_rst				),
	.wb_dat_i		(wb_dat_i_m1		),
	.wb_dat_o		(wb_dat_o_m1		),
	.wb_adr_i		(wb_adr_i_m1		),
	.wb_sel_i		(wb_sel_i_m1		),
	.wb_we_i		(wb_we_i_m1			),
	.wb_cyc_i		(wb_cyc_i_m1		),
	.wb_stb_i		(wb_stb_i_m1		),
	.wb_ack_o		(wb_ack_o_m1		),
	.we_m_tx_bd		(wb_m_tx_bd_m1		),
	.new_cmd		(new_cmd_m1			),
	.we_ack			(we_ack_m1			),
	.int_ack		(int_ack_m1			),
	.cmd_int_busy	(cmd_int_busy_m1	),
	.we_m_rx_bd		(we_m_rx_bd_m1		),
	.int_busy		(int_busy_m1		),
	.write_req_s	(write_req_s_m1		),
	.cmd_set_s		(cmd_set_s_m1		),
	.cmd_arg_s		(cmd_arg_s_m1		)
);

sd_controller_wb	cnt2	(
	.wb_clk_i		(i_clk				),
	.wb_rst_i		(i_rst				),
	.wb_dat_i		(wb_dat_i_m2		),
	.wb_dat_o		(wb_dat_o_m2		),
	.wb_adr_i		(wb_adr_i_m2		),
	.wb_sel_i		(wb_sel_i_m2		),
	.wb_we_i		(wb_we_i_m2			),
	.wb_cyc_i		(wb_cyc_i_m2		),
	.wb_stb_i		(wb_stb_i_m2		),
	.wb_ack_o		(wb_ack_o_m2		),
	.we_m_tx_bd		(wb_m_tx_bd_m2		),
	.new_cmd		(new_cmd_m2			),
	.we_ack			(we_ack_m2			),
	.int_ack		(int_ack_m2			),
	.cmd_int_busy	(cmd_int_busy_m2	),
	.we_m_rx_bd		(we_m_rx_bd_m2		),
	.int_busy		(int_busy_m2		),
	.write_req_s	(write_req_s_m2		),
	.cmd_set_s		(cmd_set_s_m2		),
	.cmd_arg_s		(cmd_arg_s_m2		)
);


endmodule

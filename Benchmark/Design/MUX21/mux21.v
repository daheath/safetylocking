
module mux21 (
	input i_a,
	input i_b,
	input i_sel,
	output o_c
);

	assign o_c = (i_sel == 0)? i_a : i_b;

endmodule


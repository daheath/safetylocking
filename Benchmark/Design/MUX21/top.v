
module top (
	output wire o_out
);

	reg r_a = 1'b0;
	reg r_b = 1'b1;
//	reg r_sel = 1'b0; //The output should one. ABC should say "Equivalent"	
	reg r_sel = 1'b1; //The output should zero. ABC should say "Not Equivalent"	


	mux21 themux ( 
		.i_a(r_a),
		.i_b(r_b),
		.i_sel(r_sel),
		.o_c(o_out)
	);

endmodule

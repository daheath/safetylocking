/* Password module */

/* Policy description

	module cond_add;
		if(i_password[-1] != PASSWORD)
			prohibit i_sensitive[-1] -> o_output;
	endmodule

*/

//This is a wrapper for the property check
module miter # (
	parameter	RESET_VALUE		= 0,
	parameter	PASSWORD		= 1231321,
	parameter	DATA_WIDTH		= 32,
	parameter	SENSITIVE_DATA	= 303242
)
(
	/*	IO ports				*/
	input	wire							i_clk,
	input	wire							i_rst,
	input	wire	[DATA_WIDTH-1	:	0]	i_normal,
	input	wire	[DATA_WIDTH-1	:	0]	i_sensitive,
	input	wire	[DATA_WIDTH-1	:	0]	i_password,
	output	wire							o_property
);

	/*	Registers				*/
	reg		[DATA_WIDTH-1	:	0]	r_password			[1:2];
	reg		[DATA_WIDTH-1	:	0]	r_outcoming_data;

	reg		[DATA_WIDTH-1	:	0]	r_normal			[1:2];
	reg		[DATA_WIDTH-1	:	0]	r_sensitive			[1:2];

	reg		[DATA_WIDTH-1	:	0]	r_clk_counter;

	/*	Wires					*/
	wire	[DATA_WIDTH-1	:	0]	w_outcoming_data;

	/*	Temporary value					*/
	reg		tmp;


	//Style 1
	always @(*)
	begin: PROPERTY_COMBINATIONAL
		if(r_password[1] != PASSWORD) begin
			if(r_normal[1] == r_normal[2]) begin
				if(r_sensitive[1] != r_sensitive[2]) begin
					tmp = (r_outcoming_data == w_outcoming_data);
				end
				else begin
					tmp = 1;
				end
			end
			else begin
				tmp = 1;
			end
		end
		else begin
			tmp = 1;
		end
	end

	assign o_property = ~tmp;

	//Style 2
	/*
	assign o_property = ~( (r_password[1] == PASSWORD) ? 1
							 : (r_normal[1] != r_normal[2])? 1
								: (r_sensitive[1] == r_sensitive[2])? 1
									: (r_outcoming_data != w_outcoming_data)? 0 : 1;
	*/

	always @(posedge i_clk)
	begin: R_COUNTER
		if(i_rst) begin
			r_clk_counter <= RESET_VALUE;
		end
		else begin
			r_clk_counter <= r_clk_counter + 1;
		end

	end

	//Save  i_password[-1]
	always @(posedge i_clk)
	begin: R_PASSWORD
		if(i_rst) begin
			r_password[1] <= RESET_VALUE;
			r_password[2] <= RESET_VALUE;
		end
		else begin
			r_password[1] <= i_password;
			r_password[2] <= r_password[1];
		end
	end

	//Save w_outcoming_data[-1]
	always @(posedge i_clk)
	begin: R_OUTCOMING
		if(i_rst) begin
			r_outcoming_data <= RESET_VALUE;
		end
		else begin
			r_outcoming_data <= w_outcoming_data;
		end
	end

	always @(posedge i_clk)
	begin: R_NORMAL
		if(i_rst) begin
			r_normal[1]	<= RESET_VALUE;
			r_normal[2] <= RESET_VALUE;
		end
		else begin
			r_normal[1] <= i_normal;
			r_normal[2] <= r_normal[1];
		end
	end

	always @(posedge i_clk)
	begin: R_SENSITIVE
		if(i_rst) begin
			r_sensitive[1] <= RESET_VALUE;
			r_sensitive[2] <= RESET_VALUE;
		end
		else begin
			r_sensitive[1] <= i_sensitive;
			r_sensitive[2] <= r_sensitive[1];
		end
	end

	//Design under verification
	cond_add# (
		.RESET_VALUE	(RESET_VALUE),
		.DATA_WIDTH		(DATA_WIDTH),
		.PASSWORD		(PASSWORD)
	) passwd
	(
		.i_clk			(i_clk),
		.i_rst			(i_rst),
		.i_normal_data	(i_normal),
		.i_sensitive	(i_sensitive),
		.i_password		(i_password),
		.o_output		(w_outcoming_data)
	);

endmodule

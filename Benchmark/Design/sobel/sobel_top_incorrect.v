/*
	Naive implementation of Sobel filter 
	This can block information flow (pixel data -> output) by setting prohibited region.
	Use PROHIBIT_W_START, PROHIBIT_W_END, PROHIBIT_H_START, PROHIBIT_H_END
*/

/*
	Pixel ID = (wID, hID)

*/

module sobel_top#(
	parameter	IMAGE_WIDTH				=	10,
	parameter	IMAGE_HEIGHT			=	10,
	parameter	INPUT_DATA_BIT_WIDTH	=	9,
	parameter	OUTPUT_DATA_BIT_WIDTH	=	8,
	parameter	WINDOW_SIZE				=	3,
	parameter	PROHIBIT_W_START		=	4,
	parameter	PROHIBIT_W_END			=	6,
	parameter	PROHIBIT_H_START		=	3,
	parameter	PROHIBIT_H_END			=	7
)
(
	input	wire								i_clk,
	input	wire								i_rst,
	input	wire	[INPUT_BIT_WIDTH-1	:	0]	i_pixel_data,
	input	wire	[INPUT_BIT_WIDTH-1	:	0]	i_pixel_wID,
	input	wire	[INPUT_BIT_WIDTH-1	:	0]	i_pixel_hID,

	output	wire	[OUTPUT_BIT_WIDTH-1	:	0]	o_output_data
);
	localparam	NUM_PIXELS					=	WINDOW_SIZE * WINDOW_SIZE;
	localparam	INPUT_BIT_WIDTH				=	NUM_PIXELS * INPUT_DATA_BIT_WIDTH;
	localparam	OUTPUT_BIT_WIDTH			=	OUTPUT_DATA_BIT_WIDTH;

	reg		[INPUT_DATA_BIT_WIDTH-1	:	0]	p	[0:NUM_PIXELS-1];
	reg		[INPUT_DATA_BIT_WIDTH-1	:	0]	wID	[0:NUM_PIXELS-1];
	reg		[INPUT_DATA_BIT_WIDTH-1	:	0]	hID	[0:NUM_PIXELS-1];

	reg		did_violate[0:NUM_PIXELS-1];

	genvar i;
	generate
		for(i=0; i<NUM_PIXELS; i=i+1) begin
			always@(*)
			begin: VIOLATION_CHECK
				wID[i]	= i_pixel_wID[INPUT_BIT_WIDTH-1 -i*INPUT_DATA_BIT_WIDTH -: INPUT_DATA_BIT_WIDTH];
				hID[i]	= i_pixel_hID[INPUT_BIT_WIDTH-1 -i*INPUT_DATA_BIT_WIDTH -: INPUT_DATA_BIT_WIDTH];

				//If the pixel is not from prohibited area
				if((wID[i] < PROHIBIT_W_START || wID[i] > PROHIBIT_W_END)
					&& (hID[i] < PROHIBIT_H_START || hID[i] > PROHIBIT_H_END)
				) begin
					did_violate[i] = 0;
				end
				else begin
					did_violate[i] = 1;
				end
			end
		end
	endgenerate

	genvar k;
	generate
		for(k=0; k<NUM_PIXELS; k=k+1) begin
			always @(*)
			begin: ASSIGN_P
				p[k] = i_pixel_data[INPUT_BIT_WIDTH-1 -k*INPUT_DATA_BIT_WIDTH -: INPUT_DATA_BIT_WIDTH];
			end
		end
	endgenerate


	sobel	sobel_filter (
		.clk		(i_clk				),
		.p0			(p[0]				),
		.p1			(p[1]				),
		.p2			(p[2]				),
		.p3			(p[3]				),
		.p5			(p[5]				),
		.p6			(p[6]				),
		.p7			(p[7]				),
		.p8			(p[8]				),
		.out		(o_output_data		)
	);

endmodule


module exoor_0 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module fa_0 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n1, n2, n3;

  XOR2_X2_slow U1 ( .A(c_in), .B(n1), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n1) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n1), .ZN(n3) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n2) );
  NAND2_X4_slow U5 ( .A1(n2), .A2(n3), .ZN(c_out) );
endmodule


module exoor_117 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_118 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_119 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_120 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_121 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_122 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_123 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_124 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module fa_117 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module fa_118 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_119 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n4), .ZN(c_out) );
endmodule


module fa_120 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n4), .ZN(c_out) );
endmodule


module fa_121 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_122 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_123 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n4), .ZN(c_out) );
endmodule


module fa_124 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module sobel_add_nb_bitwidth9_0 ( a, b, ans_out, cout, subtract );
  input [8:0] a;
  input [8:0] b;
  output [8:0] ans_out;
  input subtract;
  output cout;

  wire   [8:0] bcomp;
  wire   [7:0] carry;

  exoor_0 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_0 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), .c_out(
        carry[0]) );
  exoor_124 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_124 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_123 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_123 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(
        ans_out[2]), .c_out(carry[2]) );
  exoor_122 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_122 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(
        ans_out[3]), .c_out(carry[3]) );
  exoor_121 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_121 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(
        ans_out[4]), .c_out(carry[4]) );
  exoor_120 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_120 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(
        ans_out[5]), .c_out(carry[5]) );
  exoor_119 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_119 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(
        ans_out[6]), .c_out(carry[6]) );
  exoor_118 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_118 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(
        ans_out[7]), .c_out(carry[7]) );
  exoor_117 X_bitwidth ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_117 fa_bitwidth ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(ans_out[8]), 
        .c_out(cout) );
endmodule


module exoor_80 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_81 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_82 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_83 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(n1), .B(in1), .ZN(out) );
endmodule


module exoor_84 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(n1), .B(in1), .ZN(out) );
endmodule


module exoor_85 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(n1), .B(in1), .ZN(out) );
endmodule


module exoor_86 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(n1), .B(in1), .ZN(out) );
endmodule


module exoor_87 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(n1), .B(in1), .ZN(out) );
endmodule


module exoor_88 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_89 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module fa_80 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module fa_81 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n2, n3, n4, n5, n6;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n4) );
  XOR2_X2_slow U1 ( .A(c_in), .B(n4), .Z(s) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n5), .ZN(n3) );
  BUF_X32_slow U3 ( .A(n4), .Z(n5) );
  BUF_X32_slow U5 ( .A(b), .Z(n6) );
  NAND2_X4_slow U6 ( .A1(a), .A2(n6), .ZN(n2) );
  NAND2_X4_slow U7 ( .A1(n2), .A2(n3), .ZN(c_out) );
endmodule


module fa_82 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7, n8;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n8) );
  BUF_X32_slow U1 ( .A(c_in), .Z(n4) );
  BUF_X32_slow U2 ( .A(n8), .Z(n5) );
  XOR2_X2_slow U3 ( .A(n4), .B(n5), .Z(s) );
  NAND2_X4_slow U5 ( .A1(b), .A2(a), .ZN(n7) );
  NAND2_X4_slow U6 ( .A1(n6), .A2(n7), .ZN(c_out) );
  NAND2_X4_slow U7 ( .A1(c_in), .A2(n8), .ZN(n6) );
endmodule


module fa_83 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  BUF_X32_slow U1 ( .A(n7), .Z(n4) );
  XOR2_X2_slow U2 ( .A(c_in), .B(n4), .Z(s) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n7), .ZN(n5) );
  NAND2_X4_slow U6 ( .A1(n5), .A2(n6), .ZN(c_out) );
endmodule


module fa_84 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  BUF_X32_slow U1 ( .A(n7), .Z(n4) );
  XOR2_X2_slow U2 ( .A(c_in), .B(n4), .Z(s) );
  NAND2_X4_slow U3 ( .A1(b), .A2(a), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n7), .ZN(n5) );
  NAND2_X4_slow U6 ( .A1(n5), .A2(n6), .ZN(c_out) );
endmodule


module fa_85 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n7) );
  BUF_X32_slow U1 ( .A(n7), .Z(n4) );
  XOR2_X2_slow U2 ( .A(c_in), .B(n4), .Z(s) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(n6), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n7), .ZN(n5) );
endmodule


module fa_86 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n7) );
  BUF_X32_slow U1 ( .A(n7), .Z(n4) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n6) );
  XOR2_X2_slow U3 ( .A(c_in), .B(n4), .Z(s) );
  NAND2_X4_slow U5 ( .A1(n7), .A2(c_in), .ZN(n5) );
  NAND2_X4_slow U6 ( .A1(n5), .A2(n6), .ZN(c_out) );
endmodule


module fa_87 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  BUF_X32_slow U1 ( .A(n7), .Z(n4) );
  XOR2_X2_slow U2 ( .A(c_in), .B(n4), .Z(s) );
  NAND2_X4_slow U3 ( .A1(b), .A2(a), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(n6), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n7), .ZN(n5) );
endmodule


module fa_88 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U2 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U4 ( .A1(n4), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_89 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module sobel_add_nb_bitwidth10_0 ( a, b, ans_out, cout, subtract );
  input [9:0] a;
  input [9:0] b;
  output [9:0] ans_out;
  input subtract;
  output cout;

  wire   [9:0] bcomp;
  wire   [8:0] carry;

  exoor_89 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_89 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), 
        .c_out(carry[0]) );
  exoor_88 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_88 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_87 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_87 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(
        ans_out[2]), .c_out(carry[2]) );
  exoor_86 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_86 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(
        ans_out[3]), .c_out(carry[3]) );
  exoor_85 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_85 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(
        ans_out[4]), .c_out(carry[4]) );
  exoor_84 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_84 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(
        ans_out[5]), .c_out(carry[5]) );
  exoor_83 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_83 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(
        ans_out[6]), .c_out(carry[6]) );
  exoor_82 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_82 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(
        ans_out[7]), .c_out(carry[7]) );
  exoor_81 gen_fa_8__X_i ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_81 gen_fa_8__fa_i ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(
        ans_out[8]), .c_out(carry[8]) );
  exoor_80 X_bitwidth ( .in1(b[9]), .in2(subtract), .out(bcomp[9]) );
  fa_80 fa_bitwidth ( .a(a[9]), .b(bcomp[9]), .c_in(carry[8]), .s(ans_out[9]), 
        .c_out(cout) );
endmodule


module not_11b_0 ( in, out );
  input [10:0] in;
  output [10:0] out;


  INV_X32_slow U1 ( .A(in[0]), .ZN(out[0]) );
  INV_X32_slow U2 ( .A(in[1]), .ZN(out[1]) );
  INV_X32_slow U3 ( .A(in[2]), .ZN(out[2]) );
  INV_X32_slow U4 ( .A(in[4]), .ZN(out[4]) );
  INV_X32_slow U5 ( .A(in[5]), .ZN(out[5]) );
  INV_X32_slow U6 ( .A(in[7]), .ZN(out[7]) );
  INV_X32_slow U7 ( .A(in[6]), .ZN(out[6]) );
  INV_X32_slow U8 ( .A(in[3]), .ZN(out[3]) );
  INV_X32_slow U9 ( .A(in[10]), .ZN(out[10]) );
  INV_X32_slow U10 ( .A(in[9]), .ZN(out[9]) );
  INV_X32_slow U11 ( .A(in[8]), .ZN(out[8]) );
endmodule


module exoor_23 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_24 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_25 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_26 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_27 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_28 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_29 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_30 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_31 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_32 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_33 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module fa_23 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module fa_24 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_25 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_26 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_27 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_28 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_29 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_30 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_31 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_32 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_33 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module sobel_add_nb_bitwidth11_0 ( a, b, ans_out, cout, subtract );
  input [10:0] a;
  input [10:0] b;
  output [10:0] ans_out;
  input subtract;
  output cout;

  wire   [10:0] bcomp;
  wire   [9:0] carry;

  exoor_33 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_33 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), 
        .c_out(carry[0]) );
  exoor_32 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_32 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_31 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_31 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(
        ans_out[2]), .c_out(carry[2]) );
  exoor_30 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_30 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(
        ans_out[3]), .c_out(carry[3]) );
  exoor_29 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_29 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(
        ans_out[4]), .c_out(carry[4]) );
  exoor_28 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_28 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(
        ans_out[5]), .c_out(carry[5]) );
  exoor_27 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_27 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(
        ans_out[6]), .c_out(carry[6]) );
  exoor_26 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_26 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(
        ans_out[7]), .c_out(carry[7]) );
  exoor_25 gen_fa_8__X_i ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_25 gen_fa_8__fa_i ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(
        ans_out[8]), .c_out(carry[8]) );
  exoor_24 gen_fa_9__X_i ( .in1(b[9]), .in2(subtract), .out(bcomp[9]) );
  fa_24 gen_fa_9__fa_i ( .a(a[9]), .b(bcomp[9]), .c_in(carry[8]), .s(
        ans_out[9]), .c_out(carry[9]) );
  exoor_23 X_bitwidth ( .in1(b[10]), .in2(subtract), .out(bcomp[10]) );
  fa_23 fa_bitwidth ( .a(a[10]), .b(bcomp[10]), .c_in(carry[9]), .s(
        ans_out[10]), .c_out(cout) );
endmodule


module mux_11b_0 ( in0, in1, sel, out );
  input [10:0] in0;
  input [10:0] in1;
  output [10:0] out;
  input sel;
  wire   n1, n2, n3, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17,
         n18, n19, n20, n21, n22, n23, n4, n5, n24, n25, n26, n27;

  INV_X32_slow U1 ( .A(sel), .ZN(n1) );
  BUF_X32_slow U2 ( .A(n1), .Z(n4) );
  INV_X32_slow U3 ( .A(n4), .ZN(n5) );
  INV_X32_slow U4 ( .A(sel), .ZN(n24) );
  BUF_X32_slow U5 ( .A(n5), .Z(n25) );
  OR2_X4_slow U6 ( .A1(n26), .A2(n27), .ZN(out[8]) );
  AND2_X4_slow U7 ( .A1(in1[8]), .A2(n25), .ZN(n26) );
  AND2_X4_slow U8 ( .A1(in0[8]), .A2(n24), .ZN(n27) );
  NAND2_X4_slow U9 ( .A1(n6), .A2(n7), .ZN(out[7]) );
  NAND2_X4_slow U10 ( .A1(in0[7]), .A2(n24), .ZN(n7) );
  NAND2_X4_slow U11 ( .A1(n16), .A2(n17), .ZN(out[2]) );
  NAND2_X4_slow U12 ( .A1(n15), .A2(n14), .ZN(out[3]) );
  NAND2_X4_slow U13 ( .A1(n8), .A2(n9), .ZN(out[6]) );
  NAND2_X4_slow U14 ( .A1(in0[6]), .A2(n24), .ZN(n9) );
  NAND2_X4_slow U15 ( .A1(n12), .A2(n13), .ZN(out[4]) );
  NAND2_X4_slow U16 ( .A1(n10), .A2(n11), .ZN(out[5]) );
  NAND2_X4_slow U17 ( .A1(n19), .A2(n18), .ZN(out[1]) );
  NAND2_X4_slow U18 ( .A1(n2), .A2(n3), .ZN(out[9]) );
  NAND2_X4_slow U19 ( .A1(in0[9]), .A2(n24), .ZN(n3) );
  NAND2_X4_slow U20 ( .A1(n20), .A2(n21), .ZN(out[10]) );
  NAND2_X4_slow U21 ( .A1(in0[10]), .A2(n24), .ZN(n21) );
  NAND2_X4_slow U22 ( .A1(n1), .A2(in0[1]), .ZN(n19) );
  NAND2_X4_slow U23 ( .A1(n1), .A2(in0[0]), .ZN(n23) );
  NAND2_X4_slow U24 ( .A1(in0[2]), .A2(n24), .ZN(n17) );
  NAND2_X4_slow U25 ( .A1(in0[3]), .A2(n24), .ZN(n15) );
  NAND2_X4_slow U26 ( .A1(in0[4]), .A2(n24), .ZN(n13) );
  NAND2_X4_slow U27 ( .A1(in0[5]), .A2(n24), .ZN(n11) );
  NAND2_X4_slow U28 ( .A1(n25), .A2(in1[9]), .ZN(n2) );
  NAND2_X4_slow U29 ( .A1(in1[7]), .A2(n25), .ZN(n6) );
  NAND2_X4_slow U30 ( .A1(in1[6]), .A2(n25), .ZN(n8) );
  NAND2_X4_slow U31 ( .A1(in1[5]), .A2(n5), .ZN(n10) );
  NAND2_X4_slow U32 ( .A1(in1[4]), .A2(n5), .ZN(n12) );
  NAND2_X4_slow U33 ( .A1(in1[3]), .A2(n5), .ZN(n14) );
  NAND2_X4_slow U34 ( .A1(sel), .A2(in1[2]), .ZN(n16) );
  NAND2_X4_slow U35 ( .A1(sel), .A2(in1[1]), .ZN(n18) );
  NAND2_X4_slow U36 ( .A1(in1[10]), .A2(n25), .ZN(n20) );
  NAND2_X4_slow U37 ( .A1(sel), .A2(in1[0]), .ZN(n22) );
  NAND2_X4_slow U38 ( .A1(n23), .A2(n22), .ZN(out[0]) );
endmodule


module mux_8b ( in0, in1, sel, out );
  input [7:0] in0;
  input [7:0] in1;
  output [7:0] out;
  input sel;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26;

  INV_X32_slow U1 ( .A(in1[0]), .ZN(n24) );
  BUF_X32_slow U2 ( .A(sel), .Z(n25) );
  NAND2_X4_slow U3 ( .A1(n25), .A2(in1[7]), .ZN(n2) );
  NAND2_X4_slow U4 ( .A1(in1[1]), .A2(n21), .ZN(n14) );
  NAND2_X4_slow U5 ( .A1(n21), .A2(in1[6]), .ZN(n4) );
  INV_X32_slow U6 ( .A(n22), .ZN(n18) );
  NAND2_X4_slow U7 ( .A1(in0[3]), .A2(n1), .ZN(n11) );
  OR2_X4_slow U8 ( .A1(n1), .A2(n24), .ZN(n16) );
  INV_X32_slow U9 ( .A(n22), .ZN(n19) );
  BUF_X32_slow U10 ( .A(sel), .Z(n21) );
  NAND2_X4_slow U11 ( .A1(n25), .A2(in1[3]), .ZN(n10) );
  INV_X32_slow U12 ( .A(n21), .ZN(n20) );
  NAND2_X4_slow U13 ( .A1(in1[2]), .A2(n25), .ZN(n12) );
  NAND2_X4_slow U14 ( .A1(in1[5]), .A2(n25), .ZN(n6) );
  NAND2_X4_slow U15 ( .A1(in1[4]), .A2(n25), .ZN(n8) );
  BUF_X32_slow U16 ( .A(sel), .Z(n22) );
  INV_X32_slow U17 ( .A(n26), .ZN(n23) );
  BUF_X32_slow U18 ( .A(sel), .Z(n26) );
  NAND2_X4_slow U19 ( .A1(n18), .A2(in0[6]), .ZN(n5) );
  NAND2_X4_slow U20 ( .A1(n16), .A2(n17), .ZN(out[0]) );
  NAND2_X4_slow U21 ( .A1(n15), .A2(n14), .ZN(out[1]) );
  NAND2_X4_slow U22 ( .A1(n13), .A2(n12), .ZN(out[2]) );
  NAND2_X4_slow U23 ( .A1(n11), .A2(n10), .ZN(out[3]) );
  NAND2_X4_slow U24 ( .A1(n9), .A2(n8), .ZN(out[4]) );
  NAND2_X4_slow U25 ( .A1(n7), .A2(n6), .ZN(out[5]) );
  NAND2_X4_slow U26 ( .A1(n5), .A2(n4), .ZN(out[6]) );
  NAND2_X4_slow U27 ( .A1(n3), .A2(n2), .ZN(out[7]) );
  NAND2_X4_slow U28 ( .A1(n19), .A2(in0[7]), .ZN(n3) );
  NAND2_X4_slow U29 ( .A1(n20), .A2(in0[0]), .ZN(n17) );
  NAND2_X4_slow U30 ( .A1(n23), .A2(in0[1]), .ZN(n15) );
  NAND2_X4_slow U31 ( .A1(n18), .A2(in0[2]), .ZN(n13) );
  NAND2_X4_slow U32 ( .A1(n19), .A2(in0[4]), .ZN(n9) );
  NAND2_X4_slow U33 ( .A1(n23), .A2(in0[5]), .ZN(n7) );
  INV_X32_slow U34 ( .A(n26), .ZN(n1) );
endmodule


module exoor_44 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_45 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_46 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_47 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_48 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_49 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_50 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_51 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_52 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module fa_44 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U1 ( .A(c_in), .B(n7), .Z(s) );
  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  BUF_X32_slow U2 ( .A(n7), .Z(n4) );
  NAND2_X4_slow U3 ( .A1(n6), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n6) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n4), .ZN(n5) );
endmodule


module fa_45 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XNOR2_X2_slow U1 ( .A(c_in), .B(n4), .ZN(s) );
  XNOR2_X2_slow U2 ( .A(b), .B(a), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n6) );
  NAND2_X4_slow U4 ( .A1(c_in), .A2(n7), .ZN(n5) );
  XOR2_X2_slow U5 ( .A(b), .B(a), .Z(n7) );
  NAND2_X4_slow U6 ( .A1(n5), .A2(n6), .ZN(c_out) );
endmodule


module fa_46 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  XNOR2_X2_slow U1 ( .A(c_in), .B(n4), .ZN(s) );
  XNOR2_X2_slow U2 ( .A(b), .B(a), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n6), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n7), .ZN(n5) );
endmodule


module fa_47 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  XNOR2_X2_slow U1 ( .A(c_in), .B(n4), .ZN(s) );
  XNOR2_X2_slow U2 ( .A(b), .B(a), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(b), .A2(a), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n6), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n7), .ZN(n5) );
endmodule


module fa_48 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  NAND2_X4_slow U1 ( .A1(b), .A2(a), .ZN(n6) );
  XNOR2_X2_slow U2 ( .A(n4), .B(c_in), .ZN(s) );
  XNOR2_X2_slow U3 ( .A(b), .B(a), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n6), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n7), .ZN(n5) );
endmodule


module fa_49 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U1 ( .A(c_in), .B(n7), .Z(s) );
  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  BUF_X32_slow U2 ( .A(b), .Z(n4) );
  NAND2_X4_slow U3 ( .A1(a), .A2(n4), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n6), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n7), .ZN(n5) );
endmodule


module fa_50 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  XNOR2_X2_slow U1 ( .A(c_in), .B(n4), .ZN(s) );
  XNOR2_X2_slow U2 ( .A(b), .B(a), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(b), .A2(a), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n7), .ZN(n5) );
  NAND2_X4_slow U6 ( .A1(n5), .A2(n6), .ZN(c_out) );
endmodule


module fa_51 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n7) );
  XOR2_X2_slow U1 ( .A(c_in), .B(n4), .Z(s) );
  BUF_X32_slow U2 ( .A(n7), .Z(n4) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(n6), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(n7), .A2(c_in), .ZN(n5) );
endmodule


module fa_52 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n4), .ZN(c_out) );
endmodule


module sobel_add_nb_bitwidth9_1 ( a, b, ans_out, cout, subtract );
  input [8:0] a;
  input [8:0] b;
  output [8:0] ans_out;
  input subtract;
  output cout;

  wire   [8:0] bcomp;
  wire   [7:0] carry;

  exoor_52 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_52 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), 
        .c_out(carry[0]) );
  exoor_51 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_51 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_50 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_50 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(
        ans_out[2]), .c_out(carry[2]) );
  exoor_49 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_49 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(
        ans_out[3]), .c_out(carry[3]) );
  exoor_48 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_48 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(
        ans_out[4]), .c_out(carry[4]) );
  exoor_47 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_47 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(
        ans_out[5]), .c_out(carry[5]) );
  exoor_46 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_46 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(
        ans_out[6]), .c_out(carry[6]) );
  exoor_45 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_45 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(
        ans_out[7]), .c_out(carry[7]) );
  exoor_44 X_bitwidth ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_44 fa_bitwidth ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(ans_out[8]), 
        .c_out(cout) );
endmodule


module exoor_53 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_54 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_55 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_56 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_57 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_58 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_59 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_60 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_61 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module fa_53 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module fa_54 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n4), .ZN(c_out) );
endmodule


module fa_55 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n4), .ZN(c_out) );
endmodule


module fa_56 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_57 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_58 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_59 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n4), .ZN(c_out) );
endmodule


module fa_60 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n4), .ZN(c_out) );
endmodule


module fa_61 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U1 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  XOR2_X2_slow U5 ( .A(c_in), .B(n6), .Z(s) );
endmodule


module sobel_add_nb_bitwidth9_2 ( a, b, ans_out, cout, subtract );
  input [8:0] a;
  input [8:0] b;
  output [8:0] ans_out;
  input subtract;
  output cout;

  wire   [8:0] bcomp;
  wire   [7:0] carry;

  exoor_61 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_61 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), 
        .c_out(carry[0]) );
  exoor_60 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_60 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_59 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_59 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(
        ans_out[2]), .c_out(carry[2]) );
  exoor_58 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_58 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(
        ans_out[3]), .c_out(carry[3]) );
  exoor_57 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_57 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(
        ans_out[4]), .c_out(carry[4]) );
  exoor_56 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_56 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(
        ans_out[5]), .c_out(carry[5]) );
  exoor_55 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_55 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(
        ans_out[6]), .c_out(carry[6]) );
  exoor_54 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_54 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(
        ans_out[7]), .c_out(carry[7]) );
  exoor_53 X_bitwidth ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_53 fa_bitwidth ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(ans_out[8]), 
        .c_out(cout) );
endmodule


module exoor_62 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_63 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_64 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_65 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_66 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_67 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_68 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_69 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_70 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module fa_62 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module fa_63 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_64 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_65 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_66 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_67 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_68 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_69 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_70 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module sobel_add_nb_bitwidth9_3 ( a, b, ans_out, cout, subtract );
  input [8:0] a;
  input [8:0] b;
  output [8:0] ans_out;
  input subtract;
  output cout;

  wire   [8:0] bcomp;
  wire   [7:0] carry;

  exoor_70 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_70 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), 
        .c_out(carry[0]) );
  exoor_69 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_69 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_68 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_68 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(
        ans_out[2]), .c_out(carry[2]) );
  exoor_67 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_67 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(
        ans_out[3]), .c_out(carry[3]) );
  exoor_66 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_66 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(
        ans_out[4]), .c_out(carry[4]) );
  exoor_65 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_65 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(
        ans_out[5]), .c_out(carry[5]) );
  exoor_64 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_64 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(
        ans_out[6]), .c_out(carry[6]) );
  exoor_63 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_63 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(
        ans_out[7]), .c_out(carry[7]) );
  exoor_62 X_bitwidth ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_62 fa_bitwidth ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(ans_out[8]), 
        .c_out(cout) );
endmodule


module exoor_71 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_72 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_73 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_74 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_75 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_76 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_77 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_78 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_79 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module fa_71 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_72 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(n4), .A2(n5), .ZN(c_out) );
endmodule


module fa_73 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n4), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_74 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n4), .A2(n5), .ZN(c_out) );
endmodule


module fa_75 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n4), .A2(n5), .ZN(c_out) );
endmodule


module fa_76 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n4), .A2(n5), .ZN(c_out) );
endmodule


module fa_77 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n4), .A2(n5), .ZN(c_out) );
endmodule


module fa_78 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n4), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_79 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  BUF_X32_slow U1 ( .A(n7), .Z(n4) );
  XOR2_X2_slow U2 ( .A(c_in), .B(n4), .Z(s) );
  NAND2_X4_slow U3 ( .A1(b), .A2(a), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n6), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(n7), .A2(c_in), .ZN(n5) );
endmodule


module sobel_add_nb_bitwidth9_4 ( a, b, ans_out, cout, subtract );
  input [8:0] a;
  input [8:0] b;
  output [8:0] ans_out;
  input subtract;
  output cout;

  wire   [8:0] bcomp;
  wire   [7:0] carry;

  exoor_79 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_79 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), 
        .c_out(carry[0]) );
  exoor_78 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_78 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_77 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_77 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(
        ans_out[2]), .c_out(carry[2]) );
  exoor_76 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_76 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(
        ans_out[3]), .c_out(carry[3]) );
  exoor_75 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_75 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(
        ans_out[4]), .c_out(carry[4]) );
  exoor_74 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_74 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(
        ans_out[5]), .c_out(carry[5]) );
  exoor_73 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_73 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(
        ans_out[6]), .c_out(carry[6]) );
  exoor_72 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_72 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(
        ans_out[7]), .c_out(carry[7]) );
  exoor_71 X_bitwidth ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_71 fa_bitwidth ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(ans_out[8]), 
        .c_out(cout) );
endmodule


module exoor_90 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_91 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_92 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(n1), .B(in1), .ZN(out) );
endmodule


module exoor_93 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_94 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_95 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_96 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_97 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(n1), .B(in1), .ZN(out) );
endmodule


module exoor_98 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module fa_90 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7, n8;

  XOR2_X2_slow U1 ( .A(n8), .B(c_in), .Z(s) );
  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n8) );
  BUF_X32_slow U2 ( .A(n8), .Z(n4) );
  NAND2_X4_slow U3 ( .A1(n7), .A2(n6), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n7) );
  NAND2_X4_slow U6 ( .A1(n5), .A2(n4), .ZN(n6) );
  BUF_X32_slow U7 ( .A(c_in), .Z(n5) );
endmodule


module fa_91 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U1 ( .A(c_in), .B(n4), .Z(s) );
  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  XOR2_X2_slow U2 ( .A(b), .B(a), .Z(n4) );
  NAND2_X4_slow U3 ( .A1(b), .A2(a), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n6), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n7), .ZN(n5) );
endmodule


module fa_92 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U1 ( .A(c_in), .B(n4), .Z(s) );
  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  XOR2_X2_slow U2 ( .A(b), .B(a), .Z(n4) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n7), .ZN(n5) );
  NAND2_X4_slow U6 ( .A1(n5), .A2(n6), .ZN(c_out) );
endmodule


module fa_93 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(n6), .B(c_in), .Z(s) );
  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n4), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_94 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U1 ( .A(c_in), .B(n4), .Z(s) );
  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  XOR2_X2_slow U2 ( .A(b), .B(a), .Z(n4) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n7), .ZN(n5) );
  NAND2_X4_slow U6 ( .A1(n5), .A2(n6), .ZN(c_out) );
endmodule


module fa_95 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n4), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_96 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(b), .A2(a), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n4), .A2(n5), .ZN(c_out) );
endmodule


module fa_97 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U1 ( .A(c_in), .B(n4), .Z(s) );
  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  BUF_X32_slow U2 ( .A(n7), .Z(n4) );
  NAND2_X4_slow U3 ( .A1(n6), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(b), .A2(a), .ZN(n6) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n7), .ZN(n5) );
endmodule


module fa_98 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module sobel_add_nb_bitwidth9_5 ( a, b, ans_out, cout, subtract );
  input [8:0] a;
  input [8:0] b;
  output [8:0] ans_out;
  input subtract;
  output cout;

  wire   [8:0] bcomp;
  wire   [7:0] carry;

  exoor_98 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_98 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), 
        .c_out(carry[0]) );
  exoor_97 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_97 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_96 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_96 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(
        ans_out[2]), .c_out(carry[2]) );
  exoor_95 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_95 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(
        ans_out[3]), .c_out(carry[3]) );
  exoor_94 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_94 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(
        ans_out[4]), .c_out(carry[4]) );
  exoor_93 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_93 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(
        ans_out[5]), .c_out(carry[5]) );
  exoor_92 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_92 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(
        ans_out[6]), .c_out(carry[6]) );
  exoor_91 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_91 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(
        ans_out[7]), .c_out(carry[7]) );
  exoor_90 X_bitwidth ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_90 fa_bitwidth ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(ans_out[8]), 
        .c_out(cout) );
endmodule


module exoor_99 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_100 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_101 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_102 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_103 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_104 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_105 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_106 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_107 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in1), .B(in2), .Z(out) );
endmodule


module fa_99 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module fa_100 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n4), .ZN(c_out) );
endmodule


module fa_101 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_102 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n4), .ZN(c_out) );
endmodule


module fa_103 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n4), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_104 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_105 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n4), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_106 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n4), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_107 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n6) );
  NAND2_X4_slow U1 ( .A1(b), .A2(a), .ZN(n5) );
  XOR2_X2_slow U2 ( .A(n6), .B(c_in), .Z(s) );
  NAND2_X4_slow U3 ( .A1(n4), .A2(n5), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(n6), .A2(c_in), .ZN(n4) );
endmodule


module sobel_add_nb_bitwidth9_6 ( a, b, ans_out, cout, subtract );
  input [8:0] a;
  input [8:0] b;
  output [8:0] ans_out;
  input subtract;
  output cout;

  wire   [8:0] bcomp;
  wire   [7:0] carry;

  exoor_107 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_107 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), 
        .c_out(carry[0]) );
  exoor_106 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_106 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_105 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_105 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(
        ans_out[2]), .c_out(carry[2]) );
  exoor_104 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_104 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(
        ans_out[3]), .c_out(carry[3]) );
  exoor_103 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_103 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(
        ans_out[4]), .c_out(carry[4]) );
  exoor_102 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_102 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(
        ans_out[5]), .c_out(carry[5]) );
  exoor_101 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_101 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(
        ans_out[6]), .c_out(carry[6]) );
  exoor_100 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_100 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(
        ans_out[7]), .c_out(carry[7]) );
  exoor_99 X_bitwidth ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_99 fa_bitwidth ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(ans_out[8]), 
        .c_out(cout) );
endmodule


module exoor_108 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_109 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_110 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_111 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_112 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_113 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_114 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_115 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_116 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module fa_108 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module fa_109 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_110 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_111 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_112 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_113 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_114 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_115 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_116 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module sobel_add_nb_bitwidth9_7 ( a, b, ans_out, cout, subtract );
  input [8:0] a;
  input [8:0] b;
  output [8:0] ans_out;
  input subtract;
  output cout;

  wire   [8:0] bcomp;
  wire   [7:0] carry;

  exoor_116 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_116 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), 
        .c_out(carry[0]) );
  exoor_115 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_115 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_114 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_114 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(
        ans_out[2]), .c_out(carry[2]) );
  exoor_113 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_113 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(
        ans_out[3]), .c_out(carry[3]) );
  exoor_112 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_112 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(
        ans_out[4]), .c_out(carry[4]) );
  exoor_111 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_111 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(
        ans_out[5]), .c_out(carry[5]) );
  exoor_110 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_110 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(
        ans_out[6]), .c_out(carry[6]) );
  exoor_109 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_109 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(
        ans_out[7]), .c_out(carry[7]) );
  exoor_108 X_bitwidth ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_108 fa_bitwidth ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(ans_out[8]), 
        .c_out(cout) );
endmodule


module exoor_34 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_35 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_36 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_37 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_38 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_39 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_40 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_41 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_42 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in1), .B(in2), .Z(out) );
endmodule


module exoor_43 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module fa_34 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module fa_35 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n1, n4, n5, n6, n7;

  XNOR2_X2_slow U1 ( .A(c_in), .B(n1), .ZN(s) );
  XNOR2_X2_slow U2 ( .A(b), .B(a), .ZN(n1) );
  BUF_X32_slow U3 ( .A(b), .Z(n4) );
  XOR2_X2_slow U4 ( .A(n4), .B(a), .Z(n5) );
  NAND2_X4_slow U5 ( .A1(n7), .A2(n6), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(a), .A2(n4), .ZN(n7) );
  NAND2_X4_slow U7 ( .A1(c_in), .A2(n5), .ZN(n6) );
endmodule


module fa_36 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n7) );
  XOR2_X2_slow U1 ( .A(c_in), .B(n4), .Z(s) );
  BUF_X32_slow U2 ( .A(n7), .Z(n4) );
  NAND2_X4_slow U3 ( .A1(b), .A2(a), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n7), .ZN(n5) );
  NAND2_X4_slow U6 ( .A1(n5), .A2(n6), .ZN(c_out) );
endmodule


module fa_37 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7, n8;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n8) );
  BUF_X32_slow U1 ( .A(c_in), .Z(n4) );
  BUF_X32_slow U2 ( .A(n8), .Z(n5) );
  XOR2_X2_slow U3 ( .A(n4), .B(n5), .Z(s) );
  NAND2_X4_slow U5 ( .A1(b), .A2(a), .ZN(n7) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n8), .ZN(n6) );
  NAND2_X4_slow U7 ( .A1(n6), .A2(n7), .ZN(c_out) );
endmodule


module fa_38 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n6) );
  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  NAND2_X4_slow U2 ( .A1(b), .A2(a), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n4), .A2(n5), .ZN(c_out) );
endmodule


module fa_39 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7, n8;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n8) );
  BUF_X32_slow U1 ( .A(n8), .Z(n4) );
  BUF_X32_slow U2 ( .A(c_in), .Z(n5) );
  XOR2_X2_slow U3 ( .A(n5), .B(n4), .Z(s) );
  NAND2_X4_slow U5 ( .A1(n7), .A2(n6), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(a), .A2(b), .ZN(n7) );
  NAND2_X4_slow U7 ( .A1(c_in), .A2(n8), .ZN(n6) );
endmodule


module fa_40 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7, n8;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n8) );
  BUF_X32_slow U1 ( .A(c_in), .Z(n4) );
  BUF_X32_slow U2 ( .A(n8), .Z(n5) );
  XOR2_X2_slow U3 ( .A(n4), .B(n5), .Z(s) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n7) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n8), .ZN(n6) );
  NAND2_X4_slow U7 ( .A1(n7), .A2(n6), .ZN(c_out) );
endmodule


module fa_41 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(n6), .A2(c_in), .ZN(n4) );
endmodule


module fa_42 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n4), .ZN(c_out) );
endmodule


module fa_43 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module sobel_add_nb_bitwidth10_1 ( a, b, ans_out, cout, subtract );
  input [9:0] a;
  input [9:0] b;
  output [9:0] ans_out;
  input subtract;
  output cout;

  wire   [9:0] bcomp;
  wire   [8:0] carry;

  exoor_43 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_43 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), 
        .c_out(carry[0]) );
  exoor_42 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_42 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_41 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_41 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(
        ans_out[2]), .c_out(carry[2]) );
  exoor_40 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_40 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(
        ans_out[3]), .c_out(carry[3]) );
  exoor_39 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_39 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(
        ans_out[4]), .c_out(carry[4]) );
  exoor_38 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_38 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(
        ans_out[5]), .c_out(carry[5]) );
  exoor_37 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_37 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(
        ans_out[6]), .c_out(carry[6]) );
  exoor_36 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_36 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(
        ans_out[7]), .c_out(carry[7]) );
  exoor_35 gen_fa_8__X_i ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_35 gen_fa_8__fa_i ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(
        ans_out[8]), .c_out(carry[8]) );
  exoor_34 X_bitwidth ( .in1(b[9]), .in2(subtract), .out(bcomp[9]) );
  fa_34 fa_bitwidth ( .a(a[9]), .b(bcomp[9]), .c_in(carry[8]), .s(ans_out[9]), 
        .c_out(cout) );
endmodule


module not_11b_1 ( in, out );
  input [10:0] in;
  output [10:0] out;


  INV_X32_slow U1 ( .A(in[7]), .ZN(out[7]) );
  INV_X32_slow U2 ( .A(in[3]), .ZN(out[3]) );
  INV_X32_slow U3 ( .A(in[5]), .ZN(out[5]) );
  INV_X32_slow U4 ( .A(in[4]), .ZN(out[4]) );
  INV_X32_slow U5 ( .A(in[2]), .ZN(out[2]) );
  INV_X32_slow U6 ( .A(in[6]), .ZN(out[6]) );
  INV_X32_slow U7 ( .A(in[0]), .ZN(out[0]) );
  INV_X32_slow U8 ( .A(in[1]), .ZN(out[1]) );
  INV_X32_slow U9 ( .A(in[10]), .ZN(out[10]) );
  INV_X32_slow U10 ( .A(in[9]), .ZN(out[9]) );
  INV_X32_slow U11 ( .A(in[8]), .ZN(out[8]) );
endmodule


module exoor_1 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_2 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_3 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_4 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_5 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_6 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_7 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_8 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_9 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_10 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module exoor_11 ( in1, in2, out );
  input in1, in2;
  output out;
  wire   n1;

  INV_X32_slow U1 ( .A(in2), .ZN(n1) );
  XNOR2_X2_slow U2 ( .A(in1), .B(n1), .ZN(out) );
endmodule


module fa_1 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module fa_2 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_3 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n2, n3, n1, n4, n5;

  NAND2_X4_slow U1 ( .A1(a), .A2(b), .ZN(n2) );
  INV_X32_slow U2 ( .A(n5), .ZN(n4) );
  BUF_X32_slow U3 ( .A(c_in), .Z(n1) );
  XNOR2_X2_slow U4 ( .A(c_in), .B(n5), .ZN(s) );
  XNOR2_X2_slow U5 ( .A(a), .B(b), .ZN(n5) );
  NAND2_X4_slow U6 ( .A1(n4), .A2(n1), .ZN(n3) );
  NAND2_X4_slow U7 ( .A1(n2), .A2(n3), .ZN(c_out) );
endmodule


module fa_4 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n7) );
  BUF_X32_slow U1 ( .A(c_in), .Z(n4) );
  XOR2_X2_slow U2 ( .A(n4), .B(n7), .Z(s) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n6), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n7), .ZN(n5) );
endmodule


module fa_5 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n7) );
  BUF_X32_slow U1 ( .A(c_in), .Z(n4) );
  XOR2_X2_slow U2 ( .A(n4), .B(n7), .Z(s) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n6) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n7), .ZN(n5) );
  NAND2_X4_slow U6 ( .A1(n5), .A2(n6), .ZN(c_out) );
endmodule


module fa_6 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7, n8;

  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n8) );
  INV_X32_slow U1 ( .A(c_in), .ZN(n4) );
  INV_X32_slow U2 ( .A(n4), .ZN(n5) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n7) );
  XOR2_X2_slow U5 ( .A(n5), .B(n8), .Z(s) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n8), .ZN(n6) );
  NAND2_X4_slow U7 ( .A1(n6), .A2(n7), .ZN(c_out) );
endmodule


module fa_7 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n7) );
  BUF_X32_slow U1 ( .A(c_in), .Z(n4) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n6) );
  XOR2_X2_slow U3 ( .A(n4), .B(n7), .Z(s) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n7), .ZN(n5) );
  NAND2_X4_slow U6 ( .A1(n5), .A2(n6), .ZN(c_out) );
endmodule


module fa_8 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n7) );
  BUF_X32_slow U1 ( .A(c_in), .Z(n4) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n6) );
  XOR2_X2_slow U3 ( .A(n4), .B(n7), .Z(s) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n7), .ZN(n5) );
  NAND2_X4_slow U6 ( .A1(n5), .A2(n6), .ZN(c_out) );
endmodule


module fa_9 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7;

  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n7) );
  BUF_X32_slow U1 ( .A(c_in), .Z(n4) );
  NAND2_X4_slow U2 ( .A1(a), .A2(b), .ZN(n6) );
  XOR2_X2_slow U3 ( .A(n7), .B(n4), .Z(s) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n6), .ZN(c_out) );
  NAND2_X4_slow U6 ( .A1(c_in), .A2(n7), .ZN(n5) );
endmodule


module fa_10 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6, n7, n8;

  XOR2_X2_slow U4 ( .A(b), .B(a), .Z(n8) );
  BUF_X32_slow U1 ( .A(n8), .Z(n4) );
  BUF_X32_slow U2 ( .A(c_in), .Z(n5) );
  XOR2_X2_slow U3 ( .A(n5), .B(n4), .Z(s) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n7) );
  NAND2_X4_slow U6 ( .A1(n6), .A2(n7), .ZN(c_out) );
  NAND2_X4_slow U7 ( .A1(n8), .A2(c_in), .ZN(n6) );
endmodule


module fa_11 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U2 ( .A(b), .B(a), .Z(n6) );
  NAND2_X4_slow U3 ( .A1(b), .A2(a), .ZN(n5) );
  NAND2_X4_slow U4 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(n5), .A2(n4), .ZN(c_out) );
endmodule


module sobel_add_nb_bitwidth11_1 ( a, b, ans_out, cout, subtract );
  input [10:0] a;
  input [10:0] b;
  output [10:0] ans_out;
  input subtract;
  output cout;

  wire   [10:0] bcomp;
  wire   [9:0] carry;

  exoor_11 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_11 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), 
        .c_out(carry[0]) );
  exoor_10 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_10 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_9 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_9 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(ans_out[2]), .c_out(carry[2]) );
  exoor_8 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_8 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(ans_out[3]), .c_out(carry[3]) );
  exoor_7 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_7 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(ans_out[4]), .c_out(carry[4]) );
  exoor_6 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_6 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(ans_out[5]), .c_out(carry[5]) );
  exoor_5 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_5 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(ans_out[6]), .c_out(carry[6]) );
  exoor_4 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_4 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(ans_out[7]), .c_out(carry[7]) );
  exoor_3 gen_fa_8__X_i ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_3 gen_fa_8__fa_i ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(ans_out[8]), .c_out(carry[8]) );
  exoor_2 gen_fa_9__X_i ( .in1(b[9]), .in2(subtract), .out(bcomp[9]) );
  fa_2 gen_fa_9__fa_i ( .a(a[9]), .b(bcomp[9]), .c_in(carry[8]), .s(ans_out[9]), .c_out(carry[9]) );
  exoor_1 X_bitwidth ( .in1(b[10]), .in2(subtract), .out(bcomp[10]) );
  fa_1 fa_bitwidth ( .a(a[10]), .b(bcomp[10]), .c_in(carry[9]), .s(ans_out[10]), .c_out(cout) );
endmodule


module exoor_12 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_13 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_14 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_15 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_16 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_17 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_18 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_19 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_20 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_21 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module exoor_22 ( in1, in2, out );
  input in1, in2;
  output out;


  XOR2_X2_slow U1 ( .A(in2), .B(in1), .Z(out) );
endmodule


module fa_12 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module fa_13 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_14 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_15 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_16 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_17 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_18 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U2 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U3 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U4 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_19 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_20 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_21 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(a), .A2(b), .ZN(n5) );
  NAND2_X4_slow U5 ( .A1(c_in), .A2(n6), .ZN(n4) );
endmodule


module fa_22 ( a, b, c_in, s, c_out );
  input a, b, c_in;
  output s, c_out;
  wire   n4, n5, n6;

  XOR2_X2_slow U1 ( .A(c_in), .B(n6), .Z(s) );
  XOR2_X2_slow U4 ( .A(a), .B(b), .Z(n6) );
  NAND2_X4_slow U2 ( .A1(n5), .A2(n4), .ZN(c_out) );
  NAND2_X4_slow U3 ( .A1(c_in), .A2(n6), .ZN(n4) );
  NAND2_X4_slow U5 ( .A1(a), .A2(b), .ZN(n5) );
endmodule


module sobel_add_nb_bitwidth11_2 ( a, b, ans_out, cout, subtract );
  input [10:0] a;
  input [10:0] b;
  output [10:0] ans_out;
  input subtract;
  output cout;

  wire   [10:0] bcomp;
  wire   [9:0] carry;

  exoor_22 X_0 ( .in1(b[0]), .in2(subtract), .out(bcomp[0]) );
  fa_22 fa_0 ( .a(a[0]), .b(bcomp[0]), .c_in(subtract), .s(ans_out[0]), 
        .c_out(carry[0]) );
  exoor_21 gen_fa_1__X_i ( .in1(b[1]), .in2(subtract), .out(bcomp[1]) );
  fa_21 gen_fa_1__fa_i ( .a(a[1]), .b(bcomp[1]), .c_in(carry[0]), .s(
        ans_out[1]), .c_out(carry[1]) );
  exoor_20 gen_fa_2__X_i ( .in1(b[2]), .in2(subtract), .out(bcomp[2]) );
  fa_20 gen_fa_2__fa_i ( .a(a[2]), .b(bcomp[2]), .c_in(carry[1]), .s(
        ans_out[2]), .c_out(carry[2]) );
  exoor_19 gen_fa_3__X_i ( .in1(b[3]), .in2(subtract), .out(bcomp[3]) );
  fa_19 gen_fa_3__fa_i ( .a(a[3]), .b(bcomp[3]), .c_in(carry[2]), .s(
        ans_out[3]), .c_out(carry[3]) );
  exoor_18 gen_fa_4__X_i ( .in1(b[4]), .in2(subtract), .out(bcomp[4]) );
  fa_18 gen_fa_4__fa_i ( .a(a[4]), .b(bcomp[4]), .c_in(carry[3]), .s(
        ans_out[4]), .c_out(carry[4]) );
  exoor_17 gen_fa_5__X_i ( .in1(b[5]), .in2(subtract), .out(bcomp[5]) );
  fa_17 gen_fa_5__fa_i ( .a(a[5]), .b(bcomp[5]), .c_in(carry[4]), .s(
        ans_out[5]), .c_out(carry[5]) );
  exoor_16 gen_fa_6__X_i ( .in1(b[6]), .in2(subtract), .out(bcomp[6]) );
  fa_16 gen_fa_6__fa_i ( .a(a[6]), .b(bcomp[6]), .c_in(carry[5]), .s(
        ans_out[6]), .c_out(carry[6]) );
  exoor_15 gen_fa_7__X_i ( .in1(b[7]), .in2(subtract), .out(bcomp[7]) );
  fa_15 gen_fa_7__fa_i ( .a(a[7]), .b(bcomp[7]), .c_in(carry[6]), .s(
        ans_out[7]), .c_out(carry[7]) );
  exoor_14 gen_fa_8__X_i ( .in1(b[8]), .in2(subtract), .out(bcomp[8]) );
  fa_14 gen_fa_8__fa_i ( .a(a[8]), .b(bcomp[8]), .c_in(carry[7]), .s(
        ans_out[8]), .c_out(carry[8]) );
  exoor_13 gen_fa_9__X_i ( .in1(b[9]), .in2(subtract), .out(bcomp[9]) );
  fa_13 gen_fa_9__fa_i ( .a(a[9]), .b(bcomp[9]), .c_in(carry[8]), .s(
        ans_out[9]), .c_out(carry[9]) );
  exoor_12 X_bitwidth ( .in1(b[10]), .in2(subtract), .out(bcomp[10]) );
  fa_12 fa_bitwidth ( .a(a[10]), .b(bcomp[10]), .c_in(carry[9]), .s(
        ans_out[10]), .c_out(cout) );
endmodule


module mux_11b_1 ( in0, in1, sel, out );
  input [10:0] in0;
  input [10:0] in1;
  output [10:0] out;
  input sel;
  wire   n16, n19, n22, net295, net427, net529, net534, net561, net655, n1,
         n18, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36,
         n37, n38, n39, n40, n41;

  INV_X32_slow U1 ( .A(net427), .ZN(net655) );
  NAND2_X4_slow U2 ( .A1(n18), .A2(in1[1]), .ZN(n1) );
  NAND2_X4_slow U3 ( .A1(n18), .A2(in1[0]), .ZN(n22) );
  NAND2_X4_slow U4 ( .A1(net655), .A2(in1[3]), .ZN(n29) );
  NAND2_X4_slow U5 ( .A1(in1[5]), .A2(net534), .ZN(n33) );
  BUF_X32_slow U6 ( .A(sel), .Z(n18) );
  NAND2_X4_slow U7 ( .A1(n18), .A2(in1[2]), .ZN(n16) );
  INV_X32_slow U8 ( .A(sel), .ZN(net427) );
  NAND2_X4_slow U9 ( .A1(n1), .A2(n19), .ZN(out[1]) );
  NAND2_X4_slow U10 ( .A1(in1[6]), .A2(net534), .ZN(n35) );
  NAND2_X4_slow U11 ( .A1(in1[7]), .A2(net529), .ZN(n37) );
  INV_X32_slow U12 ( .A(net655), .ZN(net561) );
  BUF_X32_slow U13 ( .A(net655), .Z(net534) );
  BUF_X32_slow U14 ( .A(net655), .Z(net529) );
  NAND2_X4_slow U15 ( .A1(in1[8]), .A2(net529), .ZN(n39) );
  BUF_X32_slow U16 ( .A(net427), .Z(net295) );
  NAND2_X4_slow U17 ( .A1(in0[6]), .A2(net561), .ZN(n34) );
  NAND2_X4_slow U18 ( .A1(n35), .A2(n34), .ZN(out[6]) );
  NAND2_X4_slow U19 ( .A1(n27), .A2(n16), .ZN(out[2]) );
  NAND2_X4_slow U20 ( .A1(in0[2]), .A2(net295), .ZN(n27) );
  NAND2_X4_slow U21 ( .A1(n31), .A2(n30), .ZN(out[4]) );
  NAND2_X4_slow U22 ( .A1(in0[4]), .A2(net295), .ZN(n30) );
  NAND2_X4_slow U23 ( .A1(n39), .A2(n38), .ZN(out[8]) );
  NAND2_X4_slow U24 ( .A1(n29), .A2(n28), .ZN(out[3]) );
  NAND2_X4_slow U25 ( .A1(n33), .A2(n32), .ZN(out[5]) );
  NAND2_X4_slow U26 ( .A1(n37), .A2(n36), .ZN(out[7]) );
  NAND2_X4_slow U27 ( .A1(n41), .A2(n40), .ZN(out[9]) );
  NAND2_X4_slow U28 ( .A1(n26), .A2(n25), .ZN(out[10]) );
  NAND2_X4_slow U29 ( .A1(in0[7]), .A2(net561), .ZN(n36) );
  NAND2_X4_slow U30 ( .A1(in0[5]), .A2(net561), .ZN(n32) );
  NAND2_X4_slow U31 ( .A1(in0[3]), .A2(net295), .ZN(n28) );
  NAND2_X4_slow U32 ( .A1(net529), .A2(in1[9]), .ZN(n41) );
  NAND2_X4_slow U33 ( .A1(in1[10]), .A2(net529), .ZN(n26) );
  NAND2_X4_slow U34 ( .A1(in0[8]), .A2(net561), .ZN(n38) );
  NAND2_X4_slow U35 ( .A1(in1[4]), .A2(net655), .ZN(n31) );
  NAND2_X4_slow U36 ( .A1(net427), .A2(in0[1]), .ZN(n19) );
  NAND2_X4_slow U37 ( .A1(n24), .A2(n22), .ZN(out[0]) );
  NAND2_X4_slow U38 ( .A1(in0[9]), .A2(net561), .ZN(n40) );
  NAND2_X4_slow U39 ( .A1(in0[10]), .A2(net561), .ZN(n25) );
  NAND2_X4_slow U40 ( .A1(net427), .A2(in0[0]), .ZN(n24) );
endmodule


module sobel ( p0, p1, p2, p3, p5, p6, p7, p8, out );
  input [8:0] p0;
  input [8:0] p1;
  input [8:0] p2;
  input [8:0] p3;
  input [8:0] p5;
  input [8:0] p6;
  input [8:0] p7;
  input [8:0] p8;
  output [7:0] out;
  wire   center_2_9_, gx_10_, center_4_9_, gy_10_, n1, n2, n3, n4, n5, n6, n7,
         n8, n9, n10, n11, SYNOPSYS_UNCONNECTED_1, SYNOPSYS_UNCONNECTED_2;
  wire   [8:0] p2_p0;
  wire   [8:0] p5_p3;
  wire   [8:0] p8_p6;
  wire   [8:0] center_rm;
  wire   [9:0] gx_temp;
  wire   [8:0] p0_p6;
  wire   [8:0] p1_p7;
  wire   [8:0] p2_p8;
  wire   [8:0] center_tm;
  wire   [9:0] gy_temp;
  wire   [10:0] gx_comp;
  wire   [10:0] gy_comp;
  wire   [10:0] gx_2s_comp;
  wire   [10:0] gy_2s_comp;
  wire   [10:0] abs_gx;
  wire   [10:0] abs_gy;
  wire   [8:0] sum;

  sobel_add_nb_bitwidth9_0 S0 ( .a(p2), .b(p0), .ans_out(p2_p0), .subtract(
        1'b1) );
  sobel_add_nb_bitwidth9_7 S1 ( .a(p5), .b(p3), .ans_out(p5_p3), .subtract(
        1'b1) );
  sobel_add_nb_bitwidth9_6 S2 ( .a(p8), .b(p6), .ans_out(p8_p6), .subtract(
        1'b1) );
  sobel_add_nb_bitwidth9_5 S3 ( .a(p2_p0), .b(p8_p6), .ans_out(center_rm), 
        .cout(center_2_9_), .subtract(1'b0) );
  sobel_add_nb_bitwidth10_0 S4 ( .a({p5_p3, 1'b0}), .b({center_2_9_, center_rm}), .ans_out(gx_temp), .cout(gx_10_), .subtract(1'b0) );
  sobel_add_nb_bitwidth9_4 S5 ( .a(p0), .b(p6), .ans_out(p0_p6), .subtract(
        1'b1) );
  sobel_add_nb_bitwidth9_3 S6 ( .a(p1), .b(p7), .ans_out(p1_p7), .subtract(
        1'b1) );
  sobel_add_nb_bitwidth9_2 S7 ( .a(p2), .b(p8), .ans_out(p2_p8), .subtract(
        1'b1) );
  sobel_add_nb_bitwidth9_1 S8 ( .a(p2_p8), .b(p0_p6), .ans_out(center_tm), 
        .cout(center_4_9_), .subtract(1'b0) );
  sobel_add_nb_bitwidth10_1 S9 ( .a({p1_p7, 1'b0}), .b({center_4_9_, center_tm}), .ans_out(gy_temp), .cout(gy_10_), .subtract(1'b0) );
  not_11b_0 N0 ( .in({gx_10_, gx_temp[9], n7, n9, n1, n6, gx_temp[4:0]}), 
        .out(gx_comp) );
  not_11b_1 N1 ( .in({gy_10_, gy_temp[9], n11, n2, n5, n4, n8, n10, n3, 
        gy_temp[1:0]}), .out(gy_comp) );
  sobel_add_nb_bitwidth11_0 S10 ( .a({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b1}), .b(gx_comp), .ans_out(gx_2s_comp), 
        .subtract(1'b0) );
  sobel_add_nb_bitwidth11_2 S11 ( .a({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b1}), .b(gy_comp), .ans_out(gy_2s_comp), 
        .subtract(1'b0) );
  mux_11b_0 M0 ( .in0({gx_10_, gx_temp[9], n7, n9, n1, n6, gx_temp[4:0]}), 
        .in1(gx_2s_comp), .sel(gx_temp[8]), .out(abs_gx) );
  mux_11b_1 M1 ( .in0({gy_10_, gy_temp[9], n11, n2, n5, n4, n8, n10, n3, 
        gy_temp[1:0]}), .in1(gy_2s_comp), .sel(gy_temp[8]), .out(abs_gy) );
  sobel_add_nb_bitwidth11_1 S12 ( .a(abs_gx), .b(abs_gy), .ans_out({
        SYNOPSYS_UNCONNECTED_1, SYNOPSYS_UNCONNECTED_2, sum}), .subtract(1'b0)
         );
  mux_8b M2 ( .in0(sum[7:0]), .in1({1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 
        1'b1}), .sel(sum[8]), .out(out) );
  BUF_X32_slow U3 ( .A(gx_temp[6]), .Z(n1) );
  BUF_X32_slow U4 ( .A(gy_temp[7]), .Z(n2) );
  BUF_X32_slow U5 ( .A(gy_temp[2]), .Z(n3) );
  BUF_X32_slow U6 ( .A(gy_temp[5]), .Z(n4) );
  BUF_X32_slow U7 ( .A(gy_temp[6]), .Z(n5) );
  BUF_X32_slow U8 ( .A(gx_temp[5]), .Z(n6) );
  BUF_X32_slow U9 ( .A(gx_temp[8]), .Z(n7) );
  BUF_X32_slow U10 ( .A(gy_temp[4]), .Z(n8) );
  BUF_X32_slow U11 ( .A(gx_temp[7]), .Z(n9) );
  BUF_X32_slow U12 ( .A(gy_temp[3]), .Z(n10) );
  BUF_X32_slow U13 ( .A(gy_temp[8]), .Z(n11) );
endmodule



module top (
	input wire r_a,
	output wire o_out
);

//	reg r_a = 1'b0;
	reg r_b = 1'b1;

	
	and2 andgate(
		.i_a(r_a),
		.i_b(r_b),
		.o_c(o_out)
	);

endmodule

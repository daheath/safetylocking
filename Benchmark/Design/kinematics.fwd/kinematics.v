// Working: Forward Kinematics
//			x = x_1 + x_2; 	y = y_1 + y_2;
//			x_1 = 11*cos(theta1) ; x_2 = 12*cos(theta_sum)
//			y_1 = 11*sin(theta1) ; y_2 = 12*sin(theta_sum)
//
//
//
//
//





`timescale 1ns/1ps

`define BIT_WIDTH 32
`define FRACTIONS 15
module kinematics(theta1,theta2,x,y, rst, clk);
	input [`BIT_WIDTH-1:0] theta1,theta2;
	input rst, clk;
	output [`BIT_WIDTH-1:0] x,y;
	
	wire [`BIT_WIDTH-1:0] theta_sum;
	reg [`BIT_WIDTH-1:0] x_1, x_2, y_1, y_2, cos_1, cos_2, sin_1, sin_2;
	wire [`BIT_WIDTH-1:0] x_1_reg, x_2_reg, y_1_reg, y_2_reg, cos_1_reg, cos_2_reg, sin_1_reg, sin_2_reg;
	wire overflow_flag1;
	wire overflow_flag2;
	wire overflow_flag3;
	wire overflow_flag4;
	reg [`BIT_WIDTH-1:0] const_1, const_2;
	//assign const_1 = `BIT_WIDTH'b00000000000001011000000000000000;
	//assign const_2 = `BIT_WIDTH'b00000000000001100000000000000000;

	//always@(theta1 or theta2) begin
		qadd #(`FRACTIONS,`BIT_WIDTH) theta_adder(.a(theta1), .b(theta2), .c(theta_sum));
		cos_lut U0 (theta_sum,cos_2_reg);
		sin_lut U1 (theta_sum,sin_2_reg);
		cos_lut U2 (theta1,cos_1_reg);
		sin_lut U3 (theta1,sin_1_reg);
/*
        assign x_1_reg = cos_1 * `BIT_WIDTH'b00000000000001011000000000000000;
        assign x_2_reg = cos_2 * `BIT_WIDTH'b00000000000001100000000000000000;
        assign y_1_reg = sin_1 * 2;
        assign y_2_reg = sin_2 * 2;
*/

		qmult #(`FRACTIONS,`BIT_WIDTH) x1_multiplier(.i_multiplicand(`BIT_WIDTH'b00000000000001011000000000000000), .i_multiplier(cos_1), .o_result(x_1_reg), .ovr(overflow_flag1), .clk(clk), .rst(rst)); 
		qmult #(`FRACTIONS,`BIT_WIDTH) x2_multiplier(.i_multiplicand(`BIT_WIDTH'b00000000000001100000000000000000), .i_multiplier(cos_2), .o_result(x_2_reg), .ovr(overflow_flag2), .clk(clk), .rst(rst)); 
		always@(posedge clk) begin
				cos_1 <= cos_1_reg; 
				cos_2 <= cos_2_reg;
				sin_1 <= sin_1_reg;
				sin_2 <= sin_2_reg;
				x_1 <= x_1_reg; 
				x_2 <= x_2_reg;
				y_1 <= y_1_reg;
				y_2 <= y_2_reg;
		end
		qadd #(`FRACTIONS,`BIT_WIDTH) x_adder(.a(x_1), .b(x_2), .c(x));
		qmult #(`FRACTIONS,`BIT_WIDTH) y1_multiplier(.i_multiplicand(`BIT_WIDTH'b00000000000001011000000000000000), .i_multiplier(sin_1), .o_result(y_1_reg), .ovr(overflow_flag3), .clk(clk), .rst(rst)); 
		qmult #(`FRACTIONS,`BIT_WIDTH) y2_multiplier(.i_multiplicand(`BIT_WIDTH'b00000000000001100000000000000000), .i_multiplier(sin_2), .o_result(y_2_reg), .ovr(overflow_flag4), .clk(clk), .rst(rst)); 
		qadd #(`FRACTIONS,`BIT_WIDTH) y_adder(.a(y_1), .b(y_2), .c(y));
	//end

endmodule

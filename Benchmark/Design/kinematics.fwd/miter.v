
/*
	Target module: Kinematics

	This module takes two cycles to give an output.
	This module is pipelined.
	Which means, only theta1[-2] and theta2[-2] should affect the output x and y.
	
	Policy:

		level {L, H};
		order {L<H};
		set theta1 as H;
		set theta2 as H;
		set x as L;
		set y as L;

		allow theta1[-2] -> x;
		allow theta1[-2] -> y;
		allow theta2[-2] -> x;
		allow theta2[-2] -> y;

*/

module miter #(
	parameter	INPUT_WIDTH		=	32,
	parameter	OUTPUT_WIDTH	=	32,
	parameter	RESET_VALUE		=	0
)
(
	input	wire							i_clk,
	input	wire							i_rst,
	input	wire	[INPUT_WIDTH-1	:	0]	i_theta1_0,	
	input	wire	[INPUT_WIDTH-1	:	0]	i_theta1_1,	
	input	wire	[INPUT_WIDTH-1	:	0]	i_theta2_0,	
	input	wire	[INPUT_WIDTH-1	:	0]	i_theta2_1,	
	output	wire							o_property
);

	localparam	TARGET_INDEX	=	2;

	wire	[OUTPUT_WIDTH-1	:	0]	x0;
	wire	[OUTPUT_WIDTH-1	:	0]	x1;
	wire	[OUTPUT_WIDTH-1	:	0]	y0;
	wire	[OUTPUT_WIDTH-1	:	0]	y1;

	//Index: 0~1 machine number (duplicated test module)
	//		 1~5 To store the values over time
	//Register index starts from one in order to make the index exactly same as the time index
	
	reg		[INPUT_WIDTH-1	:	0]	ir_theta1_0		[1:4];
	reg		[INPUT_WIDTH-1	:	0]	ir_theta1_1		[1:4];
	reg		[INPUT_WIDTH-1	:	0]	ir_theta2_0		[1:4];
	reg		[INPUT_WIDTH-1	:	0]	ir_theta2_1		[1:4];

	reg p1=1;
	reg p2=1;

	/* Property Logics */

	/*	Property logic1 represents

			allow theta1[-2] -> x 
			allow theta1[-2] -> y

		(Otherwise, prohibit. It is stated using the level statements)
	*/

	always @(*)
	begin: PROPERTY_LOGIC_1	//theta1
		if((ir_theta2_0[TARGET_INDEX] == ir_theta2_1[TARGET_INDEX])) begin
			if(ir_theta1_0[TARGET_INDEX] == ir_theta1_1[TARGET_INDEX]) begin
				p1 = ((x0 == x1) && (y0 == y1))? 1:0;	
			end
			else begin									//For the different theta1[-2] values
				p1 = 1;									//The results can be anything
			end
		end
		else begin
			p1 = 1;
		end
	end

	always @(*)
	begin: PROPERTY_LOGIC_2	//theta2
		if((ir_theta1_0[TARGET_INDEX] == ir_theta1_1[TARGET_INDEX])) begin
			if(ir_theta2_0[TARGET_INDEX] == ir_theta2_1[TARGET_INDEX]) begin
				p2 = ((x0 == x1) && (y0 == y1))? 1:0;	
			end
			else begin									//For the different theta2[-2] values
				p2 = 1;									//The results can be anything
			end
		end
		else begin
			p2 = 1;
		end
	end

	assign	o_property = ~(p1 && p2);

	/* Input storing logic */
	// Implemented using shift registers

	always@(posedge i_clk)
	begin: INPUT_THETA1
		if(i_rst) begin
			ir_theta1_0[1] <= RESET_VALUE;
			ir_theta1_0[2] <= RESET_VALUE;
			ir_theta1_0[3] <= RESET_VALUE;
			ir_theta1_0[4] <= RESET_VALUE;

			ir_theta1_1[1] <= RESET_VALUE;
			ir_theta1_1[2] <= RESET_VALUE;
			ir_theta1_1[4] <= RESET_VALUE;
		end
		else begin
			ir_theta1_0[1] <= i_theta1_0;
			ir_theta1_0[2] <= ir_theta1_0[1];
			ir_theta1_0[3] <= ir_theta1_0[2];
			ir_theta1_0[4] <= ir_theta1_0[3];

			ir_theta1_1[1] <= i_theta1_1;
			ir_theta1_1[2] <= ir_theta1_1[1];
			ir_theta1_1[3] <= ir_theta1_1[2];
			ir_theta1_1[4] <= ir_theta1_1[3];
		end
	end

	
	always@(posedge i_clk)
	begin: INPUT_THETA2
		if(i_rst) begin
			ir_theta2_0[1] <= RESET_VALUE;
			ir_theta2_0[2] <= RESET_VALUE;
			ir_theta2_0[3] <= RESET_VALUE;
			ir_theta2_0[4] <= RESET_VALUE;

			ir_theta2_1[1] <= RESET_VALUE;
			ir_theta2_1[2] <= RESET_VALUE;
			ir_theta2_1[3] <= RESET_VALUE;
			ir_theta2_1[4] <= RESET_VALUE;
		end
		else begin
			ir_theta2_0[1] <= i_theta2_0;
			ir_theta2_0[2] <= ir_theta2_0[1];
			ir_theta2_0[3] <= ir_theta2_0[2];
			ir_theta2_0[4] <= ir_theta2_0[3];

			ir_theta2_1[1] <= i_theta2_1;
			ir_theta2_1[2] <= ir_theta2_1[1];
			ir_theta2_1[3] <= ir_theta2_1[2];
			ir_theta2_1[4] <= ir_theta2_1[3];
		end
	end



	/* Module under verification */
	kinematics m0 (
		.theta1		(i_theta1_0	),
		.theta2		(i_theta2_0	),
		.x			(x0			),
		.y			(y0			),
		.clk		(i_clk		),
		.rst		(i_rst		)
	);

	kinematics m1 (
		.theta1		(i_theta1_1	),
		.theta2		(i_theta2_1	),
		.x			(x1			),
		.y			(y1			),
		.clk		(i_clk		),
		.rst		(i_rst		)
	);
endmodule


`timescale 1ns/1ps

`define BIT_WIDTH 32
`define FRACTIONS 15

module kinematics_test;
	reg	[`BIT_WIDTH-1:0] theta1,theta2;
	wire	[`BIT_WIDTH-1:0] x, y;

	reg clk;
	reg rst;

	always #1 clk = ~clk;

	initial begin
		clk = 0;
		$dumpfile("TB.vcd");
		$dumpvars(0,kinematics_test);
		#1000
		$finish;
	end

	initial begin
		$monitor("%b,%b,%b,%b",theta1,theta2,X1.theta_sum,X1.sin_2);
		//$monitor("%b,%b,%b,%b",x_1,x_2,y_1,y_2);

		theta1= 0;								 // 0*
		theta2= 0; // 90*
		#2 rst = 1;
		#2 rst = 0;

		#9
		theta1=`BIT_WIDTH'b0;								 // 0*
		theta2=`BIT_WIDTH'b00000000001011010000000000000000; // 90*

		#2
		theta1=`BIT_WIDTH'b0;								 // 0*
		theta2=`BIT_WIDTH'b00000000001011010000000000010000; // ...

		#2
		theta1=`BIT_WIDTH'b0;								 // 0*
		theta2=`BIT_WIDTH'b00000000001011010000000000000000; // 90*

		#2
		theta1=`BIT_WIDTH'b0;								 // 0*
		theta2=`BIT_WIDTH'b00000000001011010000000000010000; // ...


		#2
		theta1=`BIT_WIDTH'b00000000000101101000000000000000; //45*
		theta2=`BIT_WIDTH'b00000000001011010000000000010000; // ...

		
		#2
		theta1=`BIT_WIDTH'b00000000000101101000000010000000; //??
		theta2=`BIT_WIDTH'b00000000001011010000000000010000; // ...

		#2
		theta1=`BIT_WIDTH'b00000000000101101000000000000000; //45*
		theta2=`BIT_WIDTH'b00000000001011010000000000010000; // ...

		#2
		theta1=`BIT_WIDTH'b00000000000101101000000010000000; //??
		theta2=`BIT_WIDTH'b00000000001011010000000000010000; // ...

		#2
		theta1=`BIT_WIDTH'b00000000000101101000000000000000; //45*
		theta2=`BIT_WIDTH'b00000000001011010000000000010000; // ...

		#2
		theta1=0;
		theta2=0;

		
		#10
		theta1=`BIT_WIDTH'b0;								 // 0*
		theta2=`BIT_WIDTH'b00000000001011010000000000000000; // 90*


		/*
		#2
		theta1=`BIT_WIDTH'b0;							 // 0*
		theta2=`BIT_WIDTH'b00000000001011010000010000000000; // ??

		#2

		theta1=`BIT_WIDTH'b0;								 // 0*
		theta2=`BIT_WIDTH'b00000000001011010000000000000000; // 90*

		#2

		theta1=`BIT_WIDTH'b0;							 // ??
		theta2=`BIT_WIDTH'b01000000001011010000000000000000; // 90*

		#2;
		theta1=`BIT_WIDTH'b00000000001011010000000000000000; // 90*
		theta2=`BIT_WIDTH'b0;								 // 0*
		
		#2 ;
		theta1=`BIT_WIDTH'b00000000000101101000000000000000; //45*
		theta2=`BIT_WIDTH'b00000000000101101000000000000000; //45*
		
		#2 ;
		theta1=`BIT_WIDTH'b0; // 0*
		theta2=`BIT_WIDTH'b0; // 0*

		#2;
		theta1=`BIT_WIDTH'b0;								 // 0*
		theta2=`BIT_WIDTH'b10000000001011010000000000000000; // -90*

		#2 ;
		theta1=`BIT_WIDTH'b10000000001011010000000000000000; // -90*
		theta2=`BIT_WIDTH'b0;								 // 0*
		
		#2 ;
		theta1=`BIT_WIDTH'b10000000000101101000000000000000; // -45*
		theta2=`BIT_WIDTH'b10000000000101101000000000000000; // -45*


		#2 ;
		theta1=`BIT_WIDTH'b10000000000100000000000000000000; // ??
		theta2=`BIT_WIDTH'b10000000000101101000000000000000; // -45*

		#2 ;
		theta1=`BIT_WIDTH'b10000000000110000000000000000000; // ??
		theta2=`BIT_WIDTH'b10000000000101101000000000000000; // -45*

		#2 ;
		theta1=`BIT_WIDTH'b10000000000110000000000000000000; // ??
		theta2=`BIT_WIDTH'b10000000000001101000000000000000; // ??
*/

		#100
		$finish;

	end

	kinematics X1 (theta1,theta2,x,y,rst,clk);
endmodule

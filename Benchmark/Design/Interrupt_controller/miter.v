/*
	Target module: Interrupt controller
	
	Policy:

		if(we_i ==0)
          prohibit irq[1] -> int_o


	       
*/

module miter #(
    parameter   RESET_VALUE     =   0,
	parameter	TEXT_WIDTH		=	128,
	parameter	KEY_WIDTH		=	128
)
(
	input	wire							clk_i,
    input   wire                            rst_i,

	input	wire							cyc_1,
	input	wire							cyc_2,
	
	input	wire							stb_1,
	input	wire							stb_2,

	input	wire	[2				:	1]	adr_i_1,
	input	wire	[2				:	1]	adr_i_2,

	input	wire							we_i_1,
	input	wire							we_i_2,

	input	wire	[7				:	0]	dat_i_1,
	input	wire	[7				:	0]	dat_i_2,

	input	wire	[8				:	1]	irq_1,
	input	wire	[8				:	1]	irq_2,

	output	wire							o_property
);
	localparam	MAX_TIME_INDEX = 2;
	localparam  TARGET_TIME_INDEX = 1;


    /* Output Ports */
	wire [7				:	0]	dat_o_1;
	wire [7				:	0]	dat_o_2;

	wire						ack_o_1;
	wire						ack_o_2;

	wire						int_o_1;
	wire						int_o_2;

    /* Input Value Registers */
	reg								ir_cyc_1		[1	:	MAX_TIME_INDEX];
	reg								ir_cyc_2		[1	:	MAX_TIME_INDEX];

	reg								ir_stb_1		[1	:	MAX_TIME_INDEX];
	reg								ir_stb_2		[1	:	MAX_TIME_INDEX];

    reg     [2				:   0]	ir_adr_i_1		[1  :   MAX_TIME_INDEX];
    reg     [2				:   0] 	ir_adr_i_2		[1  :   MAX_TIME_INDEX];

	reg								ir_we_i_1		[1	:	MAX_TIME_INDEX];
	reg								ir_we_i_2		[1	:	MAX_TIME_INDEX];

	reg		[7				:	0]	ir_dat_i_1		[1	:	MAX_TIME_INDEX];
	reg		[7				:	0]	ir_dat_i_2		[1	:	MAX_TIME_INDEX];

	reg		[8				:	1]	ir_irq_1		[1	:	MAX_TIME_INDEX];
	reg		[8				:	1]	ir_irq_2		[1	:	MAX_TIME_INDEX];


	/* Property Logic */

	reg p1 = 1;

	always @(*)
	begin: PROPERTY
		if(ir_cyc_1[TARGET_TIME_INDEX] == ir_cyc_2[TARGET_TIME_INDEX]
			&& ir_stb_1[TARGET_TIME_INDEX] == ir_stb_2[TARGET_TIME_INDEX]
			&& ir_adr_i_1[TARGET_TIME_INDEX] == ir_adr_i_2[TARGET_TIME_INDEX]
			&& ir_dat_i_1[TARGET_TIME_INDEX] == ir_dat_i_2[TARGET_TIME_INDEX]
			&& ir_we_i_1[TARGET_TIME_INDEX] == 0
			&& ir_irq_1[TARGET_TIME_INDEX] != ir_irq_2[TARGET_TIME_INDEX]
			)
		begin
			p1 = (int_o_1 == int_o_2)? 1:0;
		end
		else begin
			p1 = 1;
		end

	end

	assign o_property = ~p1;

    /* Input Recording Registers */
    genvar i;
	generate
	for(i=1; i<MAX_TIME_INDEX+1; i=i+1) begin

		always@(posedge clk_i)
		begin: IR_CYC
			if(rst_i) begin
				ir_cyc_1[i] <= RESET_VALUE;
				ir_cyc_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_cyc_1[i] <= cyc_1;
					ir_cyc_2[i] <= cyc_2;
				end
				else begin
					ir_cyc_1[i] <= ir_cyc_1[i-1];
					ir_cyc_2[i] <= ir_cyc_2[i-1];
				end
			end
		end

		always@(posedge clk_i)
		begin: IR_STB
			if(rst_i) begin
				ir_stb_1[i] <= RESET_VALUE;
				ir_stb_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_stb_1[i] <= stb_1;
					ir_stb_2[i] <= stb_2;
				end
				else begin
					ir_stb_1[i] <= ir_stb_1[i-1];
					ir_stb_2[i] <= ir_stb_2[i-1];
				end
			end
		end

		always@(posedge clk_i)
		begin: IR_WE_I
			if(rst_i) begin
				ir_we_i_1[i] <= RESET_VALUE;
				ir_we_i_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_we_i_1[i] <= we_i_1;
					ir_we_i_2[i] <= we_i_2;
				end
				else begin
					ir_we_i_1[i] <= ir_we_i_1[i-1];
					ir_we_i_2[i] <= ir_we_i_2[i-1];
				end
			end
		end


		always@(posedge clk_i)
		begin: IR_ADR_I
			if(rst_i) begin
				ir_adr_i_1[i] <= RESET_VALUE;
				ir_adr_i_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_adr_i_1[i] <= adr_i_1;
					ir_adr_i_2[i] <= adr_i_2;
				end
				else begin
					ir_adr_i_1[i] <= ir_adr_i_1[i-1];
					ir_adr_i_2[i] <= ir_adr_i_2[i-1];
				end
			end
		end

		always@(posedge clk_i)
		begin: IR_DAT_I
			if(rst_i) begin
				ir_dat_i_1[i] <= RESET_VALUE;
				ir_dat_i_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_dat_i_1[i] <= dat_i_1;
					ir_dat_i_2[i] <= dat_i_2;
				end
				else begin
					ir_dat_i_1[i] <= ir_dat_i_1[i-1];
					ir_dat_i_2[i] <= ir_dat_i_2[i-1];
				end
			end
		end

		always@(posedge clk_i)
		begin: IR_IRQ
			if(rst_i) begin
				ir_irq_1[i] <= RESET_VALUE;
				ir_irq_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_irq_1[i] <= irq_1;
					ir_irq_2[i] <= irq_2;
				end
				else begin
					ir_irq_1[i] <= ir_irq_1[i-1];
					ir_irq_2[i] <= ir_irq_2[i-1];
				end
			end
		end

	end
	endgenerate


	simple_pic m1 (
		.clk_i		(clk_i		),
		.rst_i		(rst_i		),
		.cyc_i		(cyc_i_1	),
		.stb_i		(stb_i_1	),
		.adr_i		(adr_i_1	),
		.we_i		(we_i_1		),
		.dat_i		(dat_i_1	),
		.dat_o		(dat_o_1	),
		.ack_o		(ack_o_1	),
		.int_o		(int_o_1	)
	);

   	simple_pic m2 (
		.clk_i		(clk_i		),
		.rst_i		(rst_i		),
		.cyc_i		(cyc_i_2	),
		.stb_i		(stb_i_2	),
		.adr_i		(adr_i_2	),
		.we_i		(we_i_2		),
		.dat_i		(dat_i_2	),
		.dat_o		(dat_o_2	),
		.ack_o		(ack_o_2	),
		.int_o		(int_o_2	)
	);


endmodule

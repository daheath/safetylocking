/* Conditional Adder */
/* 
  1. Behavior
	1) If the password matches, accumulate the value to the register r_add_data
	2) Updated value appears one cycle later 
	  -> Need to specify timing condition inside the policy
  2. 
 */


module cond_add
# (
	parameter	RESET_VALUE	=	0,
	parameter	DATA_WIDTH	=	32,
	parameter	PASSWORD	=	1,
)
(
	input	wire	i_clk,
	input	wire	i_rst,
	input	wire	[DATA_WIDTH-1	:	0]	i_sensitive,
	input	wire	[DATA_WIDTH-1	:	0]	i_password,
	output	wire	[DATA_WIDTH-1	:	0]	o_output
);
	
	reg		[DATA_WIDTH-1	:	0]	r_add_data;
	assign o_output = r_add_data;

	always @(posedge i_clk) 
	begin: R_ADD_DATA
		if(i_rst) begin
			r_add_data <= RESET_VALUE;
		end
		else begin
			if(i_password == PASSWORD) begin	//Correct version
//			if(i_password != PASSWORD) begin	//Incorrect version
				r_add_data <= r_add_data + i_sensitive;
			end
			else begin
				r_add_data <= r_add_data;
			end
		end

	end

endmodule


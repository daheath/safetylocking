
/*
	Target module: Gaussian Noise Generator
	
	Policy:

		if(ce == 0)
			prohibit i_clk -> data_out

	New policy:
		default level H
		prohibit ce -> data_out

*/

module miter #(
	parameter	OUTPUT_WIDTH	=	16,
	parameter	RESET_VALUE		=	0
)
(
	input	wire							i_clk,
	input	wire							i_rst,
	input	wire							i_ce,
	input	wire							i_ce_0,
	input	wire							i_ce_1,
	output	wire							o_property
);


	wire [OUTPUT_WIDTH-1:	0]	o_data_out0;
	wire [OUTPUT_WIDTH-1:	0]	o_data_out1;

	wire						o_valid_out0;
	wire						o_valid_out1;



	reg		diff_flag;
	reg		ir_i_ce_0;
	reg		ir_i_ce_1;

	reg p1=1;

	/* Property Logics */

	always@(posedge i_clk)
	begin: IR_I_CE0
		if(i_rst) begin
			ir_i_ce_0 <= 0;
		end
		else begin
			ir_i_ce_0 <= i_ce_0;
		end
	end

	always@(posedge i_clk)
	begin: IR_I_CE1
		if(i_rst) begin
			ir_i_ce_1 <= 0;
		end
		else begin
			ir_i_ce_1 <= i_ce_1;
		end
	end

	always@(posedge i_clk)
	begin: DIFF_FLAG
		if(i_rst) begin
			diff_flag <= 0;
		end
		else begin
			diff_flag <= (diff_flag == 1)? 1 : (i_ce_0 != i_ce_1);
		end
	end

	reg p1=1;

	always @(*)
	begin: PROPERTY_LOGIC	//CE
		if(ir_ce_0 == ir_ce_1) begin
			if(diff_flag == 1) begin
				p1 = (o_data_out0 == o_data_out1);
			end
			else begin
				p1 = 1;
			end
		end
		else begin
			p1 = 1;
		end
	end

	assign	o_property = ~p1;

	gng m0 (
		.clk		(i_clk			),
		.rstn		(i_rst			),
		.ce			(i_ce_0			),
		.valid_out	(o_valid_out0	),
		.data_out	(o_data_out0	)
	);

	gng m1 (
		.clk		(i_clk			),
		.rstn		(i_rst			),
		.ce			(i_ce_1			),
		.valid_out	(o_valid_out1	),
		.data_out	(o_data_out1	)
	);
		
endmodule

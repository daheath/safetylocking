/* Test circuit with module hierachy and delayed execution */

/* Policy description

	//Version 1
	module add_mul;
		if(i_password[-3] != PASSWORD)
			prohibit i_sensitive[-3] -> o_output;
	endmodule
	
	=> MAX_TIME_INDEX = 3
*/

//This is a wrapper for the property check
module miter # (
	parameter	RESET_VALUE		= 0,
	parameter	PASSWORD		= 1231321,
	parameter	DATA_WIDTH		= 32,
	parameter	SENSITIVE_DATA	= 303242
)
(
	/*	IO ports				*/
	input	wire							i_clk,
	input	wire							i_rst,
	input	wire	[DATA_WIDTH-1	:	0]	i_normal,
	input	wire	[DATA_WIDTH-1	:	0]	i_sensitive,
	input	wire	[DATA_WIDTH-1	:	0]	i_password,
	output	wire							o_property
);

	localparam	MAX_TIME_INDEX	=	8;

	/*	Registers				*/
	reg		[DATA_WIDTH-1	:	0]	r_password			[1:MAX_TIME_INDEX];
	reg		[DATA_WIDTH-1	:	0]	r_o_output			[1:MAX_TIME_INDEX];

	reg		[DATA_WIDTH-1	:	0]	r_normal			[1:MAX_TIME_INDEX];
	reg		[DATA_WIDTH-1	:	0]	r_sensitive			[1:MAX_TIME_INDEX];

//	reg		[DATA_WIDTH-1	:	0]	r_clk_counter;

	/*	Wires					*/
	wire	[DATA_WIDTH-1	:	0]	w_o_output;
	wire							w_valid;
	wire							w_ready;

	/*	Temporary value					*/
	reg		tmp;


	always @(*)
	begin: PROPERTY_COMBINATIONAL
		if(r_password[4] != PASSWORD) begin
			if(r_normal[3] == r_normal[1]) begin
				if(r_sensitive[3] != r_sensitive[1]) begin
					tmp = (r_o_output[2] == w_o_output);
				end
				else begin
					tmp = 1;
				end
			end
			else begin
				tmp = 1;
			end
		end
		else begin
			tmp = 1;
		end
	end

	assign o_property = ~tmp;

	//Save w_o_output[-4]
	always @(posedge i_clk)
	begin: R_O_OUTPUT
		if(i_rst) begin
			r_o_output[1] <= RESET_VALUE;
			r_o_output[2] <= RESET_VALUE;
			r_o_output[3] <= RESET_VALUE;
			r_o_output[4] <= RESET_VALUE;
			r_o_output[5] <= RESET_VALUE;
			r_o_output[6] <= RESET_VALUE;
			r_o_output[7] <= RESET_VALUE;
			r_o_output[8] <= RESET_VALUE;
		end
		else begin
			r_o_output[1] <= w_o_output;
			r_o_output[2] <= r_o_output[1];
			r_o_output[3] <= r_o_output[2];
			r_o_output[4] <= r_o_output[3];
			r_o_output[5] <= r_o_output[4];
			r_o_output[6] <= r_o_output[5];
			r_o_output[7] <= r_o_output[6];
			r_o_output[8] <= r_o_output[7];
		end
	end

	//Save  i_password[-4]
	always @(posedge i_clk)
	begin: R_PASSWORD
		if(i_rst) begin
			r_password[1] <= RESET_VALUE;
			r_password[2] <= RESET_VALUE;
			r_password[3] <= RESET_VALUE;
			r_password[4] <= RESET_VALUE;
			r_password[5] <= RESET_VALUE;
			r_password[6] <= RESET_VALUE;
			r_password[7] <= RESET_VALUE;
			r_password[8] <= RESET_VALUE;
		end
		else begin
			if(w_ready) begin
				r_password[1] <= i_password;
				r_password[2] <= r_password[1];
				r_password[3] <= r_password[2];
				r_password[4] <= r_password[3];
				r_password[5] <= r_password[4];
				r_password[6] <= r_password[5];
				r_password[7] <= r_password[6];
				r_password[8] <= r_password[7];
			end
		end
	end

	always @(posedge i_clk)
	begin: R_NORMAL
		if(i_rst) begin
			r_normal[1]	<= RESET_VALUE;
			r_normal[2] <= RESET_VALUE;
			r_normal[3] <= RESET_VALUE;
			r_normal[4] <= RESET_VALUE;
			r_normal[5] <= RESET_VALUE;
			r_normal[6] <= RESET_VALUE;
			r_normal[7] <= RESET_VALUE;
			r_normal[8] <= RESET_VALUE;
		end
		else begin
				r_normal[1] <= i_normal;
				r_normal[2] <= r_normal[1];
				r_normal[3] <= r_normal[2];
				r_normal[4] <= r_normal[3];
				r_normal[5] <= r_normal[4];
				r_normal[6] <= r_normal[5];
				r_normal[7] <= r_normal[6];
				r_normal[8] <= r_normal[7];
		end
	end

	always @(posedge i_clk)
	begin: R_SENSITIVE
		if(i_rst) begin
			r_sensitive[1] <= RESET_VALUE;
			r_sensitive[2] <= RESET_VALUE;
			r_sensitive[3] <= RESET_VALUE;
			r_sensitive[4] <= RESET_VALUE;
			r_sensitive[5] <= RESET_VALUE;
			r_sensitive[6] <= RESET_VALUE;
			r_sensitive[7] <= RESET_VALUE;
			r_sensitive[8] <= RESET_VALUE;
		end
		else begin
			r_sensitive[1] <= i_sensitive;
			r_sensitive[2] <= r_sensitive[1];
			r_sensitive[3] <= r_sensitive[2];
			r_sensitive[4] <= r_sensitive[3];
			r_sensitive[5] <= r_sensitive[4];
			r_sensitive[6] <= r_sensitive[5];
			r_sensitive[7] <= r_sensitive[6];
			r_sensitive[8] <= r_sensitive[7];
		end
	end

	//Design under verification
	add_mul# (
		.RESET_VALUE	(RESET_VALUE),
		.DATA_WIDTH		(DATA_WIDTH),
		.PASSWORD		(PASSWORD)
	) password_addmul
	(
		.i_clk			(i_clk),
		.i_rst			(i_rst),
		.i_normal_data	(i_normal),
		.i_sensitive	(i_sensitive),
		.i_password		(i_password),
		.o_valid		(w_valid),
		.o_output		(w_o_output)
	);

endmodule

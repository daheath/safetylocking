/* Conditional Adder */
/* 
  1. Delay characteristics
	1) cond_add module requires one cycle to show the result
	2) mul module requires three cycles to show the result
 */

module add_mul
# (
	parameter	RESET_VALUE	=	0,
	parameter	DATA_WIDTH	=	32,
	parameter	PASSWORD	=	1,
	parameter	MUL_DELAY	=	3
)
(
	input	wire							i_clk,
	input	wire							i_rst,
	input	wire	[DATA_WIDTH-1	:	0]	i_sensitive,
	input	wire	[DATA_WIDTH-1	:	0]	i_normal_data,
	input	wire	[DATA_WIDTH-1	:	0]	i_password,
	output	wire							o_valid,
	output	wire	[DATA_WIDTH-1	:	0]	o_output
);

	wire	[DATA_WIDTH-1	:	0]	w_add_res;
	wire	[DATA_WIDTH-1	:	0]	w_mul_res;
	wire							w_mul_valid;
	wire							w_mul_req;
	wire							w_valid;
	
	wire	[DATA_WIDTH-1	:	0]	w_mul_input1;
	wire	[DATA_WIDTH-1	:	0]	w_mul_input2;

	reg		[DATA_WIDTH-1	:	0]	r_add_mul_res;
	reg								r_valid;

	assign	w_mul_req		=	1;
	assign	o_valid			=	r_valid;
	assign	w_mul_input1	=	w_add_res;
	assign	w_mul_input2	=	w_add_res;

	always @(posedge i_clk)
	begin: R_ADD_MUL_RES
		if(i_rst) begin
			r_add_mul_res <= RESET_VALUE;
		end
		else begin
			if(w_valid) begin
				r_add_mul_res <= w_mul_res;
			end
		end
	end

	always @(posedge i_clk)
	begin: R_VALID
		if(i_rst) begin
			r_valid <= 0;
		end
		else begin
			r_valid <= w_valid;
		end
	end

	//Delay = 1
	cond_add # (
		.RESET_VALUE	(RESET_VALUE	),
		.DATA_WIDTH		(DATA_WIDTH		),
		.PASSWORD		(PASSWORD		)
	) conditional_adder (
		.i_clk			(i_clk			),
		.i_rst			(i_rst			),
		.i_sensitive	(i_sensitive	),
		.i_normal_data	(i_normal_data	),
		.i_password		(i_password		),
		.o_output		(w_add_res		)
	);

	//Delay = 3
	mul	# (
		.RESET_VALUE	(RESET_VALUE	),
		.DATA_WIDTH		(DATA_WIDTH		),
		.DELAY			(MUL_DELAY		)
	) delayed_square (
		.i_clk			(i_clk			),
		.i_rst			(i_rst			),
		.i_input1		(w_mul_input1	),
		.i_input2		(w_mul_input2	),
		.i_req			(w_mul_req		),
		.o_ack			(w_mul_ack		),
		.o_ready		(w_mul_ready	),
		.o_valid		(w_valid		),
		.o_output		(w_mul_res		)
	);

endmodule


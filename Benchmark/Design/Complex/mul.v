/* Multiplier with delay */
/* 
  1. Behavior
	1) Calculates i_input1 * i_input2
	2) Valid bit goes high only if it spends DELAY cycles
	  -> Need to specify timing condition inside the policy
 */


module mul
# (
	parameter	RESET_VALUE	=	0,
	parameter	DATA_WIDTH	=	32,
	parameter	PASSWORD	=	1,
	parameter	DELAY		=	3
)
(
	input	wire							i_clk,
	input	wire							i_rst,
	input	wire	[DATA_WIDTH-1	:	0]	i_input1,
	input	wire	[DATA_WIDTH-1	:	0]	i_input2,
	input	wire							i_req,
	output	reg								o_ack,
	output	reg								o_ready,
	output	reg								o_valid,
	output	wire	[DATA_WIDTH-1	:	0]	o_output
);

	localparam	ST_BITS	=	2;
	localparam	ST_IDLE	=	0;
	localparam	ST_BUSY	=	1;

	reg		[DATA_WIDTH-1	:	0]	r_delay_count;
	reg		[DATA_WIDTH-1	:	0]	r_mul_data;

	reg		[ST_BITS-1		:	0]	r_state;

	assign o_output = r_mul_data;


	//State transition
	always @(posedge i_clk)
	begin:	R_STATE
		if(i_rst) begin
			r_state <= ST_IDLE;
		end
		else begin
			if(r_state == ST_IDLE) begin
				if( i_req == 1) begin
					r_state <= ST_BUSY;
				end
				else begin
					r_state <= r_state;
				end
			end
			else begin // if (r_state == ST_BUSY)
				if(r_delay_count == DELAY -1) begin
					r_state <= ST_IDLE;
				end
				else begin
					r_state <= r_state;
				end
			end
		end
	end

	always @(posedge i_clk) 
	begin: R_DELAY_COUNT
		if(i_rst) begin
			r_delay_count <= 0;	//Should be always 0. Cannot be parameterized with RESET_VALUE
		end
		else begin
			if(r_state == ST_BUSY) begin
				r_delay_count <= (r_delay_count == DELAY-1)? 0 : r_delay_count + 1;
			end
			else begin
				r_delay_count <= 0;
			end
		end
	end

	always @(posedge i_clk)
	begin:	O_READY
		if(i_rst) begin
			o_ready <= 1;
		end
		else begin
			o_ready <= (r_state == ST_IDLE)? 1 : 0;
		end
	end

	always @(posedge i_clk)
	begin:	O_VALID
		if(i_rst) begin
			o_valid <= 0;
		end
		else begin
			o_valid <= (r_delay_count == DELAY-1)? 1: 0;
		end
	end

	always @(posedge i_clk)
	begin:	O_ACK
		if(i_rst) begin
			o_ack <= 0;
		end
		else begin
			o_ack <= (r_state == ST_IDLE && i_req == 1)? 1 : 0;
		end
	end

	always @(posedge i_clk) 
	begin: R_MUL_DATA
		if(i_rst) begin
			r_mul_data	<=	RESET_VALUE;
		end
		else begin
			if(r_state == ST_IDLE && i_req == 1) begin
				r_mul_data <= i_input1 * i_input2;
			end
			else begin
				r_mul_data <= r_mul_data;
			end
		end
	end

endmodule

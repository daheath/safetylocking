/* Password module */

/* Policy description

	module cond_add;
		if(i_password != PASSWORD)
			prohibit i_sensitive -> o_output;
	endmodule

*/

//This is a wrapper for the property check
module miter # (
	parameter	RESET_VALUE		= 0,
	parameter	PASSWORD		= 1231321,
	parameter	DATA_WIDTH		= 32,
	parameter	SENSITIVE_DATA	= 303242
)
(
	/*	IO ports				*/
	input	wire							i_clk,
	input	wire							i_rst,
	input	wire	[DATA_WIDTH-1	:	0]	i_sensitive,
	input	wire	[DATA_WIDTH-1	:	0]	i_password,
	output	wire							o_property
);

	/*	Registers				*/
	reg		[DATA_WIDTH-1	:	0]	r_password;
	reg		[DATA_WIDTH-1	:	0]	r_outcoming_data;

	/*	Wires					*/
	wire	[DATA_WIDTH-1	:	0]	w_outcoming_data;

	/* Property logic			*/
		//If the password does not match, the output value must be same as the previous value.
	assign o_property = ~(((r_password != PASSWORD) && (r_outcoming_data == w_outcoming_data))
							|| (r_password == PASSWORD));

	//Save  i_password[-1]
	always @(posedge i_clk)
	begin: R_PASSWORD
		if(i_rst) begin
			r_password <= RESET_VALUE;
		end
		else begin
			r_password <= i_password;
		end
	end

	//Save w_outcoming_data[-1]
	always @(posedge i_clk)
	begin: R_OUTCOMING
		if(i_rst) begin
			r_outcoming_data <= RESET_VALUE;
		end
		else begin
			r_outcoming_data <= w_outcoming_data;
		end
	end

	//Design under verification
	password# (
		.RESET_VALUE	(RESET_VALUE),
		.DATA_WIDTH		(DATA_WIDTH),
		.PASSWORD		(PASSWORD)
	) passwd
	(
		.i_clk			(i_clk),
		.i_rst			(i_rst),
		.i_sensitive	(i_sensitive),
		.i_password		(i_password),
		.o_output		(w_outcoming_data)
	);

endmodule

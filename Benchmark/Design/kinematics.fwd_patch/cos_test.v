`timescale 1ns/1ps

module cos_test;
	reg  [31:0] theta;
	wire [31:0] value;
	initial 
	begin
		$monitor("%b,%b",theta, value);
		theta=32'b00000000000000000001000000000000;				     // 0*
		#100;
		theta=32'b00000000010110100000000000000000; // 90*
	end
	
	cos_lut X1 (theta, value);

endmodule
/*
	Target module: Kinematics

	This module takes two cycles to give an output.
	This module is pipelined.
	Which means, only theta1[-2] and theta2[-2] should affect the output x and y.
	
	Policy:

		level {L, H};
		order {L<H};
		set theta1 as H;
		set theta2 as H;
		set x as L;
		set y as L;

		allow theta1[-2] -> x;
		allow theta1[-2] -> y;
		allow theta2[-2] -> x;
		allow theta2[-2] -> y;

*/

module miter #(
	parameter	INPUT_WIDTH		=	32,
	parameter	OUTPUT_WIDTH	=	32,
	parameter	RESET_VALUE		=	0
)
(
	input	wire							i_clk,
	input	wire							i_rst,
	input	wire	[INPUT_WIDTH-1	:	0]	i_theta1_1,
	input	wire	[INPUT_WIDTH-1	:	0]	i_theta1_2,
	input	wire	[INPUT_WIDTH-1	:	0]	i_theta2_1,	
	input	wire	[INPUT_WIDTH-1	:	0]	i_theta2_2,	

	output	wire							o_property
);
	localparam	MAX_TIME_INDEX	=	4;
	localparam	TARGET_INDEX	=	1;

	wire	[OUTPUT_WIDTH-1	:	0]	x1;
	wire	[OUTPUT_WIDTH-1	:	0]	x2;
	wire	[OUTPUT_WIDTH-1	:	0]	y1;
	wire	[OUTPUT_WIDTH-1	:	0]	y2;

	reg		isReset;
	reg		diff_flag;
	
	reg		[INPUT_WIDTH-1	:	0]	ir_theta1_1		[1:MAX_TIME_INDEX];
	reg		[INPUT_WIDTH-1	:	0]	ir_theta1_2		[1:MAX_TIME_INDEX];
	reg		[INPUT_WIDTH-1	:	0]	ir_theta2_1		[1:MAX_TIME_INDEX];
	reg		[INPUT_WIDTH-1	:	0]	ir_theta2_2		[1:MAX_TIME_INDEX];


	reg		[MAX_TIME_INDEX	:	1]	cond_theta1;
	reg		[MAX_TIME_INDEX	:	1]	cond_theta2;



	genvar i;
	generate
	for(i=1; i< MAX_TIME_INDEX+1; i=i+1) begin
		always @(*)
		begin: COND_THETA1
			if(i!=TARGET_INDEX) begin
				cond_theta1[i] = ~((ir_theta1_1[i] == ir_theta1_2[i])
									&& (ir_theta2_1[i] == ir_theta2_2[i]));
			end
			else begin
				cond_theta1[i] = ~(ir_theta1_1[i] != ir_theta1_2[i]
								&& ir_theta2_1[i] == ir_theta2_2[i]
								);
			end
		end

		always @(*)
		begin: COND_THETA2
			if(i!=TARGET_INDEX) begin
				cond_theta2[i] = ~((ir_theta1_1[i] == ir_theta1_2[i])
									&& (ir_theta2_1[i] == ir_theta2_2[i]));
			end
			else begin
				cond_theta2[i] = ~(ir_theta1_1[i] == ir_theta1_2[i]
									&& ir_theta2_1[i] != ir_theta2_2[i]
									);
			end
		end



	end
	endgenerate

	wire property;
	wire property_theta1;
	wire property_theta2;

	assign property_theta1 = (cond_theta1 == 0)? ( x1 == x2 && y1 == y2) : 1;
	assign property_theta2 = (cond_theta2 == 0)? ( x1 == x2 && y1 == y2) : 1;

			
	assign property = property_theta1 & property_theta2;
	assign o_property = ~property;

	/* Input storing logic */
	// Implemented using shift registers

	generate
		for(i=1; i<MAX_TIME_INDEX+1; i=i+1) begin

			always @(posedge i_clk)
			begin: IR_THETA1
				if(i_rst) begin
					ir_theta1_1[i] <= RESET_VALUE;
					ir_theta1_2[i] <= RESET_VALUE;
				end
				else begin
					if(i==1) begin
						ir_theta1_1[i] <= i_theta1_1;
						ir_theta1_2[i] <= i_theta1_2;
					end
					else begin
						ir_theta1_1[i] <= ir_theta1_1[i-1];
						ir_theta1_2[i] <= ir_theta1_2[i-1];
					end
				end
			end	

			always @(posedge i_clk)
			begin: IR_THETA2
				if(i_rst) begin
					ir_theta2_1[i] <= RESET_VALUE;
					ir_theta2_2[i] <= RESET_VALUE;
				end
				else begin
					if(i==1) begin
						ir_theta2_1[i] <= i_theta2_1;
						ir_theta2_2[i] <= i_theta2_2;
					end
					else begin
						ir_theta2_1[i] <= ir_theta2_1[i-1];
						ir_theta2_2[i] <= ir_theta2_2[i-1];
					end
				end
			end	


		end
	endgenerate



	/* Module under verification */
	kinematics m1 (
		.theta1		(i_theta1_1	),
		.theta2		(i_theta2_1	),
		.x			(x1			),
		.y			(y1			),
		.clk		(i_clk		),
		.rst		(i_rst		)
	);

	kinematics m2 (
		.theta1		(i_theta1_2	),
		.theta2		(i_theta2_2	),
		.x			(x2			),
		.y			(y2			),
		.clk		(i_clk		),
		.rst		(i_rst		)
	);
endmodule

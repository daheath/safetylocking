
/*
	Target module: UART
	
	Policy:

		   default level H
		   order {L<H}

		   if(ld[1] == 1)
             allow text_in[1] -> text_out

	       
	Attacking the missing case
*/

module miter #(
    parameter   RESET_VALUE     =   0,
	parameter	TEXT_WIDTH		=	128,
	parameter	KEY_WIDTH		=	128
)
(
	input	wire							clk,
    input   wire                            rst,

	input	wire							ld_1,
	input	wire							ld_2,
	
    input   wire    [KEY_WIDTH-1	:   0]  key_1,
    input   wire    [KEY_WIDTH-1	:   0]  key_2,

    input   wire    [TEXT_WIDTH-1	:   0]  text_in_1,
    input   wire    [TEXT_WIDTH-1	:   0]  text_in_2,

	output	wire							o_property
);
	localparam	MAX_TIME_INDEX = 2;
	localparam  TARGET_TIME_INDEX = 1;

    /* Output Ports */
	wire [TEXT_WIDTH-1	:	0]	text_out_1;
	wire [TEXT_WIDTH-1	:	0]	text_out_2;

	wire	done_1;
	wire	done_2;

    /* Input Value Registers */
	reg								ir_ld_1			[1	:	MAX_TIME_INDEX];
	reg								ir_ld_2			[1	:	MAX_TIME_INDEX];

    reg     [KEY_WIDTH-1   :   0]	ir_key_1		[1  :   MAX_TIME_INDEX];
    reg     [KEY_WIDTH-1   :   0] 	ir_key_2		[1  :   MAX_TIME_INDEX];

    reg     [TEXT_WIDTH-1  :   0] 	ir_text_in_1	[1  :   MAX_TIME_INDEX];
    reg     [TEXT_WIDTH-1  :   0] 	ir_text_in_2	[1  :   MAX_TIME_INDEX];


	/* Property Logic */

	reg p1 = 1;

	always @(*)
	begin: PROPERTY
		if(	ir_ld_1[TARGET_TIME_INDEX] == 0
			&& ir_ld_2[TARGET_TIME_INDEX] == 0
			&& ir_key_1[TARGET_TIME_INDEX] == ir_key_2[TARGET_TIME_INDEX]
			&& ir_text_in_1[TARGET_TIME_INDEX] != ir_text_in_2[TARGET_TIME_INDEX]
		  )
		begin
			p1 = (text_out_1 == text_out_2)? 1:0;
		end
		else begin
			p1 = 1;
		end
	end

	assign o_property = ~p1;

    /* Input Recording Registers */
    genvar i;
	generate
	for(i=1; i<MAX_TIME_INDEX+1; i=i+1) begin

		always@(posedge clk)
		begin: IR_LD
			if(rst) begin
				ir_ld_1[i] <= RESET_VALUE;
				ir_ld_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_ld_1[i] <= ld_1;
					ir_ld_2[i] <= ld_2;
				end
				else begin
					ir_ld_1[i] <= ir_ld_1[i-1];
					ir_ld_2[i] <= ir_ld_2[i-1];
				end
			end
		end

		always@(posedge clk)
		begin: IR_KEY
			if(rst) begin
				ir_key_1[i] <= RESET_VALUE;
				ir_key_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_key_1[i] <= key_1;
					ir_key_2[i] <= key_2;
				end
				else begin
					ir_key_1[i] <= ir_key_1[i-1];
					ir_key_2[i] <= ir_key_2[i-1];
				end
			end
		end

		always@(posedge clk)
		begin: IR_TEXT_IN
			if(rst) begin
				ir_text_in_1[i] <= RESET_VALUE;
				ir_text_in_2[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_text_in_1[i] <= text_in_1;
					ir_text_in_2[i] <= text_in_2;
				end
				else begin
					ir_text_in_1[i] <= ir_text_in_1[i-1];
					ir_text_in_2[i] <= ir_text_in_2[i-1];
				end
			end
		end

	end
	endgenerate

   
	aes_cipher_top m1 (
		.clk		(clk		),
		.rst		(rst		),
		.ld			(ld_1		),
		.key		(key_1		),
		.text_in	(text_in_1	),
		.done		(done_1		),
		.text_out	(text_out_1	)
	);

	aes_cipher_top m2 (
		.clk		(clk		),
		.rst		(rst		),
		.ld			(ld_2		),
		.key		(key_2		),
		.text_in	(text_in_2	),
		.done		(done_2		),
		.text_out	(text_out_2	)
	);

endmodule

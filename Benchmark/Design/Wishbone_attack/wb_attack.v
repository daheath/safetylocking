/////////////////////////////////////////////////////////////////////////////
////                                                                     ////
////           An attack model of Wishbone Flash interface               ////
////                                                                     ////
/////////////////////////////////////////////////////////////////////////////
////                                                                     ////
////  The Flash interface always sends the data from (input) wb_dat_i    ////
////  to (output) flash_dat_o.                                           ////
////  Assumes that we have an incoming data from ethernet and this was   ////
////  not supposed to sent to the flash memory.                          ////
////                                                                     ////
/////////////////////////////////////////////////////////////////////////////

module wb_attack #(
	parameter DATA_WIDTH = 32,
	parameter ADDR_WIDTH = 19,
	parameter FLASH_DATA_WIDTH = 8,
	parameter SEL_WIDTH = 4
)
(
	input	wire								clk_i,
	input	wire								rst_i,

	input	wire	[DATA_WIDTH-1		:	0]	incoming_data,

	input	wire	[ADDR_WIDTH-1		:	0]	wb_adr_i,
	input	wire	[DATA_WIDTH-1		:	0]	wb_dat_i,
	input	wire	[SEL_WIDTH-1		:	0]	wb_sel_i,
	input	wire								wb_we_i,
	input	wire								wb_stb_i,
	input	wire								wb_cyc_i,
	input	wire	[FLASH_DATA_WIDTH-1	:	0]	flash_dat_i,

	output	wire	[DATA_WIDTH-1		:	0]	wb_dat_o,
	output	wire								wb_ack_o,
	output	wire	[ADDR_WIDTH-1		:	0]	flash_adr_o,
	output	wire	[FLASH_DATA_WIDTH-1	:	0]	flash_dat_o,
	output	wire								flash_oe,
	output	wire								flash_ce,
	output	wire								flash_we
);

	wire	[31	:	0]	attack_channel;

	assign attack_channel = wb_we_i? flash_dat_i : incoming_data;
//	assign attack_channel = flash_dat_i;

	wb_flash wb (
		.clk_i			(clk_i			),
		.nrst_i			(~rst_i			),
		.wb_adr_i		(wb_adr_i		),
		.wb_dat_o		(wb_dat_o		),
		.wb_dat_i		(attack_channel	),
		.wb_sel_i		(wb_sel_i		),
		.wb_we_i		(wb_we_i		),
		.wb_stb_i		(wb_stb_i		),
		.wb_cyc_i		(wb_cyc_i		),
		.wb_ack_o		(wb_ack_o		),
		.flash_adr_o	(flash_adr_o	),
		.flash_dat_o	(flash_dat_o	),
		.flash_dat_i	(flash_dat_i	),
		.flash_oe		(flash_oe		),
		.flash_ce		(flash_ce		),
		.flash_we		(flash_we		)
	);

endmodule

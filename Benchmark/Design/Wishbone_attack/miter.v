
/*
	Target module: Wishbone flash controller

	
	Policy:
		level {L,H};
		order {L<H};
		set flash_dat_i as H;
		set wb_dat_o as L;

		if(wb_we_i == 1)
			allow flash_dat_i -> wb_dat_o;

	New Policy:
		prohibit incoming_data -> flash_dat_o

*/

module miter #(
	parameter	RESET_VALUE		=	0
)
(
	input	wire				clk_i,
	input	wire				rst_i,

	input	wire	[31	:	0]	incoming_data_1,
	input	wire	[31	:	0]	incoming_data_2,

	//Inputs for the wb_flash modules
	input	wire	[18	:	0]	wb_adr_i,
	input	wire	[31	:	0]	wb_dat_i,
	input	wire	[3	:	0]	wb_sel_i,
	input	wire				wb_we_i,
	input	wire				wb_stb_i,
	input	wire				wb_cyc_i,
	input	wire	[7	:	0]	flash_dat_i,

	output	wire				o_property

);
	
	/* Output Ports */
	wire	[31	:	0]	wb_dat_o_1, wb_dat_o_2;
	wire				wb_ack_o_1,	wb_ack_o_2;
	wire	[18	:	0]	flash_adr_o_1,	flash_adr_o_2;
	wire	[7	:	0]	flash_dat_o_1,	flash_dat_o_2;
	wire				flash_oe_1; 
	wire				flash_oe_2; 
	wire				flash_ce_1; 
	wire				flash_ce_2; 
	wire				flash_we_1; 
	wire				flash_we_2; 

	reg precond=0;

	/* Property Logic */
	
	//Precondition : If incoming_data_1 and incoming_dat2 were differnt, set the flag
	always @ (posedge clk_i)
	begin: PRE_CONDITION
		if(rst_i) begin
			precond <= 0;
		end
		else begin
			precond <= (precond == 1)? precond : (incoming_data_1 != incoming_data_2);
		end
	end

	assign o_property = ~( (precond && (flash_dat_o_1 == flash_dat_o_2)) || !precond );


	/* Module under verification */
	wb_attack f1 (
		.clk_i			(clk_i			),
		.rst_i			(rst_i			),
		.incoming_data	(incoming_data_1),
		.wb_adr_i		(wb_adr_i		),
		.wb_dat_o		(wb_dat_o_1		),
		.wb_dat_i		(wb_dat_i		),
		.wb_sel_i		(wb_sel_i		),
		.wb_we_i		(wb_we_i		),
		.wb_stb_i		(wb_stb_i		),
		.wb_cyc_i		(wb_cyc_i		),
		.wb_ack_o		(wb_ack_o_1		),
		.flash_adr_o	(flash_adr_o_1	),
		.flash_dat_o	(flash_dat_o_1	),
		.flash_dat_i	(flash_dat_i	),
		.flash_oe		(flash_oe_1		),
		.flash_ce		(flash_ce_1		),
		.flash_we		(flash_we_1		)
	);

	wb_attack f2 (
		.clk_i			(clk_i			),
		.rst_i			(rst_i			),
		.incoming_data	(incoming_data_2),
		.wb_adr_i		(wb_adr_i		),
		.wb_dat_o		(wb_dat_o_2		),
		.wb_dat_i		(wb_dat_i		),
		.wb_sel_i		(wb_sel_i		),
		.wb_we_i		(wb_we_i		),
		.wb_stb_i		(wb_stb_i		),
		.wb_cyc_i		(wb_cyc_i		),
		.wb_ack_o		(wb_ack_o_2		),
		.flash_adr_o	(flash_adr_o_2	),
		.flash_dat_o	(flash_dat_o_2	),
		.flash_dat_i	(flash_dat_i	),
		.flash_oe		(flash_oe_2		),
		.flash_ce		(flash_ce_2		),
		.flash_we		(flash_we_2		)
	);

endmodule

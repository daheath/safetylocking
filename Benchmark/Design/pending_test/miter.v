/*

	Model chekcer availability test

	This benchmark tests if the model checker run efficiently for policies involving large number of evaulation steps.


	Policy:

		set i_data as H;
		set o_data as L;
		order {L<H};

		allow i_data[TARGET_TIME] -> o_data
*/


module miter#(
	parameter	TARGET_TIME	=	90,
	parameter	DATA_WIDTH	=	32
)
(
	input	wire							i_clk,
	input	wire							i_rst,

	input	wire							i_start_eval,

	input 	wire	[DATA_WIDTH-1	:	0]	i_data_1,
	input 	wire	[DATA_WIDTH-1	:	0]	i_data_2,

	output	wire							o_property
);

	/* Module outputs */
	wire	[DATA_WIDTH-1	:	0]	o_data_1;
	wire	[DATA_WIDTH-1	:	0]	o_data_2;

	/* Benchmark control registers */
	reg								eval_started 	= 0;	//If evaluation is started
	reg								pre_cond 		= 0;	
	reg 	[31				:	0]	step_counter;



	always @ (posedge i_clk)
	begin: START_EVAL
		if(i_rst) begin
			eval_started <= 0;
		end
		else begin
			if(i_start_eval == 1) begin
				eval_started <= 1;
			end
		end
	end

	always @ (posedge i_clk)
	begin: PRE_COND
		if(i_rst) begin
			pre_cond <= 0;
		end
		else begin
			if(eval_started) begin
				pre_cond <= (pre_cond == 0)? (i_data_1 != i_data_2) : pre_cond;
			end
		end
	end


	always @ (posedge i_clk)
	begin: STEP_COUNTER
		if(i_rst) begin
			step_counter <= 0;
		end
		else begin
			if(eval_started) begin
				step_counter <= step_counter + 1;
			end
		end
	end
	


	/* Property Logic */

	reg	p1 = 1;

	always @(*)
	begin: PROPERTY_LOGIC
			if(step_counter == TARGET_TIME) begin
				p1 = (o_data_1 == o_data_2);
			end
			else begin
				p1 = 1;
			end
	end

	assign o_property = ~p1;



	dut # (
		.TARGET_TIME	(TARGET_TIME),
		.DATA_WIDTH		(DATA_WIDTH)
	) m1 (
		.i_clk	(i_clk		),
		.i_rst	(i_rst		),
		.i_data	(i_data_1	),
		.o_data	(o_data_1	)
	);

	dut # (
		.TARGET_TIME	(TARGET_TIME),
		.DATA_WIDTH		(DATA_WIDTH)	
	) m2 (
		.i_clk	(i_clk		),
		.i_rst	(i_rst		),
		.i_data	(i_data_2	),
		.o_data	(o_data_2	)
	);

endmodule



module dut#(
	parameter	TARGET_TIME	=	10,
	parameter	DATA_WIDTH	=	32
)
(
	input	wire							i_clk,
	input	wire							i_rst,

	input 	wire	[DATA_WIDTH-1	:	0]	i_data,

	output	reg		[DATA_WIDTH-1	:	0]	o_data
);

	wire							isTime;
	reg								isReset = 0;
	reg		[DATA_WIDTH-1	:	0]	counter;

	reg		[DATA_WIDTH-1	:	0]	past_data;

	assign	isTime = (counter >= TARGET_TIME);

	always @ (posedge i_clk)
	begin: IS_RESET
		if(i_rst) begin
			isReset <= 1;
		end
	end

	always @ (posedge i_clk)
	begin: COUNTER
		if(i_rst) begin
			counter <= 0;
		end
		else if(isReset) begin
			counter <= counter + 1;
		end
	end

	always @ (posedge i_clk)
	begin: PAST_DATA
		if(i_rst) begin
			past_data <= 0;
		end
		else begin
			if(counter == 0) begin
				past_data <= i_data;
			end
		end
	end

	always @ (posedge i_clk)
	begin: O_DATA
		if(i_rst) begin
			o_data <= 0;
		end
		else begin
			if(isTime) begin
				o_data <= past_data;
			end
		end
	end

endmodule

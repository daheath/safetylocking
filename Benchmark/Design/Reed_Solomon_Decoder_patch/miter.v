
/*
	Target module: Reed_Solomon_Decoder
	
	Policy:

       set input_byte as H
	   set Out_byte as L
	   order {L<H}

		if(CE[7] == 1)
			allow input_byte[7] -> Out_byte

*/

module miter #(
	parameter	INPUT_WIDTH		=	8,
	parameter	OUTPUT_WIDTH	=	8,
	parameter	RESET_VALUE		=	0
)
(
	input	wire							i_clk,
	input	wire							i_rst,
	/* CE */
	input	wire							i_CE_0,
	input	wire							i_CE_1,

	/* input_byte */
	input	wire	[INPUT_WIDTH-1	:	0]	i_input_byte_0,
	input	wire	[INPUT_WIDTH-1	:	0]	i_input_byte_1,

	output	wire							o_property
);
	localparam	MAX_TIME_INDEX = 7;
	localparam	TARGET_TIME_INDEX = 7;


	wire [OUTPUT_WIDTH-1:	0]	o_Out_byte_0;
	wire [OUTPUT_WIDTH-1:	0]	o_Out_byte_1;

	wire						o_CEO_0;
	wire						o_CEO_1;

	wire						o_Valid_out_0;
	wire						o_Valid_out_1;

	reg		ir_CE_0[1:MAX_TIME_INDEX];
	reg		ir_CE_1[1:MAX_TIME_INDEX];

	reg	[INPUT_WIDTH-1:	0]	ir_input_byte_0[1:MAX_TIME_INDEX];
	reg	[INPUT_WIDTH-1:	0]	ir_input_byte_1[1:MAX_TIME_INDEX];

//	reg p1=1;

	reg [MAX_TIME_INDEX:1]p1_wide;
	reg p1=1;

	/* Property Logics */
	genvar i;
	generate
	for(i=1; i<MAX_TIME_INDEX+1; i=i+1) 
	begin
		always @(*)
		begin:PROPERTY_LOGIC_1
			if((ir_CE_0[i] == ir_CE_1[i])
				&& (ir_input_byte_0[i] && ir_input_byte_1[i]))
			begin
				p1_wide[i] = (o_Out_byte_0 != o_Out_byte_1);	//Violation?
			end
			else begin
				p1_wide[i] = 0;
			end
		end
	end
	endgenerate

//	assign o_property = ~p1;
	assign o_property = ~(p1_wide == 0);

	



	/* Input Recording Registers */
	genvar i;
	generate
	for(i=1; i<MAX_TIME_INDEX+1; i=i+1) begin

		always@(posedge i_clk)
		begin: INPUT_STORING
			if(i_rst) begin
				ir_CE_0[i]	<=	RESET_VALUE;
				ir_CE_1[i]	<=	RESET_VALUE;

				ir_input_byte_0[i] <= RESET_VALUE;
				ir_input_byte_1[i] <= RESET_VALUE;
			end
			else begin
				if(i==1) begin
					ir_CE_0[i] <= i_CE_0;
					ir_CE_1[i] <= i_CE_1;

					ir_input_byte_0[i] <= i_input_byte_0;
					ir_input_byte_1[i] <= i_input_byte_1;
				end
				else begin
					ir_CE_0[i] <= ir_CE_0[i-1];
					ir_CE_1[i] <= ir_CE_1[i-1];

					ir_input_byte_0[i] <= ir_input_byte_0[i-1];
					ir_input_byte_1[i] <= ir_input_byte_1[i-1];
				end
			end
		end
	end
	endgenerate


	RS_dec m0 (
		.clk		(i_clk			),
		.reset		(i_rst			),
		.CE			(i_CE_0			),
		.input_byte	(i_input_byte_0 ),
		.Out_byte	(o_Out_byte_0	),
		.CEO		(o_CEO_0		),
		.Valid_out	(o_Valid_out_0	)
	);

	RS_dec m1 (
		.clk		(i_clk			),
		.reset		(i_rst			),
		.CE			(i_CE_0			),
		.input_byte	(i_input_byte_0 ),
		.Out_byte	(o_Out_byte_0	),
		.CEO		(o_CEO_0		),
		.Valid_out	(o_Valid_out_0	)
	);


endmodule

/*
	Policy
	
	level	{L,H};
	order	{L<H};

	default level L;
	set wb_dat_o as H;

	allow	wb_dat_i[-1] -> wb_dat_o;
	allow	card_detect[-1] -> wb_dat_o;
	allow	wb_adr_i[-1] -> wb_dat_o;
	allow	wb_sel_i[-1] -> wb_dat_o;
	allow	wb_we_i[-1] -> wb_dat_o;
	allow	wb_cyc_i[-1] -> wb_dat_o;
	allow	wb_stb_i[-1] -> wb_dat_o;
	allow	wb_ack_i[-1] -> wb_dat_o;
	allow	sd_dat_dat_i[-1] -> wb_dat_o;
	allow	sd_cmd_dat_i[-1] -> wb_dat_o;
	//... allow every input[-1] can go to wb_dat_o


*/
module miter #(
	parameter	DATA_WIDTH		=	32,
	parameter	CMD_WIDTH		=	16,
	parameter	ADDR_WIDTH		=	8,
	parameter	SEL_WIDTH		=	4,
	parameter	CTI_WIDTH		=	3,
	parameter	BTE_WIDTH		=	2,
	parameter	SD_DATA_WIDTH	=	4,
	parameter	RESET_VALUE		=	0
)
(
	input	wire								i_clk,
	input	wire								i_rst,

	input	wire	[DATA_WIDTH-1		:	0]	wb_dat_i_m1,
	input	wire	[DATA_WIDTH-1		:	0]	wb_dat_i_m2,
	
	input	wire								card_detect_m1,
	input	wire								card_detect_m2,

	input	wire	[ADDR_WIDTH-1		:	0]	wb_adr_i_m1,
	input	wire	[ADDR_WIDTH-1		:	0]	wb_adr_i_m2,

	input	wire	[SEL_WIDTH-1		:	0]	wb_sel_i_m1,
	input	wire	[SEL_WIDTH-1		:	0]	wb_sel_i_m2,

	input	wire								wb_we_i_m1,
	input	wire								wb_we_i_m2,

	input	wire								wb_cyc_i_m1,
	input	wire								wb_cyc_i_m2,

	input	wire								wb_stb_i_m1,
	input	wire								wb_stb_i_m2,

	input	wire	[DATA_WIDTH-1		:	0]	m_wb_dat_i_m1,
	input	wire	[DATA_WIDTH-1		:	0]	m_wb_dat_i_m2,

	input	wire								m_wb_ack_i_m1,
	input	wire								m_wb_ack_i_m2,

	input	wire	[SD_DATA_WIDTH-1	:	0]	sd_dat_dat_i_m1,
	input	wire	[SD_DATA_WIDTH-1	:	0]	sd_dat_dat_i_m2,

	input	wire								sd_cmd_dat_i_m1,
	input	wire								sd_cmd_dat_i_m2,

	output	wire								o_property
);

	localparam	MAX_TIME_INDEX		=	3;
	localparam	TARGET_TIME_INDEX	=	1;

	wire	[DATA_WIDTH-1		:	0]	wb_dat_o_m1;
	wire	[DATA_WIDTH-1		:	0]	wb_dat_o_m2;

	wire								wb_ack_o_m1;
	wire								wb_ack_o_m2;

	wire	[DATA_WIDTH-1		:	0]	m_wb_adr_o_m1;
	wire	[DATA_WIDTH-1		:	0]	m_wb_adr_o_m2;

	wire	[SEL_WIDTH-1		:	0]	m_wb_sel_o_m1;
	wire	[SEL_WIDTH-1		:	0]	m_wb_sel_o_m2;

	wire								m_wb_we_o_m1;
	wire								m_wb_we_o_m2;

	wire	[DATA_WIDTH-1		:	0]	m_wb_dat_o_m1;
	wire	[DATA_WIDTH-1		:	0]	m_wb_dat_o_m2;

	wire								m_wb_cyc_o_m1;
	wire								m_wb_cyc_o_m2;

	wire								m_wb_stb_o_m1;
	wire								m_wb_stb_o_m2;

	wire	[CTI_WIDTH-1		:	0]	m_wb_cti_o_m1;
	wire	[CTI_WIDTH-1		:	0]	m_wb_cti_o_m2;

	wire	[BTE_WIDTH-1		:	0]	m_wb_bte_o_m1;
	wire	[BTE_WIDTH-1		:	0]	m_wb_bte_o_m2;

	wire	[SD_DATA_WIDTH-1	:	0]	sd_dat_out_o_m1;
	wire	[SD_DATA_WIDTH-1	:	0]	sd_dat_out_o_m2;

	wire								sd_dat_oe_o_m1;
	wire								sd_dat_oe_o_m2;

	wire								sd_cmd_out_o_m1;
	wire								sd_cmd_out_o_m2;

	wire								sd_cmd_oe_o_m1;
	wire								sd_cmd_oe_o_m2;

	wire								sd_clk_o_pad_m1;
	wire								sd_clk_o_pad_m2;

	/*	Input-recording registers	*/
	reg		[DATA_WIDTH-1		:	0]	ir_wb_dat_i_m1[1:MAX_TIME_INDEX];
	reg		[DATA_WIDTH-1		:	0]	ir_wb_dat_i_m2[1:MAX_TIME_INDEX];
	
	reg		[ADDR_WIDTH-1		:	0]	ir_wb_adr_i_m1[1:MAX_TIME_INDEX];
	reg		[ADDR_WIDTH-1		:	0]	ir_wb_adr_i_m2[1:MAX_TIME_INDEX];

	reg		[SEL_WIDTH-1		:	0]	ir_wb_sel_i_m1[1:MAX_TIME_INDEX];
	reg		[SEL_WIDTH-1		:	0]	ir_wb_sel_i_m2[1:MAX_TIME_INDEX];

	reg									ir_wb_we_i_m1[1:MAX_TIME_INDEX];
	reg									ir_wb_we_i_m2[1:MAX_TIME_INDEX];

	reg									ir_wb_cyc_i_m1[1:MAX_TIME_INDEX];
	reg									ir_wb_cyc_i_m2[1:MAX_TIME_INDEX];

	reg									ir_wb_stb_i_m1[1:MAX_TIME_INDEX];
	reg									ir_wb_stb_i_m2[1:MAX_TIME_INDEX];

	reg									ir_write_req_s_m1[1:MAX_TIME_INDEX];
	reg									ir_write_req_s_m2[1:MAX_TIME_INDEX];

	reg									ir_card_detect_m1[1:MAX_TIME_INDEX];
	reg									ir_card_detect_m2[1:MAX_TIME_INDEX];

	reg									ir_m_wb_ack_i_m1[1:MAX_TIME_INDEX];
	reg									ir_m_wb_ack_i_m2[1:MAX_TIME_INDEX];

	reg		[SD_DATA_WIDTH-1	:	0]	ir_sd_dat_dat_i_m1[1:MAX_TIME_INDEX];
	reg		[SD_DATA_WIDTH-1	:	0]	ir_sd_dat_dat_i_m2[1:MAX_TIME_INDEX];

	reg									ir_sd_cmd_dat_i_m1[1:MAX_TIME_INDEX];
	reg									ir_sd_cmd_dat_i_m2[1:MAX_TIME_INDEX];




	/*	Property Logic	*/
	reg p1;
	assign	o_property	=	~p1;


	always @(*)
	begin: PROPERTY_LOGIC_1
		if((ir_wb_dat_i_m1[TARGET_TIME_INDEX] == ir_wb_dat_i_m2[TARGET_TIME_INDEX])
			&&	(ir_wb_adr_i_m1[TARGET_TIME_INDEX]	==	ir_wb_adr_i_m2[TARGET_TIME_INDEX])
			&&	(ir_wb_sel_i_m1[TARGET_TIME_INDEX]	==	ir_wb_sel_i_m2[TARGET_TIME_INDEX])
			&&	(ir_wb_we_i_m1[TARGET_TIME_INDEX]	==	ir_wb_we_i_m2[TARGET_TIME_INDEX])
			&&	(ir_wb_cyc_i_m1[TARGET_TIME_INDEX]	==	ir_wb_cyc_i_m2[TARGET_TIME_INDEX])
			&&	(ir_wb_stb_i_m1[TARGET_TIME_INDEX]	==	ir_wb_stb_i_m2[TARGET_TIME_INDEX])
			&&	(ir_write_req_s_m1[TARGET_TIME_INDEX]	==	ir_write_req_s_m2[TARGET_TIME_INDEX])
			&&	(ir_card_detect_m1[TARGET_TIME_INDEX]	==	ir_card_detect_m2[TARGET_TIME_INDEX])
			&&	(ir_m_wb_ack_i_m1[TARGET_TIME_INDEX]	==	ir_m_wb_ack_i_m2[TARGET_TIME_INDEX])
			&&	(ir_sd_dat_dat_i_m1[TARGET_TIME_INDEX]	==	ir_sd_dat_dat_i_m2[TARGET_TIME_INDEX])
			&&	(ir_sd_cmd_dat_i_m1[TARGET_TIME_INDEX]	==	ir_sd_cmd_dat_i_m2[TARGET_TIME_INDEX])
			) begin
				p1 = (wb_dat_o_m1 == wb_dat_o_m2)? 1: 0;
			end
			else begin
				p1 = 1;
			end
	end

	/* Input storing logics	*/
	genvar i;
	generate
		for(i=1; i<MAX_TIME_INDEX;	i=i+1) begin
			always @ (posedge i_clk)
			begin: INPUT_STORING
				if(i_rst) begin
					ir_wb_dat_i_m1[i] <= RESET_VALUE;
					ir_wb_dat_i_m2[i] <= RESET_VALUE;
	
					ir_wb_adr_i_m1[i] <= RESET_VALUE;
					ir_wb_adr_i_m2[i] <=RESET_VALUE;

					ir_wb_sel_i_m1[i] <=RESET_VALUE;
					ir_wb_sel_i_m2[i] <=RESET_VALUE;

					ir_wb_we_i_m1[i] <=RESET_VALUE;
					ir_wb_we_i_m2[i] <=RESET_VALUE;

					ir_wb_cyc_i_m1[i] <=RESET_VALUE;
					ir_wb_cyc_i_m2[i] <=RESET_VALUE;

					ir_wb_stb_i_m1[i] <= RESET_VALUE;
					ir_wb_stb_i_m2[i] <= RESET_VALUE;

					ir_card_detect_m1[i]	<= RESET_VALUE;	
					ir_card_detect_m2[i]	<= RESET_VALUE;		

					ir_m_wb_ack_i_m1[i]		<= RESET_VALUE;		
					ir_m_wb_ack_i_m2[i]		<= RESET_VALUE;		

					ir_sd_dat_dat_i_m1[i]	<= RESET_VALUE;		
					ir_sd_dat_dat_i_m2[i]	<= RESET_VALUE;		

					ir_sd_cmd_dat_i_m1[i]	<= RESET_VALUE;	
					ir_sd_cmd_dat_i_m2[i]	<= RESET_VALUE;	

				end
				else begin
					if(i==1) begin
						ir_wb_dat_i_m1[i] <= wb_dat_i_m1;
						ir_wb_dat_i_m2[i] <= wb_dat_i_m2;
	
						ir_wb_adr_i_m1[i] <= wb_adr_i_m1;
						ir_wb_adr_i_m2[i] <= wb_adr_i_m2;

						ir_wb_sel_i_m1[i] <= wb_sel_i_m1;
						ir_wb_sel_i_m2[i] <= wb_sel_i_m2;

						ir_wb_we_i_m1[i] <= wb_we_i_m1;
						ir_wb_we_i_m2[i] <= wb_we_i_m2;

						ir_wb_cyc_i_m1[i] <= wb_cyc_i_m1;
						ir_wb_cyc_i_m2[i] <= wb_cyc_i_m2;

						ir_wb_stb_i_m1[i] <= wb_stb_i_m1;
						ir_wb_stb_i_m2[i] <= wb_stb_i_m2;

						ir_card_detect_m1[i]	<= card_detect_m1;	
						ir_card_detect_m2[i]	<= card_detect_m2;

						ir_m_wb_ack_i_m1[i]		<= m_wb_ack_i_m1;
						ir_m_wb_ack_i_m2[i]		<= m_wb_ack_i_m2;

						ir_sd_dat_dat_i_m1[i]	<= sd_dat_dat_i_m1;
						ir_sd_dat_dat_i_m2[i]	<= sd_dat_dat_i_m2;

						ir_sd_cmd_dat_i_m1[i]	<= sd_cmd_dat_i_m1;
						ir_sd_cmd_dat_i_m2[i]	<= sd_cmd_dat_i_m2;
					end
					else begin
						ir_wb_dat_i_m1[i] <= ir_wb_dat_i_m1[i-1];
						ir_wb_dat_i_m2[i] <= ir_wb_dat_i_m2[i-1];
	
						ir_wb_adr_i_m1[i] <= ir_wb_adr_i_m1[i-1];
						ir_wb_adr_i_m2[i] <= ir_wb_adr_i_m2[i-1];

						ir_wb_sel_i_m1[i] <= ir_wb_sel_i_m1[i-1];
						ir_wb_sel_i_m2[i] <= ir_wb_sel_i_m2[i-1];

						ir_wb_we_i_m1[i] <= ir_wb_we_i_m1[i-1];
						ir_wb_we_i_m2[i] <= ir_wb_we_i_m2[i-1];

						ir_wb_cyc_i_m1[i] <= ir_wb_cyc_i_m1[i-1];
						ir_wb_cyc_i_m2[i] <= ir_wb_cyc_i_m2[i-1];

						ir_wb_stb_i_m1[i] <= ir_wb_stb_i_m1[i-1];
						ir_wb_stb_i_m2[i] <= ir_wb_stb_i_m2[i-1];

						ir_card_detect_m1[i]	<= ir_card_detect_m1[i-1];
						ir_card_detect_m2[i]	<= ir_card_detect_m2[i-1];

						ir_m_wb_ack_i_m1[i]		<=	ir_m_wb_ack_i_m1[i-1];
						ir_m_wb_ack_i_m2[i]		<=	ir_m_wb_ack_i_m2[i-1];

						ir_sd_dat_dat_i_m1[i]	<=	ir_sd_dat_dat_i_m1[i-1];
						ir_sd_dat_dat_i_m2[i]	<=	ir_sd_dat_dat_i_m2[i-1];

						ir_sd_cmd_dat_i_m1[i]	<= ir_sd_cmd_dat_i_m1[i-1];
						ir_sd_cmd_dat_i_m2[i]	<= ir_sd_cmd_dat_i_m2[i-1];

					end
				end
			end
		end
	endgenerate








sdc_controller cnt1 (
	.wb_clk_i		(i_clk				),
	.wb_rst_i		(i_rst				),
	.wb_dat_i		(wb_dat_i_m1		),
	.wb_dat_o		(wb_dat_o_m1		),
	.wb_adr_i		(wb_adr_i_m1		),
	.wb_sel_i		(wb_sel_i_m1		),
	.wb_we_i		(wb_we_i_m1			),
	.wb_cyc_i		(wb_cyc_i_m1		),
	.wb_stb_i		(wb_stb_i_m1		),
	.wb_ack_o		(wb_ack_o_m1		),
	.m_wb_adr_o		(m_wb_adr_o_m1		),
	.m_wb_sel_o		(m_wb_sel_o_m1		),
	.m_wb_we_o		(m_wb_we_o_m1		),
	.m_wb_dat_o		(m_wb_dat_o_m1		),
	.m_wb_dat_i		(m_wb_dat_i_m1		),
	.m_wb_cyc_o		(m_wb_cyc_o_m1		),
	.m_wb_stb_o		(m_wb_stb_o_m1		),
	.m_wb_ack_i		(m_wb_ack_i_m1		),
	.m_wb_cti_o		(m_wb_cti_o_m1		),
	.m_wb_bte_o		(m_wb_bte_o_m1		),
	.sd_cmd_dat_i	(sd_cmd_dat_i_m1	),
	.sd_cmd_out_o	(sd_cmd_out_o_m1	),
	.sd_cmd_oe_o	(sd_cmd_oe_o_m1		),
	.card_detect	(card_detect_m1		),
	.sd_dat_dat_i	(sd_dat_dat_i_m1	),
	.sd_dat_out_o	(sd_dat_out_o_m1	),
	.sd_dat_oe_o	(sd_dat_oe_o_m1		),
	.sd_clk_o_pad	(sd_clk_o_pad_m1	)
);

sdc_controller cnt2 (
	.wb_clk_i		(i_clk				),
	.wb_rst_i		(i_rst				),
	.wb_dat_i		(wb_dat_i_m2		),
	.wb_dat_o		(wb_dat_o_m2		),
	.wb_adr_i		(wb_adr_i_m2		),
	.wb_sel_i		(wb_sel_i_m2		),
	.wb_we_i		(wb_we_i_m2			),
	.wb_cyc_i		(wb_cyc_i_m2		),
	.wb_stb_i		(wb_stb_i_m2		),
	.wb_ack_o		(wb_ack_o_m2		),
	.m_wb_adr_o		(m_wb_adr_o_m2		),
	.m_wb_sel_o		(m_wb_sel_o_m2		),
	.m_wb_we_o		(m_wb_we_o_m2		),
	.m_wb_dat_o		(m_wb_dat_o_m2		),
	.m_wb_dat_i		(m_wb_dat_i_m2		),
	.m_wb_cyc_o		(m_wb_cyc_o_m2		),
	.m_wb_stb_o		(m_wb_stb_o_m2		),
	.m_wb_ack_i		(m_wb_ack_i_m2		),
	.m_wb_cti_o		(m_wb_cti_o_m2		),
	.m_wb_bte_o		(m_wb_bte_o_m2		),
	.sd_cmd_dat_i	(sd_cmd_dat_i_m2	),
	.sd_cmd_out_o	(sd_cmd_out_o_m2	),
	.sd_cmd_oe_o	(sd_cmd_oe_o_m2		),
	.card_detect	(card_detect_m2		),
	.sd_dat_dat_i	(sd_dat_dat_i_m2	),
	.sd_dat_out_o	(sd_dat_out_o_m2	),
	.sd_dat_oe_o	(sd_dat_oe_o_m2		),
	.sd_clk_o_pad	(sd_clk_o_pad_m2	)
);

endmodule

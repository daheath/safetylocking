module miter #(
	parameter	DATA_WIDTH		=	32,
	parameter	ADDR_WIDTH		=	8,
	parameter	SEL_WIDTH		=	4,
	parameter	CTI_WIDTH		=	3,
	parameter	BTE_WIDTH		=	2,
	parameter	SD_DATA_WIDTH	=	4
)
(
	input										wb_clk_i_m1,
	input										wb_clk_i_m2,

	input										wb_rst_i_m1,
	input										wb_rst_i_m2,

	input	wire	[DATA_WIDTH-1		:	0]	wb_dat_i_m1,
	input	wire	[DATA_WIDTH-1		:	0]	wb_dat_i_m2,
	

	input	wire								card_detect_m1,
	input	wire								card_detect_m2,

	input	wire	[ADDR_WIDTH-1		:	0]	wb_adr_i_m1,
	input	wire	[ADDR_WIDTH-1		:	0]	wb_adr_i_m2,

	input	wire	[SEL_WIDTH-1		:	0]	wb_sel_i_m1,
	input	wire	[SEL_WIDTH-1		:	0]	wb_sel_i_m2,

	input	wire								wb_we_i_m1,
	input	wire								wb_we_i_m2,

	input	wire								wb_cyc_i_m1,
	input	wire								wb_cyc_i_m2,

	input	wire								wb_stb_i_m1,
	input	wire								wb_stb_i_m2,

	input	wire	[DATA_WIDTH-1		:	0]	m_wb_dat_i_m1,
	input	wire	[DATA_WIDTH-1		:	0]	m_wb_dat_i_m2,

	input	wire								m_wb_ack_i_m1,
	input	wire								m_wb_ack_i_m2,

	input	wire	[SD_DATA_WIDTH-1	:	0]	sd_dat_dat_i_m1,
	input	wire	[SD_DATA_WIDTH-1	:	0]	sd_dat_dat_i_m2,

	input	wire								sd_cmd_dat_i_m1,
	input	wire								sd_cmd_dat_i_m2
);

	wire	[DATA_WIDTH-1		:	0]	wb_dat_o_m1;
	wire	[DATA_WIDTH-1		:	0]	wb_dat_o_m2;

	wire								wb_ack_o_m1;
	wire								wb_ack_o_m2;

	wire	[DATA_WIDTH-1		:	0]	m_wb_adr_o_m1;
	wire	[DATA_WIDTH-1		:	0]	m_wb_adr_o_m2;

	wire	[SEL_WIDTH-1		:	0]	m_wb_sel_o_m1;
	wire	[SEL_WIDTH-1		:	0]	m_wb_sel_o_m2;

	wire								m_wb_we_o_m1;
	wire								m_wb_we_o_m2;

	wire	[DATA_WIDTH-1		:	0]	m_wb_dat_o_m1;
	wire	[DATA_WIDTH-1		:	0]	m_wb_dat_o_m2;

	wire								m_wb_cyc_o_m1;
	wire								m_wb_cyc_o_m2;

	wire								m_wb_stb_o_m1;
	wire								m_wb_stb_o_m2;

	wire	[CTI_WIDTH-1		:	0]	m_wb_cti_o_m1;
	wire	[CTI_WIDTH-1		:	0]	m_wb_cti_o_m2;

	wire	[BTE_WIDTH-1		:	0]	m_wb_bte_o_m1;
	wire	[BTE_WIDTH-1		:	0]	m_wb_bte_o_m2;

	wire	[SD_DATA_WIDTH-1	:	0]	sd_dat_out_o_m1;
	wire	[SD_DATA_WIDTH-1	:	0]	sd_dat_out_o_m2;

	wire								sd_dat_oe_o_m1;
	wire								sd_dat_oe_o_m2;

	wire								sd_cmd_out_o_m1;
	wire								sd_cmd_out_o_m2;

	wire								sd_cmd_oe_o_m1;
	wire								sd_cmd_oe_o_m2;

	wire								sd_clk_o_pad_m1;
	wire								sd_clk_o_pad_m2;

sdc_controller cnt1 (
	.wb_clk_i		(wb_clk_i_m1		),
	.wb_rst_i		(wb_rst_i_m1		),
	.wb_dat_i		(wb_dat_i_m1		),
	.wb_dat_o		(wb_dat_o_m1		),
	.wb_adr_i		(wb_adr_i_m1		),
	.wb_sel_i		(wb_sel_i_m1		),
	.wb_we_i		(wb_we_i_m1			),
	.wb_cyc_i		(wb_cyc_i_m1		),
	.wb_stb_i		(wb_stb_i_m1		),
	.wb_ack_o		(wb_ack_o_m1		),
	.m_wb_adr_o		(m_wb_adr_o_m1		),
	.m_wb_sel_o		(m_wb_sel_o_m1		),
	.m_wb_we_o		(m_wb_we_o_m1		),
	.m_wb_dat_o		(m_wb_dat_o_m1		),
	.m_wb_dat_i		(m_wb_dat_i_m1		),
	.m_wb_cyc_o		(m_wb_cyc_o_m1		),
	.m_wb_stb_o		(m_wb_stb_o_m1		),
	.m_wb_ack_i		(m_wb_ack_i_m1		),
	.m_wb_cti_o		(m_wb_cti_o_m1		),
	.m_wb_bte_o		(m_wb_bte_o_m1		),
	.sd_cmd_dat_i	(sd_cmd_dat_i_m1	),
	.sd_cmd_out_o	(sd_cmd_out_o_m1	),
	.sd_cmd_oe_o	(sd_cmd_oe_o_m1		),
	.card_detect	(card_detect_m1		),
	.sd_dat_dat_i	(sd_dat_dat_i_m1	),
	.sd_dat_out_o	(sd_dat_out_o_m1	),
	.sd_dat_oe_o	(sd_dat_oe_o_m1		),
	.sd_clk_o_pad	(sd_clk_o_pad_m1	)
);

sdc_controller cnt2 (
	.wb_clk_i		(wb_clk_i_m2		),
	.wb_rst_i		(wb_rst_i_m2		),
	.wb_dat_i		(wb_dat_i_m2		),
	.wb_dat_o		(wb_dat_o_m2		),
	.wb_adr_i		(wb_adr_i_m2		),
	.wb_sel_i		(wb_sel_i_m2		),
	.wb_we_i		(wb_we_i_m2			),
	.wb_cyc_i		(wb_cyc_i_m2		),
	.wb_stb_i		(wb_stb_i_m2		),
	.wb_ack_o		(wb_ack_o_m2		),
	.m_wb_adr_o		(m_wb_adr_o_m2		),
	.m_wb_sel_o		(m_wb_sel_o_m2		),
	.m_wb_we_o		(m_wb_we_o_m2		),
	.m_wb_dat_o		(m_wb_dat_o_m2		),
	.m_wb_dat_i		(m_wb_dat_i_m2		),
	.m_wb_cyc_o		(m_wb_cyc_o_m2		),
	.m_wb_stb_o		(m_wb_stb_o_m2		),
	.m_wb_ack_i		(m_wb_ack_i_m2		),
	.m_wb_cti_o		(m_wb_cti_o_m2		),
	.m_wb_bte_o		(m_wb_bte_o_m2		),
	.sd_cmd_dat_i	(sd_cmd_dat_i_m2	),
	.sd_cmd_out_o	(sd_cmd_out_o_m2	),
	.sd_cmd_oe_o	(sd_cmd_oe_o_m2		),
	.card_detect	(card_detect_m2		),
	.sd_dat_dat_i	(sd_dat_dat_i_m2	),
	.sd_dat_out_o	(sd_dat_out_o_m2	),
	.sd_dat_oe_o	(sd_dat_oe_o_m2		),
	.sd_clk_o_pad	(sd_clk_o_pad_m2	)
);

endmodule

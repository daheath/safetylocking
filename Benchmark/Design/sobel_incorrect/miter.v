/*
	Policy
	
	Lmitation of the language

	if((i_pixel_wID[N] >= PROHIBIT_W_START && i_pixel_wID[N] <= PROHIBIT_W_END)
		|| (i_pixel_hID[N] >= PROHIBIT_H_START && i_pixel_hID[N] <= PROHIBIT_H_END))

		prohibit i_pixel_data[-1] -> o_output
*/

module miter #(
	parameter	IMAGE_WIDTH				=	10,
	parameter	IMAGE_HEIGHT			=	10,
	parameter	INPUT_DATA_BIT_WIDTH	=	9,
	parameter	OUTPUT_DATA_BIT_WIDTH	=	8,
	parameter	WINDOW_SIZE				=	3,
	parameter	PROHIBIT_W_START		=	4,
	parameter	PROHIBIT_W_END			=	6,
	parameter	PROHIBIT_H_START		=	3,
	parameter	PROHIBIT_H_END			=	7,
	parameter	RECORD_STAGES			=	2,
	parameter	TARGET_TIME				=	1,
	parameter	RESET_VALUE				=	0
)
(
	input	wire								i_clk,
	input	wire								i_rst,

	input	wire	[INPUT_BIT_WIDTH-1	:	0]	i_pixel_data_m1,
	input	wire	[INPUT_BIT_WIDTH-1	:	0]	i_pixel_data_m2,

	input	wire	[INPUT_BIT_WIDTH-1	:	0]	i_pixel_wID_m1,
	input	wire	[INPUT_BIT_WIDTH-1	:	0]	i_pixel_wID_m2,

	input	wire	[INPUT_BIT_WIDTH-1	:	0]	i_pixel_hID_m1,
	input	wire	[INPUT_BIT_WIDTH-1	:	0]	i_pixel_hID_m2,

	output	wire								o_property
);
	/*	Local Params	*/
	//All deduced by existing parameters
	localparam	NUM_PIXELS					=	WINDOW_SIZE * WINDOW_SIZE-1;
	localparam	INPUT_BIT_WIDTH				=	NUM_PIXELS * INPUT_DATA_BIT_WIDTH;
	localparam	OUTPUT_BIT_WIDTH			=	OUTPUT_DATA_BIT_WIDTH;


	wire	[OUTPUT_BIT_WIDTH-1	:	0]	o_output_data_m1;
	wire	[OUTPUT_BIT_WIDTH-1	:	0]	o_output_data_m2;

	/*	Registers for input Recording	*/
	reg		[INPUT_BIT_WIDTH-1	:	0]	ir_pixel_data_m1	[1:RECORD_STAGES];
	reg		[INPUT_BIT_WIDTH-1	:	0]	ir_pixel_data_m2	[1:RECORD_STAGES];
	
	/*	Registers for condition Recording	*/
	reg		[NUM_PIXELS-1		:	0]	cr_violate_flag_m1	[1:RECORD_STAGES];
	reg		[NUM_PIXELS-1		:	0]	cr_violate_flag_m2	[1:RECORD_STAGES];

	/*	Temporary variables	*/
	reg		[NUM_PIXELS-1		:	0]	violate_flag_m1;
	reg		[NUM_PIXELS-1		:	0]	violate_flag_m2;

	reg p1;

	assign	o_property = ~p1;
//	assign	o_property = 1;

	/*	Property Logic	*/

	always@(*)
	begin: PROPERTY_LOGIC_1
		
		//	If the inputs for both machines violated the condition,
		//	the output should remain same even though they were different. (Violation => No information flow)

		//If the inputs were different
		if(ir_pixel_data_m1[TARGET_TIME] != ir_pixel_data_m2[TARGET_TIME])
		begin
			//And they all violated the condition
			if((cr_violate_flag_m1[TARGET_TIME] != 0)
				&&	(cr_violate_flag_m2[TARGET_TIME] != 0)
			) begin
				//Then the output should remain same
				p1 = (o_output_data_m1 == o_output_data_m2)? 1: 0;
			end
			else begin
				p1 = 1;
			end
		end
		else begin
			p1 = 1;
		end
	end


	/*	Condition checking	*/
	genvar i;
	generate
		for(i=0; i<NUM_PIXELS; i=i+1) begin
			//For Machine 1
			always@(*)
			begin: VIOLATION_CHECK_M1
				//Check if the pixel is not from prohibited area
				if(
					//Check width ID condition
					((i_pixel_wID_m1[i*INPUT_DATA_BIT_WIDTH +: INPUT_DATA_BIT_WIDTH] < PROHIBIT_W_START)
					|| (i_pixel_wID_m1[i*INPUT_DATA_BIT_WIDTH +: INPUT_DATA_BIT_WIDTH] > PROHIBIT_W_END)
					)	
					//Check height ID condition
				|| ((i_pixel_hID_m1[i*INPUT_DATA_BIT_WIDTH +: INPUT_DATA_BIT_WIDTH] < PROHIBIT_H_START) 
					|| (i_pixel_hID_m1[i*INPUT_DATA_BIT_WIDTH +: INPUT_DATA_BIT_WIDTH] > PROHIBIT_H_END))
				) begin
					violate_flag_m1[i] = 0;	//If the pixel is from allowed area, violate flag is 0.
				end
				else begin
					violate_flag_m1[i] = 1;	//If there is a violation, set the violation flag
				end
			end

			//For Machine 2
			always@(*)
			begin: VIOLATION_CHECK_M2
				//Check if the pixel is not from prohibited area
				if(
					//Check width ID condition
					((i_pixel_wID_m2[i*INPUT_DATA_BIT_WIDTH +: INPUT_DATA_BIT_WIDTH] < PROHIBIT_W_START)
					|| (i_pixel_wID_m2[i*INPUT_DATA_BIT_WIDTH +: INPUT_DATA_BIT_WIDTH] > PROHIBIT_W_END)
					)	
					//Check height ID condition
				||((i_pixel_hID_m2[i*INPUT_DATA_BIT_WIDTH +: INPUT_DATA_BIT_WIDTH] < PROHIBIT_H_START) 
					|| (i_pixel_hID_m2[i*INPUT_DATA_BIT_WIDTH +: INPUT_DATA_BIT_WIDTH] > PROHIBIT_H_END))
				) begin
					violate_flag_m2[i] = 0;	//If the pixel is from allowed area, violate flag is 0.
				end
				else begin
					violate_flag_m2[i] = 1;	//If there is a violation, set the violation flag
				end
			end

		end
	endgenerate

	/*	Condition Recording	*/
	genvar k;
	generate
		for(k=1; k<RECORD_STAGES+1; k=k+1) begin
			always@(posedge i_clk)
			begin:	RECORD_VIOLATE_FLAG
				if(i_rst) begin
					cr_violate_flag_m1[k] <= RESET_VALUE;
					cr_violate_flag_m2[k] <= RESET_VALUE;
				end
				else begin
					if(k==1) begin
						cr_violate_flag_m1[k] <= violate_flag_m1;
						cr_violate_flag_m2[k] <= violate_flag_m2;
					end
					else begin
						cr_violate_flag_m1[k] <= cr_violate_flag_m1[k-1];
						cr_violate_flag_m2[k] <= cr_violate_flag_m2[k-1];
					end
				end

			end
		end
	endgenerate

	/*	Input Recording	*/
	genvar x;
	generate
		for(x=1; x<RECORD_STAGES+1; x=x+1) begin
			always @(posedge i_clk)
			begin: RECORD_I_PIXEL_DATA
				if(i_rst) begin
					ir_pixel_data_m1[x] <= RESET_VALUE;
					ir_pixel_data_m2[x] <= RESET_VALUE;
				end
				else begin
					if(x == 1) begin
						ir_pixel_data_m1[x] <= i_pixel_data_m1;
						ir_pixel_data_m2[x] <= i_pixel_data_m2;
					end
					else begin
						ir_pixel_data_m1[x] <= ir_pixel_data_m1[x-1];
						ir_pixel_data_m2[x] <= ir_pixel_data_m2[x-1];
					end
				end
			end
		end
	endgenerate

	/*	Modules	*/
	sobel_top #(
		.IMAGE_WIDTH			(10	),
		.IMAGE_HEIGHT			(10	),
		.INPUT_DATA_BIT_WIDTH	(9	),
		.OUTPUT_DATA_BIT_WIDTH	(8	),
		.WINDOW_SIZE			(3	),
		.PROHIBIT_W_START		(4	),
		.PROHIBIT_W_END			(6	),
		.PROHIBIT_H_START		(3	),
		.PROHIBIT_H_END			(7	)
	) sb1 (
		.i_clk					(i_clk				),
		.i_rst					(i_rst				),
		.i_pixel_data			(i_pixel_data_m1	),
		.i_pixel_wID			(i_pixel_wID_m1		),
		.i_pixel_hID			(i_pixel_hID_m1		),
		.o_output_data			(o_output_data_m1	)
	);

	sobel_top #(
		.IMAGE_WIDTH			(10	),
		.IMAGE_HEIGHT			(10	),
		.INPUT_DATA_BIT_WIDTH	(9	),
		.OUTPUT_DATA_BIT_WIDTH	(8	),
		.WINDOW_SIZE			(3	),
		.PROHIBIT_W_START		(4	),
		.PROHIBIT_W_END			(6	),
		.PROHIBIT_H_START		(3	),
		.PROHIBIT_H_END			(7	)
	) sb2 (
		.i_clk					(i_clk				),
		.i_rst					(i_rst				),
		.i_pixel_data			(i_pixel_data_m2	),
		.i_pixel_wID			(i_pixel_wID_m2		),
		.i_pixel_hID			(i_pixel_hID_m2		),
		.o_output_data			(o_output_data_m2	)
	);

endmodule

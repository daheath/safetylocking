//`timescale 1ns / 1ps

module sobel_tb();

	reg [8:0] p0,p1,p2,p3,p5,p6,p7,p8,p4;
	reg clk;
	wire [7:0] out;
	integer in,temp,ot;
	integer count;

	initial begin
		$dumpfile("TB.vcd");
		$dumpvars(0, sobel_tb);
		#1000
		$finish;
	end

	initial begin
		clk = 1'b0;
		count = 0;
		in = $fopen("misc/512x512sobel_in.txt","r");	//enter input file name containing pixels
		ot = $fopen("/misc/512x512sobel_out_nbits.txt","w");	//enter output file name
		#1 p0 = 8'b0; p1 = 8'b0; p2 = 8'b0; p3 = 8'b0; p4 = 8'b0; p5 = 8'b0; p6 = 8'b0; p7 = 8'b0;
		#1;
		//repeat(262144) begin	//enter number of pixels in image
		//	temp = $fscanf(in,"%d %d %d %d %d %d %d %d %d\n",p0,p1,p2,p3,p4,p5,p6,p7,p8); 
			//$display("%d %d %d %d %d %d %d %d %d\n",p0,p1,p2,p3,p4,p5,p6,p7,p8);
		//	#1 $fwrite(ot,"%d\n",out);
			//#1 $display("%b %b %b %b %b %b %b %b %b %b %b %b %b %b %b %b %b",U0.p2_p0, U0.p5_p3, U0.p8_p6, U0.center_2, U0.abs_gx, U0.p0_p6, U0.p1_p7, U0.p2_p8, U0.center_4, U0.abs_gy, U0.gx_2s_comp, U0.gy_2s_comp, U0.abs_gx, U0.abs_gy, U0.sum, U0.or_out, U0.out);
			//#1 $display("%b %b %b %b %b %b %b %b %b",U0.gx, U0.gy, U0.gx_2s_comp, U0.gy_2s_comp, U0.abs_gx, U0.abs_gy, U0.sum, U0.or_out, U0.out);
		//	#5;
		//end
	end

	always @(posedge clk)
	begin
		if(count > 262144)
		begin
			$finish;
		end
		else
		begin
			temp = $fscanf(in,"%d %d %d %d %d %d %d %d %d\n",p0,p1,p2,p3,p4,p5,p6,p7,p8);
			if(count > 0)
			begin
				#1 $fwrite(ot,"%d\n",out);
			end 
		end
		count = count + 1;
	end

	always
	begin
		#5 clk = ~clk;
	end

	sobel U0 (clk,p0,p1,p2,p3,p5,p6,p7,p8,out);

endmodule

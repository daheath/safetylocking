# Contributing

Before you get started, read the README.

## Getting Started

You are going to need the logic analysis tool `abc`.
You can built it from source by performing the following:

```shell
$ hg clone https://bitbucket.org/alanmi/abc
$ cd abc
$ make
$ sudo cp abc /usr/local/bin
```

In addition you'll need `yosys`.
You should be able to obtain it via your package manager.

[Useful documentation for `abc`](https://people.eecs.berkeley.edu/~alanmi/abc/abc.htm).

## Building

```shell
cabal sandbox init
cabal install --only-dependencies
cabal build
```
